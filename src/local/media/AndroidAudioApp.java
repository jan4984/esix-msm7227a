package local.media;

import android.content.Context;
import android.net.rtp.AudioCodec;
import android.net.rtp.AudioGroup;
import android.net.rtp.AudioStream;
import android.net.rtp.RtpStream;
import android.os.ServiceManager;
import android.util.Log;
import com.womate.esix.MainService;
import com.womate.esix.service.ServiceFactory;
import com.womate.esix.service.impl.SIPUAServiceImpl;
import local.server.ESixLocationServiceImpl;
import local.ua.MediaAgent;
import org.zoolu.net.SocketAddress;
import org.zoolu.net.UdpSocket;

import java.net.Inet4Address;
import java.net.InetAddress;
import android.os.INetworkManagementService;

/** audio app for android, currently in static audio format **/
public class AndroidAudioApp implements MediaApp, RtpStreamReceiverListener {
	final static String TAG = "ESix-AndroidAudioApp";

    InetAddress mRemoteAddr;
    int mRemotePort;
    static AudioStream mAS;
    static boolean ASUsed = true;
    AudioGroup mAG;

	/** @param local true to use local mic and speaker, otherwise, use cellular call  */
	public AndroidAudioApp(Object owner, boolean local
			, FlowSpec flowSpec, boolean inFile, boolean outFile)
	{
        try{
            mRemoteAddr = InetAddress.getByName(flowSpec.getRemoteAddress());
            mRemotePort = flowSpec.getRemotePort();
            if(ASUsed){
                throw new Exception("please new a new AudioStream by calling newAudioStream()");
            }
            mAS.setCodec(flowSpec.getMediaSpec().codec.equalsIgnoreCase("gsm") ? AudioCodec.GSM : AudioCodec.PCMU);
            mAS.associate(mRemoteAddr, mRemotePort);
            ASUsed = true;
            Log.d(TAG, "audio stream created." + flowSpec.getLocalPort() + " <==> " + mRemoteAddr + ":" + mRemotePort + " with codec:" + flowSpec.getMediaSpec().codec);
        }
        catch(Exception exp){
            Log.e(TAG, "new instance", exp);
        }
	}
    /*
    public static synchronized  void setAudioStreamCodec(boolean isGSM){
        if(ASUsed){
            Log.e(TAG, "no audio stream for being set codec");
        }
        mAS.setCodec(isGSM ? AudioCodec.GSM : AudioCodec.PCMU);
    }
    */
    public void sendFile2Rtp(String file){
        if(mAG == null){
            Log.e(TAG, "audio group not found");
            return;
        }
        if(mAG.getMode() != AudioGroup.MODE_NORMAL){// && mAG.getMode() != AudioGroup.MODE_ECHO_SUPPRESSION){
            Log.e(TAG, "audio group not in normal mode");
            return;
        }
        mAG.sendFile2Rtp(file);
    }
    public static synchronized int newAudioStream(){

        try{
            if(!ASUsed && mAS != null) return mAS.getLocalPort();
            mAS = new AudioStream(java.net.InetAddress.getByName(ESixLocationServiceImpl.ESIX_STATIC_IP));
            mAS.setCodec(AudioCodec.PCMU);
            mAS.setMode(RtpStream.MODE_NORMAL);
            ASUsed = false;
        }catch(Exception exp){
            android.util.Log.d(TAG, "new audio stream", exp);
            return 0;
        }
        return mAS.getLocalPort();
    }

	@Override
	public boolean startApp() {
		Log.d(TAG, "startApp");
        try{
            mAG = new AudioGroup();
            mAS.setDtmfType(MediaAgent.getDtmfRecvPayloadType());
            mAG.setDtmfReceivedListener(new AudioGroup.IDTMFReceivedListener() {
                public void onDtmfReceived(int c) {
                    ServiceFactory.getCallService().onDtmfReceived(c);
                }
            });
        }catch(Exception exp){
            Log.e(TAG, "new audio group", exp);
        }
        if(mAS == null || mAG == null){
            Log.e(TAG, "AudioStream and AudioGroup not created");
            return false;
        }
        switchToHighPerformance();
        mAS.join(mAG);
        mAG.setMode(AudioGroup.MODE_NORMAL);
        Log.d(TAG, "AudioStream join, start audio and rtp");
		return true;
	}

	@Override
	public boolean stopApp() {
		Log.d(TAG, "stopApp");
        if(mAG != null){
            mAG.setMode(AudioGroup.MODE_ON_HOLD);
        }
        if(mAS != null){
            mAS.join(null);
        }
        if(mAG != null){
            mAG.clear();
        }
        mAG = null;
        mAS = null;
        System.gc();
        switchToNormalPerformance();
        Log.d(TAG, "set audio group to HOLD and remove audio stream from it");
		return true;
	}

	public void onRemoteSoAddressChanged(RtpStreamReceiver rr,
			SocketAddress remote_soaddr) {
		Log.e(TAG, "remote addres changed, audio Hacking, stop audio now");
        stopApp();
	}
    public static void switchToNormalPerformance(){
        try{
            INetworkManagementService serv =
                    INetworkManagementService.Stub.asInterface(
                            ServiceManager.getService(Context.NETWORKMANAGEMENT_SERVICE));
            serv.untetherInterface("cpu-performance");
        }catch(Exception exp){
            Log.e(TAG, "try set cpu performance", exp);
        }
    }
    public static void switchToHighPerformance(){
        try{
            INetworkManagementService serv =
                    INetworkManagementService.Stub.asInterface(
                            ServiceManager.getService(Context.NETWORKMANAGEMENT_SERVICE));
            serv.tetherInterface("cpu-performance");
        }catch(Exception exp){
            Log.e(TAG, "try set cpu performance", exp);
        }
    }
}
