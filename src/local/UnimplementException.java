package local;

public class UnimplementException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7462830313644688539L;
	
	public UnimplementException(String msg){
		super(msg);
	}
}
