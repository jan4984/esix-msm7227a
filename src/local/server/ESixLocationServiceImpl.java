package local.server;

import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Pattern;

import com.womate.esix.MainService;
import com.womate.esix.service.ServiceFactory;
import org.zoolu.tools.Log;
import org.zoolu.sip.address.NameAddress;
import org.zoolu.sip.address.SipURL;

public class ESixLocationServiceImpl implements LocationService{

	private static boolean _isAllNumber(String s){
		int idx = s.indexOf('@');
		if(idx != -1){
			return rexp.matcher(s.substring(0, idx)).find();
		}
		return rexp.matcher(s).find();
	}

    private static boolean _isEsix(String s){
        int idx = s.indexOf('@');
        if(idx != -1){
            String name = s.substring(0, idx);
            return name.equals(ESixLocationServiceImpl.ESIX_USER_NAME);
        }
        return s.equals(ESixLocationServiceImpl.ESIX_USER_NAME);
    }

	public static String ESIX_USER_NAME;
	public static String ESIX_STATIC_IP = null;
	public static int ESIX_STATIC_PORT;
	public static String ESIX_CONTACT_URL;
	public static String[] ESIX_CONTACT_URLS;
	public static MyEnumeration ESIX_CONTACT_URLS_ENUM;
	public static NameAddress ESIX_NAME_ADDRESS;
	//final static Pattern rexp = Pattern.compile("^"+ESIX_USER_NAME+"_[0-9*#+]+$");
	public static Pattern rexp;
	public static Date ESIX_EXPIRE_DATE;
	public static int MAX_USER;
    public static boolean IS_INTERNAL_SIP_PROXY;

    public static void initConsts(String ip, boolean isInternalSipProxy){
        ESIX_USER_NAME = "esix";
        ESIX_STATIC_IP = ip;
        ESIX_STATIC_PORT = 34567;
        ESIX_CONTACT_URL = "sip:"+ESIX_USER_NAME+"@"+ESIX_STATIC_IP+":"+ESIX_STATIC_PORT+";transport=TCP";
        ESIX_CONTACT_URLS = new String[]{ESIX_CONTACT_URL};
        ESIX_CONTACT_URLS_ENUM = makeArray2Enumeration(ESIX_CONTACT_URLS);
        ESIX_NAME_ADDRESS = new NameAddress(ESIX_USER_NAME
                , new SipURL(ESIX_USER_NAME, ESIX_STATIC_IP, ESIX_STATIC_PORT));
        //final static Pattern rexp = Pattern.compile("^"+ESIX_USER_NAME+"_[0-9*#+]+$");
        rexp = Pattern.compile("^[0-9*#+]+$");
        ESIX_EXPIRE_DATE = new Date(LocationServiceImpl.NEVER);
        MAX_USER = 10;
        IS_INTERNAL_SIP_PROXY = isInternalSipProxy;
    }
	Hashtable mUsers = new Hashtable(MAX_USER);
	public ESixLocationServiceImpl(String file_name) {
		this();
	}
	public ESixLocationServiceImpl(){
		
	}

	public boolean hasUser(String user){
        if(_isAllNumber(user)){
            return true;
        }
        if(!mUsers.containsKey(user)){
            if(user.endsWith("@192.168.43.1")){
                user = user + "@sipserver";
            }else if(user.endsWith("@sipserver")){
                user = user + "@192.168.43.1";
            }
        }
		return mUsers.containsKey(user);
	}
	private UserBindingInfo getUserBindingInfo(String user) {
        if(!mUsers.containsKey(user)){
            if(user.endsWith("@192.168.43.1")){
                user = user + "@sipserver";
            }else if(user.endsWith("@sipserver")){
                user = user + "@192.168.43.1";
            }
        }
		return (UserBindingInfo)mUsers.get(user);  
	 }
	@Override
	public void sync() {
	}

	@Override
	public int size() {
		return mUsers.size();
	}

	@Override
	public Enumeration getUsers() {		
		return mUsers.keys();
	}

    public String[] getAllNonESixUsers(){
        LinkedList<String> ret = new LinkedList<String>();
        for(Object k : mUsers.keySet()){
            if(_isEsix((String)k)) continue;
            ret.add((String)k);
        }
        return ret.toArray(new String[0]);
    }

	@Override
	public Repository addUser(String user) {
		if (hasUser(user)) return this;
        android.util.Log.d("ESixLocationServiceImpl", "add user:" + user);
	      UserBindingInfo ur=new UserBindingInfo(user);
	      mUsers.put(user,ur);
	      return this;
	}

	@Override
	public Repository removeUser(String user) {
		if (!hasUser(user)) return this;
        if(_isEsix(user)) return this;
        if(!mUsers.containsKey(user)){
            if(user.endsWith("@192.168.43.1")){
                user = user + "@sipserver";
            }else if(user.endsWith("@sipserver")){
                user = user + "@192.168.43.1";
            }
        }
	      mUsers.remove(user);
        android.util.Log.d("ESixLocationServiceImpl", "remove user:" + user);
        MainService.getSIPUAService().notifyClientStatusChanged(false, null);
	      return this;
	}

	@Override
	public Repository removeAllUsers() {
		mUsers.clear();
		return this;
	}

    public void removeAllNonESixUsers(){
        for(String u : getAllNonESixUsers()){
            removeUser(u);
        }
    }

	@Override
	public boolean hasUserContact(String user, String url) {
        if(!mUsers.containsKey(user)){
            if(user.endsWith("@192.168.43.1")){
                user = user + "@sipserver";
            }else if(user.endsWith("@sipserver")){
                user = user + "@192.168.43.1";
            }
        }
	  if (!hasUser(user)) return false;
	  if(_isAllNumber(user)){
		  return true;
	  }
      return getUserBindingInfo(user).hasContact(url);
	}

	@Override
	public LocationService addUserContact(String user, NameAddress contact,
			Date expire) {
        if(!mUsers.containsKey(user)){
            if(user.endsWith("@192.168.43.1")){
                user = user + "@sipserver";
            }else if(user.endsWith("@sipserver")){
                user = user + "@192.168.43.1";
            }
        }
		if (!hasUser(user)){
            addUser(user);
        }
        android.util.Log.d("ESixLocationServiceImpl", "add user contact:" + user + " for user " + user);
	      UserBindingInfo ur=getUserBindingInfo(user);
	      ur.replaceContact(contact, expire);
        if(!_isEsix(user)){
        android.util.Log.d("ESixLocationServiceImpl", "addUserContact:" + contact.getAddress().getHost() + ":" + contact.getAddress().getPort());
            MainService.getSIPUAService().notifyClientStatusChanged(true, contact.getAddress().getUserName());
        }
	    return this;
	}

	@Override
	public Enumeration getUserContactURLs(String user) {
        if(!mUsers.containsKey(user)){
            if(user.endsWith("@192.168.43.1")){
                user = user + "@sipserver";
            }else if(user.endsWith("@sipserver")){
                user = user + "@192.168.43.1";
            }
        }
		if (!hasUser(user)) return null;
		if(_isEsix(user) || _isAllNumber(user)){
			ESIX_CONTACT_URLS_ENUM.reset();
			return ESIX_CONTACT_URLS_ENUM;
		}
	    return getUserBindingInfo(user).getContacts();
	}

	@Override
	public LocationService removeUserContact(String user, String url) {
		if (!hasUser(user)) return this;
		if(_isEsix(user)) return this;
        MainService.getSIPUAService().notifyClientStatusChanged(false, null);
	      UserBindingInfo ur=getUserBindingInfo(user);
	      ur.removeContact(url);	      
	      return this;		
	}

	@Override
	public NameAddress getUserContactNameAddress(String user, String url) {
        if(!mUsers.containsKey(user)){
            if(user.endsWith("@192.168.43.1")){
                user = user + "@sipserver";
            }else if(user.endsWith("@sipserver")){
                user = user + "@192.168.43.1";
            }
        }
		if (!hasUser(user)) return null;
		if(_isEsix(user) || _isAllNumber(user)){
			return ESIX_NAME_ADDRESS;
		}
	      return getUserBindingInfo(user).getNameAddress(url);
	}

	@Override
	public Date getUserContactExpirationDate(String user, String url) {
        if(!mUsers.containsKey(user)){
            if(user.endsWith("@192.168.43.1")){
                user = user + "@sipserver";
            }else if(user.endsWith("@sipserver")){
                user = user + "@192.168.43.1";
            }
        }
		if (!hasUser(user)) return null;
		if(_isEsix(user) || _isAllNumber(user)){
			return ESIX_EXPIRE_DATE;
		}
	      return getUserBindingInfo(user).getExpirationDate(url);				
	}

    @Override
    public void setUserContactExpirationDate(String user, String url, Date date) {
        if(!mUsers.containsKey(user)){
            if(user.endsWith("@192.168.43.1")){
                user = user + "@sipserver";
            }else if(user.endsWith("@sipserver")){
                user = user + "@192.168.43.1";
            }
        }
        if(!hasUser(user)) return;
        if(_isEsix(user)){
            return;
        }
        getUserBindingInfo(user).setExpirationDate(url, date);
    }

    public ESixLocationServiceImpl(String user, String url, Date date) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
	public boolean isUserContactExpired(String user, String url) {
        if(!mUsers.containsKey(user)){
            if(user.endsWith("@192.168.43.1")){
                user = user + "@sipserver";
            }else if(user.endsWith("@sipserver")){
                user = user + "@192.168.43.1";
            }
        }
		if (!hasUser(user)) return true;
	      if(_isEsix(user) || _isAllNumber(user)){
	    	  return false;
	      }	      
	      return getUserBindingInfo(user).isExpired(url);
	}

	@Override
	public LocationService addUserStaticContact(String user,
			NameAddress name_addresss) {
		return addUserContact(user,name_addresss,new Date(LocationServiceImpl.NEVER));
	}

	@Override
	public boolean isUserContactStatic(String user, String url) {
		if(_isEsix(user)){
			return true;
		}
		return getUserContactExpirationDate(user,url).getTime()>=LocationServiceImpl.NEVER;
	}
	private static class MyEnumeration implements Enumeration{
		Object obj;
		public MyEnumeration(Object array){
			obj = array;
			size = Array.getLength(obj);
			cursor = 0;
		}
		public void reset(){
			cursor = 0;
		}
		int size;
		int cursor;

        public boolean hasMoreElements() {
          return (cursor < size);
        }

        public Object nextElement() {
          return Array.get(obj, cursor++);
        }
      }
	static public MyEnumeration makeArray2Enumeration(final Object obj) {
	    Class type = obj.getClass();
	    if (!type.isArray()) {
	      throw new IllegalArgumentException(obj.getClass().toString());
	    } else {
	      return (new MyEnumeration(obj));
	    }
	  }
}
