package local.ua;

import android.util.Log;

import java.util.Enumeration;
import java.util.LinkedList;
import java.util.Vector;

import com.womate.esix.MainService;
import local.media.AndroidAudioApp;
import local.media.MediaDesc;

import local.server.ESixLocationServiceImpl;
import local.server.Proxy;
import org.zoolu.sdp.AttributeField;
import org.zoolu.sdp.MediaDescriptor;
import org.zoolu.sdp.SessionDescriptor;
import org.zoolu.sip.address.NameAddress;
import org.zoolu.sip.call.Call;
import org.zoolu.sip.call.CallWatcher;
import org.zoolu.sip.call.ExtendedCall;
import org.zoolu.sip.message.Message;
import org.zoolu.sip.message.MessageFactory;
import org.zoolu.sip.message.SipMethods;
import org.zoolu.sip.provider.MethodId;
import org.zoolu.sip.provider.SipProvider;
import org.zoolu.sip.provider.SipProviderListener;
import org.zoolu.sip.transaction.TransactionClient;
import org.zoolu.sip.transaction.TransactionClientListener;
import org.zoolu.sip.transaction.TransactionServer;

public class ESixUserAgent extends UserAgent implements SipProviderListener, TransactionClientListener {
    final static String TAG = "ESIXUserAgent";
    Proxy mProxy;
    LinkedList<TransactionServer> mSMSTransServers;

    public ESixUserAgent(Proxy proxy, SipProvider sip_provider, UserAgentProfile ua_profile,
            UserAgentListener listener){
        super(sip_provider, ua_profile, listener);
        this.sip_provider.addSelectiveListener(new MethodId(SipMethods.MESSAGE),this);
        mProxy = proxy;
        mSMSTransServers = new LinkedList<TransactionServer>();
    }
    public void onCallTimeout(Call call){
        super.onCallTimeout(call);
        MainService.getSIPUAService().notifyClientStatusChanged(false, "");
    }
    public void onNewIncomingCall(CallWatcher call_watcher, ExtendedCall call,
            NameAddress callee, NameAddress caller, String sdp, Message invite) {
        Log.d(TAG, String.format("ESixUserAgent-newIncomingCall:%s -> %s",
                        caller.toString(), callee.toString()));
        this.call = call;
        Log.d(TAG, "new incoming call's sdp:" + call.getRemoteSessionDescriptor());
        //call.ring();

        Vector media_descs = null;
        if (sdp != null) {
            Vector md_list = (new SessionDescriptor(sdp)).getMediaDescriptors();
            media_descs = new Vector(md_list.size());
            for (int i = 0; i < md_list.size(); i++)
                media_descs.addElement(new MediaDesc((MediaDescriptor) md_list
                        .elementAt(i)));
        }
        if (listener != null)
            listener.onUaIncomingCall(this, callee, caller, media_descs);
    }

    public void ring(){
        Log.d(TAG, "ring()");
        if(call != null){
            if(call.isActive()){
                Log.d(TAG, "ignore ring() when call in active");
                return;
            }
            int port = AndroidAudioApp.newAudioStream();
            UserAgent.changeAudioLocalPort(ua_profile.media_descs, port);
            super.ring(ua_profile.media_descs);
        }
    }

    public void callExternalUser(String user){
        Log.d(TAG, "call external user:" + user);
        this.media_descs = ua_profile.media_descs;
        SessionDescriptor local_sdp=getSessionDescriptor(media_descs, AndroidAudioApp.newAudioStream());
        NameAddress calleeNameAddr = super.completeNameAddress(user);
        call=new ExtendedCall(sip_provider, ua_profile.getUserURI(), ua_profile.auth_user,ua_profile.auth_realm,ua_profile.auth_passwd,this);
        call.call(calleeNameAddr, null, local_sdp.toString());
    }
    public void callInternalUser(String callerTelNum){
        Log.d(TAG, "call internal user:" + callerTelNum);
        Vector media_descs = null;

        // new media description
        if (media_descs==null) media_descs=ua_profile.media_descs;
        this.media_descs=media_descs;
        // new call
        NameAddress callerNameAddr = getInternalLocalUser(callerTelNum);
        String calleeName = getBestInternalRemoteUserName();//getBestInternalRemoteUser();
        if(calleeName == null){
            Log.e(TAG, "no callee found, leave it ring an do nothing");
            //if(listener != null) listener.onUaCallClosed(this);
            return;
        }
        call=new ExtendedCall(sip_provider, callerNameAddr, new NameAddress(ESixLocationServiceImpl.ESIX_CONTACT_URL), ua_profile.auth_user,ua_profile.auth_realm,ua_profile.auth_passwd,this);
        SessionDescriptor local_sdp=getSessionDescriptor(media_descs, AndroidAudioApp.newAudioStream());
        call.call(completeNameAddress(calleeName), null, local_sdp.toString());
        Log.d(TAG, "call() done");
    }

    public void accept(){
        if(this.media_sessions.size() <= 0){
            int port = AndroidAudioApp.newAudioStream();
            UserAgent.changeAudioLocalPort(ua_profile.media_descs, port);
        }
        if(call.isActive()){
            Log.d(TAG, "ignore accept() when call in active");
            return;
        }
        accept(ua_profile.media_descs);
    }
    private SessionDescriptor getSessionDescriptor(Vector media_descs, int port){
        String owner=ua_profile.user;
        String media_addr=(ua_profile.media_addr!=null)? ua_profile.media_addr : sip_provider.getViaAddress();
        int media_port=port;
        SessionDescriptor sd=new SessionDescriptor(owner,media_addr);
        sd.addAttribute(new AttributeField("sendrecv"));
        sd.addAttribute(new AttributeField("ptime","20"));
        sd.addAttribute(new AttributeField("silenceSupp","off - - - -"));
        for (int i=0; i<media_descs.size(); i++)
        {  MediaDesc md=(MediaDesc)media_descs.elementAt(i);
            // check if audio or video have been disabled
            if (md.getMedia().equalsIgnoreCase("audio") && !ua_profile.audio) continue;
            if (md.getMedia().equalsIgnoreCase("video") && !ua_profile.video) continue;
            // else
            if (media_port>0)
            {  // override the media_desc port
                md.setPort(media_port);
                media_port+=2;
            }
            sd.addMediaDescriptor(md.toMediaDescriptor());
        }
        return sd;
    }

    private String sendMessageWithSenderNumber(NameAddress from_url, NameAddress to_url, String time, String content){
        Log.d(TAG, "send message:" + content);
        if(to_url == null){
            Log.e(TAG, "no local remote found");
            return "";
        }
        Log.d(TAG, "to:" + to_url);
        Message req=MessageFactory.createMessageRequest(sip_provider,to_url,from_url,time,"application/text",content);
        TransactionClient t=new TransactionClient(sip_provider,req,this);
        t.request();
        return t.getTransactionId().toString();
    }

    public String sendMessageWithSenderNumber(String senderNumber, String time, String content){
        //NameAddress callee = getBestInternalRemoteUserContact();
        String callee = getBestInternalRemoteUserName();
        if(callee == null){
            Log.e(TAG, "no sip client connected. send message fail");
            return null;
        }
        NameAddress calleeAddr = completeNameAddress(callee);
        return sendMessageWithSenderNumber(getInternalLocalUser(senderNumber), calleeAddr, time, content);
    }

    private NameAddress getInternalLocalUser(String senderOrCaller){
        return new NameAddress("sip:"+senderOrCaller+"@"+ESixLocationServiceImpl.ESIX_STATIC_IP+":"+ESixLocationServiceImpl.ESIX_STATIC_PORT);
    }
    private String getBestInternalRemoteUserName(){
        //TODO:esix-match best callee?
        //NameAddress ret = null;
        if(mProxy == null){
            return null;
        }
        if(!MainService.getSIPUAService().isSipClientConnected()){
            return null;
        }
        for (String user : mProxy.getNonESixUsers()){
            for(Enumeration ul = mProxy.getUserContactURLs(user); ul.hasMoreElements(); ){
                String url = (String)ul.nextElement();
                if(!mProxy.isContactExipred(user, url)){
                    return user;
                }
            }
        }
        //try my best to find callee, even expired.
        return null;
    }
    private NameAddress getBestInternalRemoteUserContact(){
        //TODO:esix-match best callee?
        NameAddress ret = null;
        if(mProxy == null){
            return null;
        }
        for (String user : mProxy.getNonESixUsers()){
            for(Enumeration ul = mProxy.getUserContactURLs(user); ul.hasMoreElements(); ){
                String url = (String)ul.nextElement();
                ret = new NameAddress(url);
                break;
            }
        }
        return ret;
    }
    public void reportMessageReceived(String id, boolean success){
        Log.d(TAG, "reportMessageReceived:" + id + " " + success);
        for(TransactionServer ts : mSMSTransServers){
            if(ts.getTransactionId().toString().equals(id)){
                Log.d(TAG, "current transaction status:" + ts.getStatus());
                if(success){
                    Log.d(TAG, "response message with 200");
                    ts.respondWith(200);
                }else{
                    Log.d(TAG, "response message with 486");
                    ts.respondWith(486);//busy here
                }
                mSMSTransServers.remove(ts);
                return;
            }
        }
        //report first
        for(TransactionServer ts : mSMSTransServers){
            if(success){
                Log.d(TAG, "response message with 200");
                ts.respondWith(200);
            }else{
                Log.d(TAG, "response message with 486");
                ts.respondWith(486);//busy here
            }
            mSMSTransServers.remove(ts);
            return;
        }
        Log.e(TAG, "unknown id to response:" + id);
    }
    @Override
    public void onReceivedMessage(SipProvider sip_provider, Message msg) {
        Log.d(TAG, "Message received: "+ msg.getFirstLine().substring(0,msg.toString().indexOf('\r')));
        if (msg.isRequest() && msg.isMessage()){
            TransactionServer ts = new TransactionServer(sip_provider,msg,null);
            mSMSTransServers.add(ts);
            Log.d(TAG, "send trying in status:" + ts.getStatus());
            ts.respondWith(MessageFactory.createResponse(msg, 100, null, null));//trying
            //NameAddress sender=msg.getFromHeader().getNameAddress();
            NameAddress recipient=msg.getToHeader().getNameAddress();
            //String subject=null;
            //if (msg.hasSubjectHeader()) subject=msg.getSubjectHeader().getSubject();
            //String content_type=null;
            //if (msg.hasContentTypeHeader()) content_type=msg.getContentTypeHeader().getContentType();
            String content=msg.getBody();
            if (listener!=null) listener.onMessageReceived(this, recipient, content, ts.getTransactionId().toString());
        }
    }

    @Override
    public void onTransProvisionalResponse(TransactionClient tc, Message resp) {
        Log.v(TAG, "Message provisional response " + resp.toString());
    }

    @Override
    public void onTransSuccessResponse(TransactionClient tc, Message resp) {
        Log.d(TAG, "Message successfully delivered ("+ resp.getStatusLine().getReason() +").");
        Message req=tc.getRequestMessage();
        NameAddress recipient=req.getToHeader().getNameAddress();
        //String subject=null;
        //if (req.hasSubjectHeader()) subject=req.getSubjectHeader().getSubject();
        if (listener!=null) listener.onMessageDeliverySuccess(this, recipient, tc.getTransactionId().toString());
    }

    @Override
    public void onTransFailureResponse(TransactionClient tc, Message resp) {
        Log.d(TAG, "Message delivery failed ("+ resp.getStatusLine().getReason()+").");
        Message req=tc.getRequestMessage();
        NameAddress recipient=req.getToHeader().getNameAddress();
        //String subject=null;
        //if (req.hasSubjectHeader()) subject=req.getSubjectHeader().getSubject();
        if (listener!=null) listener.onMessageDeliveryFailure(this, recipient, tc.getTransactionId().toString());
    }

    @Override
    public void onTransTimeout(TransactionClient tc) {
        Log.d(TAG, "Message delivery failed (timeout)");
        Message req=tc.getRequestMessage();
        NameAddress recipient=req.getToHeader().getNameAddress();
        //String subject=null;
        //if (req.hasSubjectHeader()) subject=req.getSubjectHeader().getSubject();
        if (listener!=null) listener.onMessageDeliveryFailure(this, recipient, tc.getTransactionId().toString());
    }

}
