package com.womate.esix.service;

import java.util.ArrayList;
import java.util.List;

public interface CallService {
    public static class Connection{
        public String orgDialNum;
        public String Num;
    }
    public static class CallInfo{
        public int state;
        public List<Connection> cons = new ArrayList<Connection>(7);
    }
    public static class Calls{
        public CallInfo Act = new CallInfo();
        public CallInfo Hld = new CallInfo();
        public CallInfo Wnt = new CallInfo();
    }
    public static final int CALL_ACT = 0;
    public static final int CALL_HLD = 1;
    public static final int CALL_WNT = 2;

    public static final int CALL_S_IDLE = 0;
    public static final int CALL_S_ACTIVE = 2;
    public static final int CALL_S_HOLD = 3;
    public static final int CALL_S_DAILING = 4;
    public static final int CALL_S_ALERTING = 5;
    public static final int CALL_S_INCOMING = 6;

    public CallInfo queryAct();
    public CallInfo queryHld();
    public CallInfo queryWnt();
    public void dial(String num);
    public void sendDtmf(char c);
    public void onDtmfReceived(int c);
    public SIPUAService.CallListenerBase getSIPUAListener();
}
