package com.womate.esix.service;

import java.util.Date;

public interface DisplayService {
    public interface Ind{//persist status
        public enum CellularDataSignalType{
            GRPS,EDGE,WCDMA,CDMA,EVDO,TDSCDMA,NONE
        }
        public enum WifiType{
            AP,STA,AP_STA,NONE
        }
        public enum CelluarNetworkStatus{
            SEARCHING, NOSIM, INSERVICE
        }
        public class WifiStatus{
            public WifiType type;
            public int apClientCount ;
            public int staSignalLevel;
            public String staSSID = "";
            public String staStatus = "";
            public String toString(){
                return String.format("%s,%d,%d,%s,%s", type.toString(), apClientCount,staSignalLevel,staSSID, staStatus);
            }

            public boolean equals(WifiStatus ws){
                if(ws == null) return false;
                return this.type == ws.type  && ws.apClientCount == this.apClientCount && ws.staSignalLevel == this.apClientCount
                        && ws.staSSID.equals(this.staSSID) && ws.staStatus.equals(this.staSSID);
            }

            public WifiStatus clone(){
                WifiStatus ws = new WifiStatus();
                ws.type = this.type;
                ws.apClientCount = this.apClientCount;
                ws.staSignalLevel = this.staSignalLevel;
                ws.staSSID = this.staSSID;
                ws.staStatus = this.staStatus;
                return ws;
            }
        }
        public class CellularStatus{
            public CellularDataSignalType dataType;
            public CelluarNetworkStatus networkStatus;
            public int signalLevel;
            public String PLMN;
        }
    }

    public void cellular(Ind.CellularStatus status);
    public void battery(int level);
    public void wifi(Ind.WifiStatus status);
    public void sipClientConnect(boolean isConnect, String name);
    public void refresh(boolean onOff);
    public void notify(int sms, int call);
    public void mobileDataTraffic(String traffic);
}
