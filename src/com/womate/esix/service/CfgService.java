package com.womate.esix.service;

import android.util.Log;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Field;

/**
 * Created with IntelliJ IDEA.
 * User: jan4984@gmail.com
 * Date: 5/28/13
 * Time: 2:32 PM
 * copyright belongs to jan4984@gmail.com
 */
public interface CfgService {
    @SuppressWarnings("unused")
    public static class CFG implements Serializable {
        public boolean isInternalSIPProxyOn = true;
        public String wifiAPName = "";
        public String wifiAPPassword = "11111111";
        public String wifiSTACfgAPName = "";
        public String wifiSTACfgAPPassword = "";
        public String externalSIPProxyAddr = "";
        public int externalSIPProxyPort = 5060;
        public String externalSIPUsername = "";
        public String externalSIPPassword = "";
        public String externalSIPUsernameRemote = "";
        public String wifiApDefaultName = "";

        public static JSONObject toJSON(CFG cfg){
            JSONObject ret = new JSONObject();
            try {
                for(Field f : CFG.class.getDeclaredFields()){
                    if(f.getType().equals(String.class)){
                        ret.put(f.getName(), f.get(cfg));
                    }else if(f.getType().equals(boolean.class)){
                        ret.put(f.getName(),String.valueOf(f.getBoolean(cfg)));
                    }else if(f.getType().equals(int.class)){
                        ret.put(f.getName(), String.valueOf(f.getInt(cfg)));
                    }else{
                        Log.e("Cfg2Json", "unknown how to process type " + f.getType() + " of configuration filed");
                    }
                }
            } catch (Exception e) {
                Log.e("Cfg2Json", "", e);
            }
            return ret;
        }
        public static CFG fromJSON(JSONObject obj){
            CFG ret = new CFG();
            try {
                for(Field f : CFG.class.getDeclaredFields()){
                    if(f.getType().equals(String.class)){
                        f.set(ret, obj.getString(f.getName()));
                    }else if(f.getType().equals(boolean.class)){
                        f.set(ret, obj.getBoolean(f.getName()));
                    }else if(f.getType().equals(int.class)){
                        f.set(ret, obj.getInt(f.getName()));
                    }else{
                        Log.e("Cfg2Json", "unknown how to process type " + f.getType() + " of configuration filed");
                    }
                }
            } catch (Exception e) {
                Log.e("Cfg2Json", "", e);
            }
            return ret;
        }
    }

    public void saveCfg(CFG cfg);
    public CFG readCfg();
    public void resetAp();
}
