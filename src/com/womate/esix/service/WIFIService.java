package com.womate.esix.service;

import android.os.Handler;

import java.net.InetAddress;

public interface WIFIService {
    public void startWIFIAp();
    public boolean isWIFIAPReady();
    //if there are wifi client configuration and wifi-ap present, connect to wifi-ap. or switch self to a wifi-ap
    public void turnOn();
    //ap ready, or wifi client connected to a wifi-ap
    public boolean isWIFIReady();
    public boolean isWIFISTAConnected();
    public String getWIFIAddress();
    public String getWIFISTAApName();
    //public int getStaSignalLevel();
}
