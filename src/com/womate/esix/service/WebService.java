package com.womate.esix.service;

import android.os.Handler;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: jan4984@gmail.com
 * Date: 5/13/13
 * Time: 3:13 PM
 * copyright belongs to jan4984@gmail.com
 */
public interface WebService {
    public interface WebSession{
        public void sendWebsockEvent(JSONObject evt) throws IOException;
        public boolean isGuest();
        public String getIPAddr();
        public int getPort();
    }

    public static class EventValue{
        public EventValue(){

        }
        public EventValue(JSONObject j, WebSession w){
            jo = j;
            ws = w;
        }
        public JSONObject jo;
        public WebSession ws;
    }

    public static enum Events{
        EVT_START ( 1000),

        EVT_GROUP_CLIENT_START   (1100),
        EVT_CLIENT_CONNECTED( 1100 + 1),
        EVT_CLIENT_DISCONNECTED   ( 1100 + 2),
        EVT_CLIENT_USER_LOGIN    ( 1100 + 3),
        EVT_CLIENT_USER_QUERY    ( 1100 + 4),
        EVT_GROUP_CLIENT_END     ( 1100 + 99),

        EVT_GROUP_WIFI_AP_CFG_START      ( 1200),
        EVT_SET_WIFI_AP                  ( 1200 + 1),
        EVT_QUERY_WIFI_AP                ( 1200 + 2),
        EVT_GROUP_CFG_END                ( 1200 + 99),

        EVT_GROUP_WIFI_STA_CFG_START    ( 1300),
        EVT_QUERY_WIFI_STA_ONOFF        ( 1300 + 1),
        EVT_WIFI_STA_SCAN               ( 1300 + 2),
        EVT_WIFI_STA_GET_SAVED_NETWORKS ( 1300 + 3),
        EVT_WIFI_STA_CONNECT( 1300 + 4),
        EVT_WIFI_STA_FORGET_NETWORK     ( 1300 + 5),
        EVT_SET_WIFI_STA_ONOFF          ( 1300 + 6),
        EVT_GROUP_WIFI_STA_CFG_END      ( 1300 + 99),

        EVT_GROUP_APN_CFG_START  ( 1400),
        EVT_QUERY_APN            ( 1400 + 1),
        EVT_APPLY_APN            ( 1400 + 2),
        EVT_GROUP_APN_CFG_END    ( 1400 + 99),

        EVT_GROUP_MT_CFG_START   ( 1500),
        EVT_QUERY_MT             ( 1500 + 1),
        EVT_RESET_MT             ( 1500 + 2),
        EVT_SET_MD_ONOFF         ( 1500 + 3),
        EVT_QUERY_MD_ONOFF       ( 1500 + 4),
        EVT_GROUP_MT_CFG_END     ( 1500 + 99),

        EVT_GROUP_FW_CFG_START   ( 1600),
        EVT_QUERY_FW             ( 1600 + 1),
        EVT_UPDATE_FW            ( 1600 + 2),
        EVT_RESET_DEV            ( 1600 + 3),
        EVT_GROUP_FW_CFG_END     ( 1600 + 99);

        public int getValue(){
            return mV;
        }
        private int mV = -1;
        private Events(int v){
            mV = v;
        }
        public static Events fromString(String str) throws IllegalArgumentException{
            return Events.valueOf(Events.class, str);
        }

        public static Events fromInt(int i) throws IllegalArgumentException{
            for(Events e : Events.values()){
                if(e.getValue() == i){
                    return e;
                }
            }

            throw new IllegalArgumentException("no value for " + i);
        }
    }
    /*
    public static class Events{
        public static final int EVT_START = 1000;

        public static final int EVT_GROUP_CLIENT_START   = EVT_START + 100;
        public static final int EVT_CLIENT_CONNECTED      = EVT_GROUP_CLIENT_START + 1;
        public static final int EVT_CLINT_DISCONNECTED   = EVT_GROUP_CLIENT_START + 2;
        public static final int EVT_CLIENT_USER_LOGIN    = EVT_GROUP_CLIENT_START + 3;
        public static final int EVT_CLIENT_USER_QUERY    = EVT_GROUP_CLIENT_START + 4;
        public static final int EVT_GROUP_CLIENT_END     = EVT_GROUP_CLIENT_START + 99;

        public static final int EVT_GROUP_WIFI_AP_CFG_START      = EVT_START + 200;
        public static final int EVT_SAVE_AND_APPLY_WIFI_AP_CFG   = EVT_GROUP_WIFI_AP_CFG_START + 1;
        public static final int EVT_CFG_QUERY_WIFI_AP            = EVT_GROUP_WIFI_AP_CFG_START + 2;
        public static final int EVT_GROUP_CFG_END                = EVT_GROUP_WIFI_AP_CFG_START + 99;

        public static final int EVT_GROUP_WIFI_STA_CFG_START    = EVT_START + 300;
        public static final int EVT_QUERY_WIFI_STA_ONOFF             = EVT_GROUP_WIFI_STA_CFG_START + 1;
        public static final int EVT_WIFI_STA_SCAN               = EVT_GROUP_WIFI_STA_CFG_START + 2;
        public static final int EVT_WIFI_STA_GET_SAVED_NETWORKS = EVT_GROUP_WIFI_STA_CFG_START + 3;
        public static final int EVT_WIFI_STA_CONNECT = EVT_GROUP_WIFI_STA_CFG_START + 4;
        public static final int EVT_WIFI_STA_FORGET_NETWORK     = EVT_GROUP_WIFI_STA_CFG_START + 5;
        public static final int EVT_GROUP_WIFI_STA_CFG_END      = EVT_GROUP_WIFI_STA_CFG_START + 99;

        public static final int EVT_GROUP_APN_CFG_START  = EVT_START + 400;
        public static final int EVT_QUERY_APN            = EVT_GROUP_APN_CFG_START + 1;
        public static final int EVT_APPLY_APN_           = EVT_GROUP_APN_CFG_START + 2;
        public static final int EVT_GROUP_APN_CFG_END    = EVT_GROUP_APN_CFG_START + 99;

        public static final int EVT_GROUP_MT_CFG_START   = EVT_START + 500;
        public static final int EVT_QUERY_MT             = EVT_GROUP_MT_CFG_START + 1;
        public static final int EVT_RESET_MT_            = EVT_GROUP_MT_CFG_START + 2;
        public static final int EVT_GROUP_MT_CFG_END     = EVT_GROUP_MT_CFG_START + 99;

        public static final int EVT_GROUP_FW_CFG_START   = EVT_START + 600;
        public static final int EVT_QUERY_FW             = EVT_GROUP_FW_CFG_START + 1;
        public static final int EVT_UPDATE_FW_           = EVT_GROUP_FW_CFG_START + 2;
        public static final int EVT_GROUP_FW_CFG_END     = EVT_GROUP_FW_CFG_START + 99;
    }
    */
    public WebSession[] querySessions();
    public void addEventHandler(Events evtGroup, Handler listener, int sockEvt);
    public void sendFirewareUpdateFile(String filePath) throws JSONException;
    public void broadcastWebsockEvent(JSONObject evt);
    public void start(String host);
}
