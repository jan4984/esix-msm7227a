package com.womate.esix.service;

import java.util.Date;

//time
//battery low
//battery changed
//SCREEN ON|OFF
//key press?
//lcd light?
public interface MiscService {
    public void setTime(String time);
    public long getTotalMT();
    public long getCurBootMT();
}
