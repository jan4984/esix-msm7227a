package com.womate.esix.service.impl;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.womate.esix.MainService;
import com.womate.esix.service.WebService;
import org.java_websocket.WebSocket;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jan4984@gmail.com
 * Date: 5/13/13
 * Time: 4:34 PM
 * copyright belongs to jan4984@gmail.com
 */
public class WebServiceImpl implements WebService {
    static final String TAG ="ESix-WebService";
    static final String WEB_DIR = "/webservice";
    public static final int MSG_NEW_WEB_SOCK_CONN = 2;
    public static final int MSG_CLOSE_WEB_SOCK_CONN = 3;
    public static final int MSG_NEW_WEBSOCK_MSG = 4;
    public static final int MSG_WEBSOCK_PING = 5;

    SimpleWebServer mWebServer = null;
    WebSockServer mWebSockServer = null;
    Handler mHandler = null;
    ArrayList<WebSession> mSessions = null;
    static class HandlerData{
        public Handler h;
        public int evt;

        public HandlerData(Handler hdl, int sockEvt) {
            h = hdl;
            evt = sockEvt;
        }
    }
    HashMap<Events, List<HandlerData>> mEvtHandles = null;

    public WebServiceImpl(){
        mHandler = new MyHandler();
        mEvtHandles = new HashMap<Events, List<HandlerData>>(5);
        mSessions = new ArrayList<WebSession>(5);
    }

    @Override
    public synchronized WebSession[] querySessions() {
        return mSessions.toArray(new WebSession[0]);
    }

    @Override
    public synchronized void addEventHandler(Events evtGroup, Handler listener, int sockEvt) {
        List<HandlerData> lns = mEvtHandles.get(evtGroup);
        if(lns == null){
            lns = new ArrayList<HandlerData>(1);
            mEvtHandles.put(evtGroup, lns);
        }
        lns.add(new HandlerData(listener, sockEvt));
    }

    static final String UPDATE_FILE_IN = "/data/efive-update.bin.tmp";
    static final String UPDATE_FILE_OUT = "/data/efive-update.bin";
    @Override
    public void sendFirewareUpdateFile(String filePath) throws JSONException{
        try{
            File fin = new File(filePath);
            File fout = new File(UPDATE_FILE_IN);
            copyFile(fin, fout);
        }catch(IOException ioe){
            Log.e(TAG, "coping update file", ioe);
        }
        fireEvt(Events.EVT_UPDATE_FW, new MyWebSession(null), new JSONObject("{input:\"" + UPDATE_FILE_IN + "\",output:\"" +  UPDATE_FILE_OUT + "\"}"));
    }

    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if(!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        }
        finally {
            if(source != null) {
                source.close();
            }
            if(destination != null) {
                destination.close();
            }
        }
    }

    @Override
    public void broadcastWebsockEvent(JSONObject evt) {
        for(WebSession ws : mSessions){
            try{
            ws.sendWebsockEvent(evt);
            }catch(Exception e){
                Log.e(TAG, "broadcasting event", e);
            }
        }
    }

    @Override
    public void start(String host) {
        try {
            mWebServer = new SimpleWebServer(host, 80, new File(MainService.WORK_DIR + WEB_DIR));
            Log.d(TAG, "new web server at port 80, in directory:" + MainService.WORK_DIR + WEB_DIR);
            InetSocketAddress websockAddr = null;
            websockAddr = new InetSocketAddress(InetAddress.getByName(host), 2211);
            Log.d(TAG, "new web sock server at port 2211");
            mWebSockServer = new WebSockServer(mHandler, websockAddr);
            mWebServer.start();
            mWebSockServer.start();
        } catch (Exception e) {
            Log.e(TAG, "starting web server", e);
        }
    }

    private void fireEvt(Events evt, WebSession ws, JSONObject jo){
        List<HandlerData> evtHandlers = null;
        int what = evt.getValue();
        if(what > Events.EVT_GROUP_CLIENT_START.getValue() && what < Events.EVT_GROUP_CLIENT_END.getValue()){
            evtHandlers = mEvtHandles.get(Events.EVT_GROUP_CLIENT_START);
        }else if(what > Events.EVT_GROUP_WIFI_AP_CFG_START.getValue() && what < Events.EVT_GROUP_CFG_END.getValue()){
            evtHandlers = mEvtHandles.get(Events.EVT_GROUP_WIFI_AP_CFG_START);
        }else if(what > Events.EVT_GROUP_WIFI_STA_CFG_START.getValue() && what < Events.EVT_GROUP_WIFI_STA_CFG_END.getValue()){
            evtHandlers = mEvtHandles.get(Events.EVT_GROUP_WIFI_STA_CFG_START);
        }else if(what > Events.EVT_GROUP_APN_CFG_START.getValue() && what < Events.EVT_GROUP_APN_CFG_END.getValue()){
            evtHandlers = mEvtHandles.get(Events.EVT_GROUP_APN_CFG_START);
        }else if(what > Events.EVT_GROUP_MT_CFG_START.getValue() && what < Events.EVT_GROUP_MT_CFG_END.getValue()){
            evtHandlers = mEvtHandles.get(Events.EVT_GROUP_MT_CFG_START);
        }else if(what > Events.EVT_GROUP_FW_CFG_START.getValue() && what < Events.EVT_GROUP_FW_CFG_END.getValue()){
            evtHandlers = mEvtHandles.get(Events.EVT_GROUP_FW_CFG_START);
        }

        if(evtHandlers == null){
            Log.e(TAG, "can not find event handler for " + evt);
            return;
        }
        for(HandlerData d:evtHandlers){
            Message msg = Message.obtain();
            msg.what = d.evt;
            msg.arg1 = what;
            if(jo == null){
                jo = new JSONObject();
            }
            msg.obj = new EventValue(jo, ws);
            Log.d(TAG, "send message " + d.evt + "to " + ws);
            d.h.sendMessage(msg);
        }
    }

    private class MyWebSession implements WebSession{
        private WebSocket mWebSock = null;
        public MyWebSession(WebSocket websock){
            mWebSock = websock;
        }
        @Override
        public void sendWebsockEvent(JSONObject evt) throws  IOException{
            if(mWebSock != null){
                Log.d(TAG, "sending " + evt + " to " + mWebSock.getRemoteSocketAddress());
            }else{
                Log.d(TAG, "web socket empty, ignore " + evt);
                return;
            }
            mWebSock.send(evt.toString());
        }

        @Override
        public boolean isGuest() {
            return false;
        }

        @Override
        public String getIPAddr() {
            if(mWebSock == null) return "";
            return mWebSock.getRemoteSocketAddress().getAddress().toString();
        }

        @Override
        public int getPort() {
            if(mWebSock == null) return -1;
            return mWebSock.getRemoteSocketAddress().getPort();
        }

        @Override
        public boolean equals(Object obj){
            if(obj != null){
                if(obj instanceof  MyWebSession){
                    return ((MyWebSession)obj).mWebSock == this.mWebSock;
                }
            }
            return false;
        }

        @Override
        public String toString(){
            if(mWebSock == null)
                return "WebSession-Dummy";
            else
                return "WebSession@" + mWebSock.toString();
        }
    }
    private WebSession findByWebSock(WebSocket websock){
        MyWebSession tmp = new MyWebSession(websock);
        for(WebSession ws : mSessions){
            if(ws.equals(tmp)){
                return ws;
            }
        }
        return null;
    }

    private class MyHandler extends Handler{
        JSONObject joPING;

        private MyHandler() {
            try {
                joPING = new JSONObject("{cmd:EVT_PING, paras:\"\"}");
            } catch (JSONException e) {
                Log.e(TAG, "NO WAY");
            }
        }

        public void handleMessage(Message msg){
            try{
                Log.d(TAG, "handle " + msg.what);
                synchronized(WebServiceImpl.this){
                    switch(msg.what){
                        case MSG_WEBSOCK_PING:{
                            broadcastWebsockEvent(joPING);
                            this.sendEmptyMessageDelayed(MSG_WEBSOCK_PING, 10 * 1000);
                        }
                            break;
                        case MSG_NEW_WEBSOCK_MSG:{
                            WebSession ws = findByWebSock((WebSocket)(((Object[])msg.obj)[0]));
                            JSONObject jmsg = null;
                            if(((Object[])msg.obj)[1] != null){
                                jmsg = (JSONObject)(((Object[])msg.obj)[1]);
                            }
                            if(ws == null){
                                Log.e(TAG, "can not find web session form sessions to remove, PLEASE CHECK");
                            }else{
                                fireEvt(Events.fromInt(msg.arg1), ws, jmsg);
                            }
                        }
                            break;
                        case MSG_NEW_WEB_SOCK_CONN:{
                            WebSession ws = new MyWebSession((WebSocket)msg.obj);
                            mSessions.add(ws);
                            fireEvt(Events.EVT_CLIENT_CONNECTED, ws, null);
                            if(!this.hasMessages(MSG_WEBSOCK_PING)){
                                this.sendEmptyMessageDelayed(MSG_WEBSOCK_PING, 1000);
                            }
                        }
                            break;
                        case MSG_CLOSE_WEB_SOCK_CONN:{
                            WebSession ws = findByWebSock((WebSocket)msg.obj);
                            if(ws == null){
                                Log.e(TAG, "can not find web session form sessions to remove, PLEASE CHECK");
                            }else{
                                mSessions.remove(ws);
                            }
                            fireEvt(Events.EVT_CLIENT_DISCONNECTED, ws, null);
                            if(mSessions.size() <= 0){
                                this.removeMessages(MSG_WEBSOCK_PING);
                            }
                        }
                            break;
                        default:
                            Log.e(TAG, "unknow event for WebServiceImpl" + msg.what);
                            break;
                    }
                }
            }catch(Exception e){
                Log.e(TAG, "handling message:" + msg.what, e);
            }
        }
    }
}
