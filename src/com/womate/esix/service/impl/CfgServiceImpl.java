package com.womate.esix.service.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.util.Log;
import com.womate.esix.MainService;
import com.womate.esix.service.CfgService;
import com.womate.esix.service.ServiceFactory;
import com.womate.esix.service.WebService;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: jan4984@gmail.com
 * Date: 5/28/13
 * Time: 2:40 PM
 * copyright belongs to jan4984@gmail.com
 */
public class CfgServiceImpl implements CfgService{
    final static String PREF = "cfg";
    final static String TAG = "ESix-CfgService";

    private Handler mHandler;
    public CfgServiceImpl(){
        //mHandler = new MyHandler();
        //ServiceFactory.getWebService().addEventHandler(WebService.Events.EVT_GROUP_WIFI_AP_CFG_START, mHandler);
    }

    @Override
    public void saveCfg(CFG cfg) {
        SharedPreferences sp = MainService.getInstance().getSharedPreferences(PREF, Context.MODE_WORLD_READABLE);
        SharedPreferences.Editor editor = sp.edit();
        try {
            for(Field f : CFG.class.getDeclaredFields()){
                if(f.getType().equals(String.class)){
                    editor.putString(f.getName(), (String)(f.get(cfg)));
                }else if(f.getType().equals(boolean.class)){
                    editor.putBoolean(f.getName(), f.getBoolean(cfg));
                }else if(f.getType().equals(int.class)){
                    editor.putInt(f.getName(), f.getInt(cfg));
                }else{
                    Log.e(TAG, "unknown how to process type " + f.getType() + " of configuration filed");
                }
            }
        } catch (IllegalAccessException e) {
            Log.e(TAG, "saving cfg", e);
        }
        editor.commit();
    }

    @Override
    public CFG readCfg() {
        CFG cfg = new CFG();
        SharedPreferences sp = MainService.getInstance().getSharedPreferences(PREF, Context.MODE_WORLD_READABLE);
        try {
            for(Field f : CFG.class.getDeclaredFields()){
                if(f.getType().equals(String.class)){
                    f.set(cfg, sp.getString(f.getName(), (String)(f.get(cfg))));
                }else if(f.getType().equals(boolean.class)){
                    f.set(cfg, sp.getBoolean(f.getName(), f.getBoolean(cfg)));
                }else if(f.getType().equals(int.class)){
                    f.set(cfg, sp.getInt(f.getName(), f.getInt(cfg)));
                }else{
                    Log.e(TAG, "unknown how to process type " + f.getType() + " of configuration filed");
                }
            }
        } catch (IllegalAccessException e) {
            Log.e(TAG, "saving cfg", e);
        }

        if(cfg.wifiApDefaultName.equals("") || cfg.wifiApDefaultName.equals("MATCH--xxxxxx")){
            //assume first time at startup
            cfg.wifiApDefaultName = "MATCH--" + get6Random();
            SharedPreferences.Editor edt = sp.edit();
            edt.putString("wifiApDefaultName", cfg.wifiApDefaultName);
            cfg.wifiAPName = cfg.wifiApDefaultName;
            edt.putString("wifiAPName", cfg.wifiAPName);
            cfg.wifiAPPassword = "11111111";
            edt.putString("wifiAPPassword", cfg.wifiAPPassword);
            Log.d(TAG, "generated a new AP name:" + cfg.wifiApDefaultName);
            edt.commit();
        }
        return cfg;
    }

    @Override
    public void resetAp() {
        CFG c = readCfg();
        c.wifiAPName = c.wifiApDefaultName;
        c.wifiAPPassword = "11111111";
        saveCfg(c);
        MainService.getInstance().reboot("resetap", 1000);
    }

    static final char[] ABC_123 = new char[10 + 26 + 26];
    static {
        for (int idx = 0; idx < 10; ++idx)
            ABC_123[idx] = (char) ('0' + idx);
        for (int idx = 10; idx < 10 + 26; ++idx)
            ABC_123[idx] = (char) ('a' + idx - 10);
        for (int idx = 10 + 26; idx < 10 + 26 + 26; ++idx)
            ABC_123[idx] = (char) ('A' + idx - 10 - 26);
    }
    private static String get6Random(){
        try{
            long cur = System.nanoTime();
            Random rnd = new Random(cur ^ System.identityHashCode(MainService.getInstance()));
            char[] rnds = new char[6];
            for(int i = 0; i < 6; i++){
                rnds[i] = ABC_123[rnd.nextInt(ABC_123.length)];
            }
            return new String(rnds);
        }catch(Exception e)
        {
            Log.e(TAG, "generating random for AP", e);
        }
        return "xxxxxx";
    }

    /*
    private class MyHandler extends Handler{
        public void handleMessage(Message msg){
            WebService.Events evt = null;
            try{
                evt = WebService.Events.fromInt(msg.what);
            }catch(Exception e){
                Log.e(TAG, "convert message", e);
                return;
            }
            Log.d(TAG, "hanlding " + evt);
            if(evt == WebService.Events.EVT_CFG_QUERY_WIFI_AP){
                CFG cfg = readCfg();
                Object[] params = (Object[])msg.obj;
                WebService.WebSession ws = (WebService.WebSession)params[0];
                try {
                    ws.sendWebsockEvent(new JSONObject().put("cfg_query_all_ok", CFG.toJSON(cfg)));
                } catch (JSONException e) {
                    Log.e(TAG, "construct replay for cfg_query_all", e);
                }
            }
            else if(evt == WebService.Events.EVT_SAVE_AND_APPLY_WIFI_AP_CFG){
                Object[] params = (Object[])msg.obj;
                Log.d(TAG, "save and reset:" + params[1]);
                saveCfg(CFG.fromJSON(((JSONObject)params[1])));
                MainService.getInstance().reboot("");
            }else{
                Log.e(TAG, "unknown message:" + msg.what);
            }
        }
    }
    */
}
