package com.womate.esix.service.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.womate.esix.MainService;
import com.womate.esix.service.*;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class WIFIServiceImpl implements WIFIService{
    final static String TAG = "ESix-WIFIServiceImpl";
    static final int MSG_CHECK_WIFI_CONNECTION = 1;
    static final int MSG_TURN_ON_WIFI_STA_COMPLETE = 2;
    static final int MSG_WEB_EVENT = 3;
    //static final int MSG_CHECK_SCAN_RECEIVERS = 4;
    //static final int MSG_CHECK_STA_NETWORK = 5;
    static final int SCAN_TIMEOUT = 60 * 60 * 1000;//10s
    static final String STATUS_IN_RANGE = "s0";
    static final String STATUS_CONNECTED = "s1";
    static final String STATUS_CONNECTING = "s2";
    static final String STATUS_ADD_FAIL = "s3";
    static final String STATUS_CONNECT_CANCEL = "s4";

    WifiManager mMgr;
    Handler mHandler;
    public WIFIServiceImpl(){
        mMgr = (WifiManager) MainService.getInstance()
                .getSystemService(Context.WIFI_SERVICE);
        if (mMgr == null) {
            Log.e(TAG, "Cannot get wifimanager");
        }
        mHandler = new Handler(){
            public void handleMessage(Message msg){
                Log.d(TAG, "handle message " + msg.what);
                switch (msg.what){
                    /*
                    case MSG_CHECK_STA_NETWORK:{
                        WifiInfo wi = mMgr.getConnectionInfo();
                        if(wi == null || wi.getSSID() == null){
                            if(msg.arg1 <= 0){
                                Log.i(TAG, "not get connection at second 15s check, report fail");
                                for(ScanReceiverTracker st:mWifiOp.receivers){
                                    sendCurrentConnectedNetwork(st.ws);
                                }
                            }else{
                                Log.i(TAG, "not get connection at first 15s check, go next 15s for checking");
                                this.sendEmptyMessageDelayed(MSG_CHECK_STA_NETWORK, msg.arg1);
                            }
                        }else{
                            if(mCurrentWifiInfo != null){
                                for(ScanReceiverTracker st:mWifiOp.receivers){
                                    st.ws.sendWebsockEvent(getScanResultObj(mCurrentWifiInfo.getSSID(), STATUS_CONNECT_CANCEL));
                                }
                            }
                            mCurrentWifiInfo = wi;
                            for(ScanReceiverTracker st:mWifiOp.receivers){
                                st.ws.sendWebsockEvent(getScanResultObj(wi.getSSID(), STATUS_CONNECTED));
                            }
                        }
                    }
                        break;
                    case MSG_CHECK_SCAN_RECEIVERS:{
                        long cur = System.currentTimeMillis();
                        int size = mWifiOp.receivers.size();
                        for(int i = size - 1; i >= 0; i--){
                            if(cur - mWifiOp.receivers.get(i).timeStart > SCAN_TIMEOUT + 100){
                                mWifiOp.receivers.remove(i);
                            }
                        }
                        if(mWifiOp.receivers.size() <= 0){
                            MainService.getInstance().unregisterReceiver(mWifiScanResultReceiver);
                            mWifiOp.brRegistered = false;
                            //mWifiOp.results.clear();
                        }else{
                            this.sendEmptyMessageDelayed(MSG_CHECK_SCAN_RECEIVERS, SCAN_TIMEOUT);
                        }
                    }
                        break;
                        */
                    case MSG_TURN_ON_WIFI_STA_COMPLETE:{
                        if(!mMgr.isWifiEnabled()){
                            Log.d(TAG, "wait another 1s for wifi sta enabled");
                            this.sendMessageDelayed(Message.obtain(msg), 1000);
                        }else{
                            if(!trySta((CfgService.CFG)msg.obj)){
                                startWIFIAp();
                            }
                        }
                    }
                        break;
                    case MSG_CHECK_WIFI_CONNECTION:{
                        NetworkInfo ni = ((ConnectivityManager)MainService.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE)).getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                        if(!ni.isConnected()){
                            startWIFIAp();
                        }else{
                            ;//MainService.turnLED(BLINK_LED, true);
                        }
                    }
                        break;
                    case MSG_WEB_EVENT:{
                        WebService.EventValue ev = (WebService.EventValue)msg.obj;
                        processWebEvent(msg.arg1, ev.ws, ev.jo);
                    }
                        break;
                    default:
                        Log.e(TAG,"unknow msg " + msg.what);
                        break;
                }
            }
        };
        ServiceFactory.getWebService().addEventHandler(WebService.Events.EVT_GROUP_WIFI_STA_CFG_START, mHandler, MSG_WEB_EVENT);
        ServiceFactory.getWebService().addEventHandler(WebService.Events.EVT_GROUP_WIFI_AP_CFG_START, mHandler, MSG_WEB_EVENT);
        ServiceFactory.getWebService().addEventHandler(WebService.Events.EVT_GROUP_CLIENT_START, mHandler, MSG_WEB_EVENT);

        BroadcastReceiver br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.i(TAG, " process intent:" + intent.getAction());
                if(intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)){
                    ConnectivityManager conn =  (ConnectivityManager)
                            context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
                    if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                        mWifiOp.sendJSON(getCurrentWifiStaStatus());
                        WifiInfo wi = mMgr.getConnectionInfo();
                        //if(!mWifiOp.ssidConnecting.equals("")){
                        //    mWifiOp.sendJSON(getScanResultObj(mWifiOp.ssidConnecting, STATUS_CONNECT_CANCEL));
                        //}
                        DisplayService.Ind.WifiStatus ind = new DisplayService.Ind.WifiStatus();
                        if(wi != null && wi.getIpAddress() != 0){
                            Log.i(TAG, "wifi network connected");
                            mWifiOp.sendJSON(getScanResultObj(wi.getSSID(), STATUS_CONNECTED));
                            ind.type = DisplayService.Ind.WifiType.AP_STA;
                            ind.staSSID = wi.getSSID();
                            ind.staStatus = wi.getSSID();
                        }else{
                            Log.i(TAG, "wifi network disconnected");
                            ind.type = DisplayService.Ind.WifiType.AP;
                        }
                        ServiceFactory.getDispService().wifi(ind);
                    }
                }else if(intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION)){
                    int state = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
                            WifiManager.WIFI_STATE_UNKNOWN);
                    Log.i(TAG, "new wifi state:" + state);
                    mWifiOp.sendJSON(getCurrentWifiStaStatus());
                    WifiInfo wi = mMgr.getConnectionInfo();
                    DisplayService.Ind.WifiStatus ind = new DisplayService.Ind.WifiStatus();
                    if(wi != null && wi.getIpAddress() != 0){
                        Log.i(TAG, "wifi network connected");
                        String ssid = wi.getSSID();
                        if(ssid == null || ssid.equals("")){
                            ssid = mWifiOp.ssidConnecting;
                        }
                        mWifiOp.sendJSON(getScanResultObj(ssid, STATUS_CONNECTED));
                        ind.type = DisplayService.Ind.WifiType.AP_STA;
                        ind.staSSID = wi.getSSID();
                        ind.staStatus = wi.getSSID();
                    }else{
                        Log.i(TAG, "wifi network disconnected");
                        ind.type = DisplayService.Ind.WifiType.AP;
                    }
                    ServiceFactory.getDispService().wifi(ind);
                }else{
                    ServiceFactory.getCfgService().resetAp();
                    SMSServiceImpl.deleteAll();
                    MiscServiceImpl.resetAll();
                    MainService.getInstance().reboot("resetap",2000);
                }
            }
        };
        IntentFilter iflt = new IntentFilter();
        iflt.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        iflt.addAction("com.womate.esix.RESET_AP_SETTING");
        iflt.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        MainService.getInstance().registerReceiver(br, iflt);
    }
    private void processWebEvent(int evtValue, WebService.WebSession webSession, JSONObject jsonObject) {
        WebService.Events evt;
        try{
            evt = WebService.Events.fromInt(evtValue);
            if(evt == WebService.Events.EVT_QUERY_WIFI_AP){
                CfgService.CFG cfg = ServiceFactory.getCfgService().readCfg();
                webSession.sendWebsockEvent(new JSONObject("{cmd:EVT_QUERY_WIFI_AP, paras:{ssid:" + cfg.wifiAPName + ",password:" + cfg.wifiAPPassword + "}}"));
            }else if(evt == WebService.Events.EVT_SET_WIFI_AP){
                CfgService.CFG cfg = ServiceFactory.getCfgService().readCfg();
                if(jsonObject.getString("password").length() < 8){
                    webSession.sendWebsockEvent(new JSONObject("{cmd:EVT_QUERY_WIFI_AP, paras:{ssid:" + cfg.wifiAPName + ",password:" + cfg.wifiAPPassword + "}}"));
                }
                cfg.wifiAPName = jsonObject.getString("ssid");
                cfg.wifiAPPassword=jsonObject.getString("password");
                ServiceFactory.getCfgService().saveCfg(cfg);
                MainService.getInstance().reboot("");
                /*
                WifiConfiguration wc = mMgr.getWifiApConfiguration();
                if (wc == null) {
                    Log.e(TAG, "Cannot get wificonfiguration");
                    return;
                }
                wc.SSID = cfg.wifiAPName;
                if(cfg.wifiAPPassword == null || cfg.wifiAPPassword.equals("")){
                    wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                }else{
                    wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                    wc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                    wc.preSharedKey = cfg.wifiAPPassword;
                }
                Thread.sleep(200);
                if (mMgr.getWifiApState() == WifiManager.WIFI_AP_STATE_ENABLED) {
                    mMgr.setWifiApEnabled(wc, true);
                } else {
                    mMgr.setWifiApConfiguration(wc);
                }
                */
            }else if(evt == WebService.Events.EVT_QUERY_WIFI_STA_ONOFF){
                webSession.sendWebsockEvent(getCurrentWifiStaStatus());
            }else if(evt == WebService.Events.EVT_SET_WIFI_STA_ONOFF){
                boolean onoff = jsonObject.getBoolean("onoff");
                mMgr.setWifiEnabled(onoff);
                mWifiOp.receivers.add(new ScanReceiverTracker(webSession));
            }else if(evt == WebService.Events.EVT_WIFI_STA_SCAN){
                try{
                Thread.sleep(1000);
                }catch(Exception e){};
                if(!mMgr.startScan()){
                    Log.e(TAG, "startScan() fail");
                    return;
                }
                ScanReceiverTracker st = new ScanReceiverTracker();
                st.ws = webSession;
                st.timeStart = System.currentTimeMillis();
                for(ScanReceiverTracker sti : mWifiOp.receivers){
                    if(sti.ws == st.ws){
                        sti.timeStart = st.timeStart;
                        st = null;
                        break;
                    }
                }
                if(st != null){
                    mWifiOp.receivers.add(st);
                }
                if(!mWifiOp.brRegistered){
                    //mHandler.sendEmptyMessageDelayed(MSG_CHECK_SCAN_RECEIVERS, SCAN_TIMEOUT);
                    MainService.getInstance().registerReceiver(mWifiScanResultReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
                    mWifiOp.brRegistered = true;
                }
                WifiInfo wi = mMgr.getConnectionInfo();
                if(wi != null && wi.getIpAddress() != 0){
                    mWifiOp.sendJSON(getScanResultObj(wi.getSSID(), STATUS_CONNECTED));
                }
            }else if(evt == WebService.Events.EVT_CLIENT_DISCONNECTED){
                //long cur = System.currentTimeMillis();
                for(ScanReceiverTracker st : mWifiOp.receivers){
                    if(st.ws == webSession){
                        mWifiOp.receivers.remove(st.ws);
                        break;
                    }
                }
                //mWifiOp.receivers.remove(webSession);
                if(mWifiOp.receivers.size() <= 0){
                    if(mWifiOp.brRegistered){
                        MainService.getInstance().unregisterReceiver(mWifiScanResultReceiver);
                        mWifiOp.brRegistered = false;
                    }
                    //mWifiOp.results.clear();
                    //mHandler.removeMessages(MSG_CHECK_SCAN_RECEIVERS);
                }
            }else if(evt == WebService.Events.EVT_WIFI_STA_CONNECT){
                String ssid = jsonObject.getString("idx");
                String pass = jsonObject.getString("password");
                enableWifiConfig(webSession, ssid, pass);
            }else{
                Log.d(TAG, "ignore " + evt);
            }
        }catch(Exception exp){
            Log.e(TAG, "convert int to WebService.Events", exp);
            return;
        }
        Log.d(TAG, "processing web event:" + evt);
    }

    private void enableWifiConfig(WebService.WebSession webSession, String ssid, String pass){
        WifiInfo wi = mMgr.getConnectionInfo();
        if(wi != null && wi.getIpAddress() != 0){
            mWifiOp.sendJSON(getScanResultObj(wi.getSSID(), STATUS_CONNECTED));
        }
        if(wi != null && ssid.equals(wi.getSSID())){
            Log.e(TAG, "to connect current ssid, ignore");
            return;
        }
        WifiConfiguration wf = null;
        WifiConfiguration oldWf = findWifiConfig(ssid);
        if(oldWf != null){
            Log.i(TAG, "found old wifi config for ssid, delete it firstly " + ssid);
            mMgr.removeNetwork(oldWf.networkId);
            mMgr.saveConfiguration();
        }

        //create a new config
        wf = new WifiConfiguration();
        wf.networkId = -1;
        wf.status = WifiConfiguration.Status.ENABLED;
        wf.SSID = "\"" + ssid + "\"";
        wf.hiddenSSID = false;

        Log.e(TAG, "chipeType for" + ssid + " is " + mCipheType.get(ssid));
        if(mCipheType.containsKey(ssid)){
            if(mCipheType.get(ssid).equals("NONE")){
                wf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            }else if(mCipheType.get(ssid).equals("WEP")){
                wf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                wf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                wf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                wf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
                wf.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.SHARED);
                wf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                wf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                wf.wepKeys[0] = "\"" + pass +"\"";
                wf.wepTxKeyIndex = 0;
            }else{
                wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
                wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
                wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
                wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
                wf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
                wf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
                wf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
                wf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
                wf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
                wf.preSharedKey = "\"" + pass + "\"";
            }
        }else{
            //is hidden ssid
            Log.e(TAG, "process hidden ssid");
            wf.hiddenSSID = true;
            //now we have not interface for user to select chrip type to hidden ssid
            //just try PSK :(
            wf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            wf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            wf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            wf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            wf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            wf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        }
        int newId = mMgr.addNetwork(wf);
        if(newId == -1){
            Log.e(TAG, "update config STA network failed");
            try {
                webSession.sendWebsockEvent(getScanResultObj(ssid, STATUS_ADD_FAIL));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return;
        }

        Log.d(TAG, "apply wifi STA config:" + newId);
        Log.d(TAG, "apply wifi STA config:" + wf.SSID);

        mMgr.saveConfiguration();
        mMgr.enableNetwork(newId, false);
        mMgr.reconnect();
        try {
            webSession.sendWebsockEvent(getScanResultObj(ssid, STATUS_CONNECTING));
        } catch (IOException e) {
            e.printStackTrace();
        }
        mWifiOp.ssidConnecting = ssid;
        //Message msgCheck = Message.obtain(mHandler, MSG_CHECK_STA_NETWORK, 15 * 1000, 0);
        //mHandler.sendMessageDelayed(msgCheck, 15 * 1000);//15s to check, 15s another check;
    }

    private WifiConfiguration findWifiConfig(String ssid){
        ssid = ssid.replace("\"", "");
        for(WifiConfiguration wf : mMgr.getConfiguredNetworks()){
            String s1 = wf.SSID.replace("\"", "");
            if(s1.equals(ssid)){
                return wf;
            }
        }
        return null;
    }

    private JSONObject getCurrentWifiStaStatus(){
        JSONObject jo;
        try{
            jo = new JSONObject();
            jo.put("onoff", "" + (mMgr.getWifiState() == WifiManager.WIFI_STATE_ENABLED));
            if(mMgr.getWifiState() == WifiManager.WIFI_STATE_ENABLED){
                WifiInfo wi = mMgr.getConnectionInfo();
                int ip = wi.getIpAddress();
                String ipS = String.format(
                        "%d.%d.%d.%d",
                        (ip & 0xff),
                        (ip >> 8 & 0xff),
                        (ip >> 16 & 0xff),
                        (ip >> 24 & 0xff));
                if(wi != null){
                    jo.put("info", "MAC:" + wi.getMacAddress() + " IP:" + ipS);
                }else{
                    jo.put("info", "");
                }
            }else{
                jo.put("info", "");
            }
            JSONObject joFinal = new JSONObject();
            joFinal.put("cmd", "EVT_QUERY_WIFI_STA_ONOFF");
            joFinal.put("paras", jo);
            return joFinal;
        }catch(Exception e){
            Log.e(TAG, "sending current wifi station information", e);
            return new JSONObject();
        }
    }

    private static class ScanReceiverTracker{
        public ScanReceiverTracker(WebService.WebSession w){
            ws = w;
            timeStart = 0;
        }
        public WebService.WebSession ws;
        public long timeStart;

        public ScanReceiverTracker() {

        }
    }

    static JSONObject JOPING = null;

    static {
        try {
            JOPING = new JSONObject("{\"cmd\":\"EVT_PING\"}");
        } catch (JSONException e) {
            Log.e(TAG, "NO WAY", e);
        }
    }

    //private volatile WifiInfo mCurrentWifiInfo = null;
    private JSONObject getScanResultObj(String ssid, String statusTxt){
        try{
            if(ssid == null || ssid.equals("")){
                return JOPING;
            }
            JSONObject jo = new JSONObject();
            jo.put("idx", ssid);
            jo.put("ssid", ssid);
            jo.put("status",statusTxt);
            jo.put("statusTxt", "");
            JSONObject finalO= new JSONObject();
            finalO.put("cmd", "EVT_WIFI_STA_SCAN");
            finalO.put("paras", jo);
            return finalO;
        }catch(Exception e){
            Log.e(TAG, "sending current connected wifi network", e);
        }
        return JOPING;
    }

    private WifiOperation mWifiOp = new WifiOperation();
    private WifiScanResultReceiver mWifiScanResultReceiver = new WifiScanResultReceiver();

    private static class WifiOperation {
        public volatile boolean brRegistered = false;
        //public List<ScanResult> results = new LinkedList<ScanResult>();
        public List<ScanReceiverTracker> receivers = new LinkedList<ScanReceiverTracker>();
        public String ssidConnecting = "";
        public void sendJSON(JSONObject jso){
            for(int i = receivers.size()- 1; i >= 0; i--){
                ScanReceiverTracker st = receivers.get(i);
                try{
                    st.ws.sendWebsockEvent(jso);
                }catch(Exception e){
                    Log.i(TAG, "sending json, remove receiver");
                    receivers.remove(i);
                }
            }
        }
    }
    /*
    private static class MyScanResult extends ScanResult{
        public MyScanResult(ScanResult sr){
            this(sr.SSID, sr.BSSID, sr.capabilities, sr.level, sr.frequency);
        }
        public MyScanResult(String SSID, String BSSID, String caps, int level, int frequency){
            super(SSID, BSSID, caps, level, frequency);
        }
        @Override
        public boolean equals(Object o){
            if(o == null) return false;
            if(o instanceof MyScanResult){
                return this.BSSID.equals(((MyScanResult)o).BSSID);
            }else if(o instanceof ScanResult){
                return this.BSSID.equals(((ScanResult)o).BSSID);
            }
            return false;
        }
    }
    */

    private HashMap<String, String> mCipheType = new HashMap<String, String>();
    private class WifiScanResultReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            if (WifiManager.SCAN_RESULTS_AVAILABLE_ACTION.equals(intent.getAction())) {

                List<ScanResult> rsts = mMgr.getScanResults();
                Log.d(TAG, "got scan result " + rsts.size());
                if (rsts != null) {
                    for (ScanResult rst : rsts) {
                        if (rst.SSID == null || rst.SSID.length() == 0 ||
                                rst.capabilities.contains("[IBSS]")) {
                            continue;
                        }
                        if(rst.capabilities.contains("WEP")){
                            mCipheType.put(rst.SSID, "WEP");
                        }else if(rst.capabilities.contains("PSK")){
                            mCipheType.put(rst.SSID, "PSK");
                        }else{
                            mCipheType.put(rst.SSID, "NONE");
                        }
                    }
                }

                long cur = System.currentTimeMillis();
                boolean hasRecentReceiver = false;
                for(ScanReceiverTracker st : mWifiOp.receivers){
                    if(cur - st.timeStart < SCAN_TIMEOUT){
                        hasRecentReceiver = true;
                        break;
                    }
                }
                if(!hasRecentReceiver){
                    Log.i(TAG, "all scan receivers outtime, not to send scan results");
                }
                String connectedSsid = "";
                WifiInfo wi = mMgr.getConnectionInfo();
                if(wi != null && wi.getIpAddress() != 0){
                    connectedSsid = wi.getSSID();
                    for(ScanReceiverTracker st : mWifiOp.receivers){
                        if(cur - st.timeStart >= SCAN_TIMEOUT){
                            //not send non-recent result
                            continue;
                        }
                        try{
                            st.ws.sendWebsockEvent(getScanResultObj(connectedSsid, STATUS_CONNECTED));
                        }catch(Exception e){
                            Log.e(TAG, "send to websocket", e);
                        }
                    }
                }
                Log.d(TAG, "got scan result " + rsts.size());
                if (rsts != null) {
                    for (ScanResult rst : rsts) {
                        Log.d(TAG, "Network: " + rst.BSSID + "," + rst.SSID);
                        if(rst.SSID.equals(connectedSsid)){
                            Log.i(TAG, "ignore current connected ssid:" + rst.SSID);
                            continue;
                        }
                        try{
                            JSONObject finalO = getScanResultObj(rst.SSID, STATUS_IN_RANGE);
                            for(ScanReceiverTracker st : mWifiOp.receivers){
                                if(cur - st.timeStart >= SCAN_TIMEOUT){
                                    //not send non-recent result
                                    continue;
                                }
                                try{
                                    st.ws.sendWebsockEvent(finalO);
                                }catch(Exception e){
                                    Log.e(TAG, "send to websocket", e);
                                }
                            }
                        }catch(Exception e){
                            Log.e(TAG, "generate json and sending wifi scan result", e);
                        }
                    }
                }
            }
        }
    }
    public void startWIFIAp() {
        Log.d(TAG, "apply wifi ap");
        //mMgr.setWifiEnabled(false);
        //try{
        //    Thread.sleep(500);
        //}catch(InterruptedException ie){
            //do nothing
        //}
        CfgService cfgSrv = ServiceFactory.getCfgService();
        CfgService.CFG cfg = cfgSrv.readCfg();

        Log.i(TAG, "start ap with:" + cfg.wifiAPName + "," + cfg.wifiAPPassword);
        WifiConfiguration wc = mMgr.getWifiApConfiguration();
        wc.SSID = cfg.wifiAPName;
        wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
        wc.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
        wc.preSharedKey = cfg.wifiAPPassword;
        mMgr.setWifiApEnabled(wc, true);
        //MainService.turnLED(BLINK_LED, true);
    }

    @Override
    public boolean isWIFIAPReady() {
        return mMgr.isWifiApEnabled();
    }
    private boolean trySta(CfgService.CFG cfg){
        int networkId = -1;
        WifiConfiguration wf = new WifiConfiguration();

        if(mMgr.getConfiguredNetworks().size() != 0){
            wf = mMgr.getConfiguredNetworks().get(0);
            networkId = wf.networkId;
            Log.d(TAG, "old cfg:" + networkId);
            Log.d(TAG, "old cfg:" + wf.SSID);
            Log.d(TAG, "old cfg:" + wf.preSharedKey);
        }else{
            Log.d(TAG, "no configured network found, create a new one");
            wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP104);
            wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
            wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
            wf.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
            wf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
            wf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
            wf.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
            wf.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
            wf.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
        }
        wf.status = WifiConfiguration.Status.ENABLED;
        wf.SSID = "\"" + cfg.wifiSTACfgAPName + "\"";
        wf.hiddenSSID = false;
        if(cfg.wifiSTACfgAPPassword.equals("")){
            wf.allowedAuthAlgorithms.set(WifiConfiguration.KeyMgmt.NONE);
        }else{
            wf.preSharedKey = "\"" + cfg.wifiSTACfgAPPassword + "\"";
        }
        if(networkId == -1){
            networkId = mMgr.addNetwork(wf);
            if(networkId == -1){
                Log.e(TAG, "config STA network failed");
                return false;
            }
            wf.networkId = networkId;
        }else{
            networkId = mMgr.updateNetwork(wf);
        }

        Log.d(TAG, "apply wifi STA config:" + networkId);
        Log.d(TAG, "apply wifi STA config:" + wf.SSID);
        Log.d(TAG, "apply wifi STA config:" + wf.preSharedKey);

        mMgr.saveConfiguration();
        mMgr.enableNetwork(networkId, true);
        mMgr.reconnect();
        mHandler.sendEmptyMessageDelayed(MSG_CHECK_WIFI_CONNECTION, 30 * 1000);//30s to make wifi connection
        return true;
    }

    public void turnOn(){
        CfgService cfgSrv = ServiceFactory.getCfgService();
        CfgService.CFG cfg = cfgSrv.readCfg();

        if(!cfg.wifiSTACfgAPName.equals("")){
            if(mMgr.isWifiEnabled()){
                //hack, restart wifi
                mMgr.setWifiEnabled(false);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    Log.e(TAG, "disabling wifi sta", e);
                }
            }
            mMgr.setWifiEnabled(true);
            Message msg = Message.obtain();
            msg.what = MSG_TURN_ON_WIFI_STA_COMPLETE;
            msg.obj = cfg;
            Log.d(TAG, "enable wifi sta and wait 2s");
            mHandler.sendMessageDelayed(msg, 2000);//wait 2s for wait wifi work, we need read configuration then
        }else{
            startWIFIAp();
        }
    }

    public boolean isWIFIReady(){
        if(isWIFIAPReady()){
            return true;
        }
        return isWIFISTAConnected();
    }

    @Override
    public boolean isWIFISTAConnected() {
        NetworkInfo ni = ((ConnectivityManager)MainService.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE)).getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return ni.isConnected();
    }

    public String getWIFIAddress(){
        try{
            if(isWIFIAPReady()){
                return "sipserver";//"192.168.43.1";
            }
            //NetworkInfo ni = ((ConnectivityManager)MainService.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE)).getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            WifiInfo wi = mMgr.getConnectionInfo();
            int ip = wi.getIpAddress();
            return String.format(
                        "%d.%d.%d.%d",
                        (ip & 0xff),
                        (ip >> 8 & 0xff),
                        (ip >> 16 & 0xff),
                        (ip >> 24 & 0xff));
        }
        catch(Exception exp){
            Log.d(TAG, "getting ip address");
        }
        return null;
    }

    @Override
    public String getWIFISTAApName() {
        if(isWIFISTAConnected()){
            return mMgr.getConnectionInfo().getSSID();
        }else{
            return "NOT CONNECTED";
        }
    }
}
