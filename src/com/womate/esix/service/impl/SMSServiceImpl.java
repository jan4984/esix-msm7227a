package com.womate.esix.service.impl;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.*;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsManager;
import android.text.format.Time;
import android.util.Log;
import com.womate.esix.MainService;
import com.womate.esix.service.DisplayService;
import com.womate.esix.service.SIPUAService;
import com.womate.esix.service.SMSService;
import com.womate.esix.service.ServiceFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: jan4984@gmail.com
 * Date: 7/3/13
 * Time: 3:16 PM
 * copyright belongs to jan4984@gmail.com
 */
public class SMSServiceImpl implements SMSService, SIPUAService.SMSListenerBase {
    final static String TAG = "ESix-SMS";
    BroadcastReceiver mSMSreceiver;
    public final static String INTENT_ACTION_SMS_SENT = "com.womate.esix.SMS_SENT";
    public final static String INTENT_ACTION_SMS_SEND_TEST = "com.womate.esix.SMS_SEND_TEST";
    HashMap<String, Bundle> mTelephonyRecievedSipNotConfirmedMessages;
    MyContentObserver mObserver;
    ContentResolver mProvider;
    boolean isClientConnected = false;


    public static void deleteAll(){
        MainService.getInstance().getContentResolver().delete(Uri.parse("content://sms"), null, null);
        MainService.getInstance().getContentResolver().delete(Uri.parse("content://call_log/calls"), null, null);
    }
    public SMSServiceImpl(){
        mMainThreadHandler = new Handler(MainService.getInstance().getMainLooper()){
            public void handleMessage(Message msg){
                Log.d(TAG, "handle " + msg.what);
                switch(msg.what){
                    case MSG_INIT_DISPLAY:{
                        ServiceFactory.getDispService().notify(mObserver.getUnreadCount(), -1);
                        ServiceFactory.getDispService().notify(-1, mObserver.getMissedCount());
                        this.sendEmptyMessage(MSG_CLIENT_STATUS_CHANGED);
                    }
                        break;
                    case MSG_NEW_UNREAD_SMS:{
                        if(!isClientConnected){
                            Log.w(TAG, "try to send sip message when client not connected");
                        }
                        Bundle data = msg.getData();
                        String addr = data.getString("address");
                        String body = data.getString("body");
                        String timeStamp = data.getString("date");
                        //String date = data.getString("date");
                        data.putLong("lastSendTime", System.currentTimeMillis());
                        data.putInt("sendTimes", data.getInt("sendTimes", 0) + 1);
                        String sipId = MainService.getSIPUAService().sendMessage(addr, timeStamp, body);
                        if(sipId == null){
                            if(isClientConnected){
                                Log.w(TAG, "cilent connected but getSIPUAService().sendMessage() returns null");
                                int telephonyId = data.getInt("id");
                                sipId = "tele-" + telephonyId;
                                mTelephonyRecievedSipNotConfirmedMessages.put(sipId, data);
                                processSipSentFail(sipId);
                            }
                        }else{
                            mTelephonyRecievedSipNotConfirmedMessages.put(sipId, data);
                        }
                    }
                        break;
                    case MSG_SEND_FAIL:{
                        processSipSentFail((String)msg.obj);
                    }
                        break;
                    case MSG_SEND_OK:{
                        processSipSentSuccess((String)msg.obj);
                    }
                        break;
                    case MSG_QUERY_UNREAD_SMS:{
                        //trigger unread messages sent.
                        mObserver.onChange(false);
                    }
                        break;
                    case MSG_CLIENT_STATUS_CHANGED:{
                        boolean curState = MainService.getSIPUAService().isSipClientConnected();
                        if(isClientConnected != curState){
                            mTelephonyRecievedSipNotConfirmedMessages.clear();
                            this.removeMessages(MSG_NEW_UNREAD_SMS);
                            this.removeMessages(MSG_QUERY_UNREAD_SMS);
                            this.sendEmptyMessageDelayed(MSG_QUERY_UNREAD_SMS, 3000);
                        }
                        isClientConnected = curState;
                    }
                        break;
                    default:
                        Log.e(TAG, "unknown msg " + msg.what);
                        break;
                }
            }
        };
        mTelephonyRecievedSipNotConfirmedMessages = new HashMap<String, Bundle>();
        mObserver = new MyContentObserver(mMainThreadHandler);
        mProvider = MainService.getInstance().getContentResolver();
        Uri ui = Uri.parse("content://sms");
        //delete all not inbox
        mProvider.delete(ui, "type<>?", new String[]{"1"});
        //delete all inbox which read
        mProvider.delete(ui, "type=? AND read=?", new String[]{"1","1"});
        mProvider.registerContentObserver(
                ui,
                true,
                mObserver);
        ui = Uri.parse("content://call_log/calls");
        mProvider.registerContentObserver(
                ui,
                true,
                mObserver);

        mSMSreceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                /*
                if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
                    Bundle data = intent.getExtras();
                    if(data != null){
                        Object[] smsextras = (Object[]) data.get( "pdus" );

                        for ( int i = 0; i < smsextras.length; i++ )
                        {
                            SmsMessage smsmsg = SmsMessage.createFromPdu((byte[])smsextras[i]);

                            String strMsgBody = smsmsg.getMessageBody().toString();
                            String strMsgSrc = smsmsg.getOriginatingAddress();
                            String id = MainService.getSIPUAService().sendMessage(strMsgSrc, strMsgBody);
                            LinkedList
                        }
                    }else{
                        Log.e(TAG, "received sms without bundle");
                    }
                }else */if(intent.getAction().equals(INTENT_ACTION_SMS_SENT)){
                    String sipId = intent.getStringExtra("id");
                    if(getResultCode() == Activity.RESULT_OK){
                        MainService.getSIPUAService().sendMessageACK(sipId, true);
                    }else{
                        MainService.getSIPUAService().sendMessageACK(sipId, false);
                    }
                }else if(intent.getAction().equals(INTENT_ACTION_SMS_SEND_TEST)){
                    String dest =  intent.getStringExtra("dest");
                    String time = intent.getStringExtra("time");
                    String body = intent.getStringExtra("body");
                    MainService.getSIPUAService().sendMessage(dest, time, body);
                }
            }
        };
        IntentFilter intf = new IntentFilter();
        //intf.addAction("android.provider.Telephony.SMS_RECEIVED");
        intf.addAction(INTENT_ACTION_SMS_SENT);
        intf.addAction(INTENT_ACTION_SMS_SEND_TEST);
        MainService.getInstance().registerReceiver(mSMSreceiver, intf);
        mMainThreadHandler.sendEmptyMessageDelayed(MSG_INIT_DISPLAY, 1000);
    }

    private void processSipSentFail(String sipId){
        if(!mTelephonyRecievedSipNotConfirmedMessages.containsKey(sipId)){
            Log.e(TAG, "not find sip id when sip message sent fail:" + sipId);
            return;
        }
        Bundle data = mTelephonyRecievedSipNotConfirmedMessages.get(sipId);
        MainService.getSIPUAService().sendMessageACK(sipId, false);
        Message smsResendDelay = Message.obtain();
        smsResendDelay.what = MSG_NEW_UNREAD_SMS;
        smsResendDelay.setData(data);
        long delay = data.getLong("delay", 10000);
        if(!(delay >= Long.MAX_VALUE / 2)){
            delay *= 2;
        }
        Log.d(TAG, "resend telephony sms to sip delay:" + delay);
        mMainThreadHandler.sendMessageDelayed(smsResendDelay, delay);
        Log.d(TAG, "remove sip id: " + sipId);
        mTelephonyRecievedSipNotConfirmedMessages.remove(sipId);
    }

    private void processSipSentSuccess(String sipId){
        if(!mTelephonyRecievedSipNotConfirmedMessages.containsKey(sipId)){
            Log.e(TAG, "not find sip id when sip message sent success:" + sipId);
            return;
        }
        int telId = mTelephonyRecievedSipNotConfirmedMessages.get(sipId).getInt("id");
        String subject = mTelephonyRecievedSipNotConfirmedMessages.get(sipId).getString("date");
        if(subject.startsWith("Missed Call")){
            Uri ui = Uri.parse("content://call_log/calls");
            ContentValues cv = new ContentValues();
            cv.put("type", 1);
            mProvider.update(ui, cv, "_id=?", new String[]{String.valueOf(telId)});
            Log.d(TAG, "remove sip id: " + sipId);
            mTelephonyRecievedSipNotConfirmedMessages.remove(sipId);
        }else{
            Uri ui = Uri.parse("content://sms");
            ContentValues cv = new ContentValues();
            cv.put("read", 1);
            mProvider.update(ui, cv, "_id=?", new String[]{String.valueOf(telId)});
            Log.d(TAG, "remove sip id: " + sipId);
            mTelephonyRecievedSipNotConfirmedMessages.remove(sipId);
        }
    }

    @Override
    public SIPUAService.SMSListenerBase getSIPUAListener() {
        return this;
    }

    @Override
    public void deleteAllReadAndSend() {
        Uri ui = Uri.parse("content://sms");
        /*
        public static final int MESSAGE_TYPE_ALL    = 0;
        public static final int MESSAGE_TYPE_INBOX  = 1;
        public static final int MESSAGE_TYPE_SENT   = 2;
        public static final int MESSAGE_TYPE_DRAFT  = 3;
        public static final int MESSAGE_TYPE_OUTBOX = 4;
        public static final int MESSAGE_TYPE_FAILED = 5; // for failed outgoing messages
        public static final int MESSAGE_TYPE_QUEUED = 6; // for messages to send later
        public static final int MESSAGE_TYPE_INBOX_SUB1 = 7;
        public static final int MESSAGE_TYPE_INBOX_SUB2 = 8;
         */
        //delete all not inbox
        mProvider.delete(ui, "type<>?", new String[]{"1"});
        //delete all inbox which read
        mProvider.delete(ui, "type=? AND read=?", new String[]{"1","1"});
        mProvider.delete(Uri.parse("content://call_log/calls"), "type<>?", new String[]{"3"});
    }

    @Override
    public Cursor queryUnreadforRead() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void markCurrentRead(Cursor current) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    /*
     * FOLLOWING for UA listener
     * FOLLOWING for UA listener
     * FOLLOWING for UA listener
     */
    @Override
    public void newMessageReceived(String id, String dest, String content) {
        try{
            SmsManager mgr = SmsManager.getDefault();
            ArrayList<String> msgs = mgr.divideMessage(content);
            int messageCount = msgs.size();
            Intent i = new Intent();
            i.putExtra("id", id);
            i.setAction(INTENT_ACTION_SMS_SENT);
            PendingIntent sentInt = PendingIntent.getBroadcast(MainService.getInstance(), 0, i, 0);
            Log.i(TAG, "divided message count:" + messageCount);
            if(messageCount == 0){
                mgr.sendTextMessage(dest, null, content, sentInt, null);
            }else{
                ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>(messageCount);
                for(int idx = 0; idx < messageCount - 1; idx++){
                    sentIntents.add(null);
                }
                sentIntents.add(sentInt);
                mgr.sendMultipartTextMessage(dest, null, msgs, sentIntents,null);
            }
            Log.d(TAG, "sent message by broadcasting, id:" + id);
        }catch(Exception e){
            Log.e(TAG, "sending text message", e);
            MainService.getSIPUAService().sendMessageACK(id, false);
        }
    }

    @Override
    public void messageSendOK(String id) {
        Message msg = Message.obtain();
        msg.obj = id;
        msg.what = MSG_SEND_OK;
        mMainThreadHandler.sendMessage(msg);
    }

    @Override
    public void messageSendFail(String id) {
        Message msg = Message.obtain();
        msg.obj = id;
        msg.what = MSG_SEND_FAIL;
        mMainThreadHandler.sendMessage(msg);
    }

    @Override
    public void clientStatusChanged(boolean isConnnected) {
        Log.d(TAG, "clientStatusChanged:" + isConnnected);
        Message msg = Message.obtain(mMainThreadHandler, MSG_CLIENT_STATUS_CHANGED, isConnnected ? 1 : 0, 0);
        msg.sendToTarget();
    }

    final static int MSG_NEW_UNREAD_SMS = 1;
    final static int MSG_SEND_OK = 2;
    final static int MSG_SEND_FAIL = 3;
    final static int MSG_QUERY_UNREAD_SMS = 4;
    final static int MSG_CLIENT_STATUS_CHANGED = 5;
    final static int MSG_INIT_DISPLAY = 6;
    private Handler mMainThreadHandler;

    private class MyContentObserver extends ContentObserver{
        public MyContentObserver(Handler h) {
            super(h);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return false;
        }

        public void onChange(boolean self){
            if(self){
                Log.d(TAG, "onChange self, ignore");
                return;
            }
            onChange(self, null);
        }

        public int getUnreadCount(){
            Uri uri = Uri.parse("content://sms");

            Cursor c = mProvider.query(uri
                    , new String[]{"_id", "read"}
                    , "read=?", new String[]{"0"}, null);
            int count = c.getCount();
            c.close();
            return count;
        }

        public int getMissedCount(){
            Uri uri = Uri.parse("content://call_log/calls");

            Cursor c = mProvider.query(uri
                    , new String[]{"_id", "type","date", "number"}
                    , "type=?", new String[]{"3"}, null);//get missed calls
            int count = c.getCount();
            c.close();
            return count;
        }

        public void onChange(boolean self, Uri u){
            Log.d(TAG, "query content://sms");
            Uri uri = Uri.parse("content://sms");

            Cursor c = mProvider.query(uri
                    , new String[]{"_id", "read","date", "body", "address"}
                    , "read=?", new String[]{"0"}, null);
            Log.d(TAG, "query out unread: " + c.getCount());
            if(!isClientConnected){
                ServiceFactory.getDispService().notify(c.getCount(), -1);
            }else{
                if(c.getCount() > 0){
                    c.moveToFirst();
                    do{
                        Message msg = Message.obtain();
                        int id = c.getInt(c.getColumnIndex("_id"));
                        boolean sending = false;
                        for(Bundle b : mTelephonyRecievedSipNotConfirmedMessages.values()){
                            if(b.getInt("id") == id){
                                sending = true;
                                break;
                            }
                        }
                        if(!sending){
                            msg.what = MSG_NEW_UNREAD_SMS;
                            msg.setData(new Bundle());
                            msg.getData().putInt("id", id);
                            msg.getData().putString("address", c.getString(c.getColumnIndex("address")));
                            msg.getData().putString("body", c.getString(c.getColumnIndex("body")));
                            long timeL = c.getLong(c.getColumnIndex("date"));
                            String timeS = getTime(timeL);
                            msg.getData().putString("date", "<time:" + timeS + ">");
                            Log.d(TAG, String.format("time: %d => %s", timeL, timeS));
                            mMainThreadHandler.sendMessage(msg);
                        }
                    }while(c.moveToNext());
                }
            }
            c.close();

            Log.d(TAG, "query content://call_log/calls ");
            uri = Uri.parse("content://call_log/calls");

            c = mProvider.query(uri
                    , new String[]{"_id", "type","date", "number", "duration"}
                    , "type=?", new String[]{"3"}, null);//get missed calls
            Log.d(TAG, "query out missed call: " + c.getCount());
            if(!isClientConnected){
                ServiceFactory.getDispService().notify(-1, c.getCount());
            }else{
                if(c.getCount() > 0){
                    c.moveToFirst();
                    do{
                        Message msg = Message.obtain();
                        int id = c.getInt(c.getColumnIndex("_id"));
                        boolean sending = false;
                        for(Bundle b : mTelephonyRecievedSipNotConfirmedMessages.values()){
                            if(b.getInt("id") == id){
                                sending = true;
                                break;
                            }
                        }
                        if(!sending){
                            msg.what = MSG_NEW_UNREAD_SMS;
                            msg.setData(new Bundle());
                            msg.getData().putInt("id", id);
                            String number = c.getString(c.getColumnIndex("number"));
                            if(number.startsWith("-")){
                                number = "Private_Number";
                            }
                            msg.getData().putString("address", number);
                            //BODY begin
                            StringBuffer sb = new StringBuffer();
                            sb.append("Missed Call(\u672a\u63a5\u6765\u7535):");
                            long timeL = c.getLong(c.getColumnIndex("date"));
                            String timeS = getTime(timeL);
                            Log.d(TAG, String.format("time: %d => %s", timeL, timeS));
                            sb.append(timeS);
                            sb.append(',');
                            //sb.append("Duration(\u672a\u63a5\u6765\u7535):");
                            sb.append(number);
                            msg.getData().putString("body", sb.toString());
                            //BODY end
                            msg.getData().putString("date", "Missed Call<time:" + timeS + ">");
                            mMainThreadHandler.sendMessage(msg);
                        }
                    }while(c.moveToNext());
                }
            }
            c.close();
        }

        //
        private String getTime(long l){
            try{
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                sdf.setTimeZone(Calendar.getInstance().getTimeZone());
                return sdf.format(l);
            }catch (Exception e){
                Log.e(TAG, "converting time from long format to string", e);
                return "<2012-12-12 12:12:12";
            }
        }
    }
}
