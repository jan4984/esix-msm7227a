package com.womate.esix.service.impl;

import android.app.AlarmManager;
import android.content.*;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Handler;
import android.os.Message;
import android.provider.Telephony;
import android.text.format.Time;
import android.util.Log;
import com.womate.esix.MainService;
import com.womate.esix.service.MiscService;
import com.womate.esix.service.ServiceFactory;
import com.womate.esix.service.WebService;
import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created with IntelliJ IDEA.
 * User: jan4984@gmail.com
 * Date: 8/14/13
 * Time: 1:56 PM
 * copyright belongs to jan4984@gmail.com
 */
public class MiscServiceImpl implements MiscService {
    private static final String TAG = "ESix-MiscService";
    private volatile long mLastBootMTTotal = -1;
    private volatile long mCurBootMobileTraffic = -1;

    static final int MSG_WEB_EVENT = 1;
    static final int MSG_REFRESH_MD = 2;
    private Handler mMainThreadHandler = new Handler(){
        @Override
        public void handleMessage(Message msg){
            Log.d(TAG, "handling " + msg.what);
            switch(msg.what){
                case MSG_REFRESH_MD:{
                    updateMobileDataTraffic();
                    this.sendEmptyMessageDelayed(MSG_REFRESH_MD, 10000);//refresh mobile data traffic every 10s
                }
                    break;
                case MSG_WEB_EVENT:{
                    WebService.EventValue ev = (WebService.EventValue)msg.obj;
                    processWebEvent(msg.arg1, ev.ws, ev.jo);
                }
                    break;
                default:
                    Log.e(TAG, "unknown message" + msg.what);
                    break;
            }
        }
    };
    BroadcastReceiver mScreenOnOffReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
                mMainThreadHandler.removeMessages(MSG_REFRESH_MD);
                Log.d(TAG, "screen off");
            }else if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
                Log.d(TAG, "screen on");
                mMainThreadHandler.removeMessages(MSG_REFRESH_MD);
                mMainThreadHandler.sendEmptyMessage(MSG_REFRESH_MD);
            }else{
                Log.e(TAG, "unknown action for receiver:" + intent.getAction());
            }
        }
    };

    private void processWebEvent(int evtValue, WebService.WebSession webSession, JSONObject jsonObject) {
        WebService.Events evt;
        try{
            evt = WebService.Events.fromInt(evtValue);
        }catch(Exception exp){
            Log.e(TAG, "convert int to WebService.Events", exp);
            return;
        }
        Log.d(TAG, "processing web event:" + evt);
        try{
            if(evt == WebService.Events.EVT_QUERY_APN){
               webSession.sendWebsockEvent(queryApn());
            }else if(evt == WebService.Events.EVT_APPLY_APN){
                applyApn(jsonObject);
                webSession.sendWebsockEvent(queryApn());
            }else if(evt == WebService.Events.EVT_QUERY_FW){
                webSession.sendWebsockEvent(new JSONObject("{cmd:EVT_QUERY_FW, paras:{hw:\"1.02\",fw:\"" + MainService.FIRMWARE_VERSIN + "\"}}"));
            }else if(evt == WebService.Events.EVT_RESET_DEV){
                MainService.getInstance().reboot("RemoteControl");
            } else if(evt == WebService.Events.EVT_RESET_MT){
                resetMobileDataTraffic(this);
                webSession.sendWebsockEvent(new JSONObject("{cmd:EVT_QUERY_MT,paras:{cur:" + (getCurBootMT()/1024) + "last:" + (getTotalMT()/1024) +"}}"));
            }else if(evt == WebService.Events.EVT_UPDATE_FW){
                String inputFilePath = jsonObject.getString("input");
                String outputFilePath = jsonObject.getString("output");
                if(!decryptFWUpdateFile(inputFilePath, outputFilePath)){
                    webSession.sendWebsockEvent(new JSONObject("{cmd:EVT_UPDATE_FW,paras:FAIL}"));
                }else{
                    webSession.sendWebsockEvent(new JSONObject("{cmd:EVT_UPDATE_FW,paras:OK}"));
                    MainService.getInstance().reboot("fw-update");
                }
            }else if(evt == WebService.Events.EVT_QUERY_MT){
                webSession.sendWebsockEvent(new JSONObject(
                        "{cmd:EVT_QUERY_MT,paras:{cur:" + (getCurBootMT()/1024) + ",total:" + (getTotalMT()/ 1024) + "}}"));
            }else if(evt == WebService.Events.EVT_SET_MD_ONOFF){
                try{
                    ConnectivityManager cm =
                            (ConnectivityManager)MainService.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
                    cm.setMobileDataEnabled(jsonObject.getBoolean("onoff"));
                    Thread.sleep(1000);
                    boolean onoff = cm.getMobileDataEnabled();
                    webSession.sendWebsockEvent(new JSONObject("{cmd:EVT_QUERY_MD_ONOFF, paras:{onoff:\"" + onoff + "\"}}"));
                }catch (Exception e){
                    Log.e(TAG, "processing set_md_onoff fail", e);
                }
            }else if(evt == WebService.Events.EVT_QUERY_MD_ONOFF){
                    ConnectivityManager cm =
                            (ConnectivityManager)MainService.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
                    webSession.sendWebsockEvent(new JSONObject("{cmd:EVT_QUERY_MD_ONOFF, paras:{onoff:" + cm.getMobileDataEnabled() + "}}"));
            }
        }catch (Exception exp){
            Log.e(TAG, "", exp);
        }
    }

    private JSONObject queryApn() throws JSONException{
        try{
            String apn_name = "womatecustom";
            Cursor cursor = MainService.getInstance().getContentResolver().query(Telephony.Carriers.CONTENT_URI, new String[]
                    {
                        "_id",
                        Telephony.Carriers.NAME,
                        Telephony.Carriers.APN,
                        Telephony.Carriers.PROXY,
                        Telephony.Carriers.PORT,
                        Telephony.Carriers.MMSPROXY,
                        Telephony.Carriers.MMSPORT,
                        Telephony.Carriers.USER,
                        Telephony.Carriers.SERVER,
                        Telephony.Carriers.PASSWORD,
                        Telephony.Carriers.MMSC,
                        Telephony.Carriers.TYPE,
                        Telephony.Carriers.MCC,
                        Telephony.Carriers.MNC,
                        Telephony.Carriers.AUTH_TYPE
                    }
                    , Telephony.Carriers.NAME + "=\"" + apn_name +"\"", null,
                    Telephony.Carriers.DEFAULT_SORT_ORDER);
            JSONObject jo;
            if(cursor.getCount() <= 0){
                Log.d(TAG, "no apn named womatecustom");
                jo = new JSONObject("{cmd:EVT_QUERY_APN, paras:{apn:\"\",proxy:\"\",port:\"\",mcc:\"\",mnc:\"\",user:\"\",server:\"\",password:\"\",authType:\"0\"}}");
            }else{
                cursor.moveToFirst();
                jo = new JSONObject();
                jo.put("apn",      cursor.getString(cursor.getColumnIndex(Telephony.Carriers.APN)));
                jo.put("proxy",    cursor.getString(cursor.getColumnIndex(Telephony.Carriers.PROXY)));
                jo.put("port",     cursor.getString(cursor.getColumnIndex(Telephony.Carriers.PORT)));
                jo.put("mcc",      cursor.getString(cursor.getColumnIndex(Telephony.Carriers.MCC)));
                jo.put("mnc",      cursor.getString(cursor.getColumnIndex(Telephony.Carriers.MNC)));
                jo.put("user",     cursor.getString(cursor.getColumnIndex(Telephony.Carriers.USER)));
                jo.put("server",   cursor.getString(cursor.getColumnIndex(Telephony.Carriers.SERVER)));
                jo.put("password", cursor.getString(cursor.getColumnIndex(Telephony.Carriers.PASSWORD)));
                jo.put("authType", cursor.getString(cursor.getColumnIndex(Telephony.Carriers.AUTH_TYPE)));
                JSONObject ret = new JSONObject();
                ret.put("cmd", "EVT_QUERY_APN");
                ret.put("paras", jo);
                jo = ret;
            }
            cursor.close();
            return jo;
        }catch(Exception exp){
            Log.e(TAG, "applying APN", exp);
            return new JSONObject("{cmd:EVT_QUERY_APN, paras:\"FAIL\"}");
        }
    }

    private boolean applyApn(JSONObject jo){
        try{
            Log.d(TAG, "saving apn:" + jo);
            String apn=jo.getString("apn");
            String proxy=jo.getString("proxy");
            String port=jo.getString("port");
            String mcc=jo.getString("mcc");
            String mnc=jo.getString("mnc");
            String user=jo.getString("user");
            String server=jo.getString("server");
            String password=jo.getString("password");
            int auth_type = -1;
            try{
                auth_type=jo.getInt("authType");
            }catch(Exception parseExp){
            }
            String apn_name = "womatecustom";
            String mmsc = "";
            String mmsproxy = "";
            String mmsport = "";
            String apn_type = "default";
            if(mcc.equals("") || mnc.equals("") || apn.equals("")){
                return false;
            }

            Uri adrUri = null;
            ContentValues values = new ContentValues();
            values.put(Telephony.Carriers.NAME, apn_name);
            values.put(Telephony.Carriers.APN, apn);
            values.put(Telephony.Carriers.PROXY, proxy);
            values.put(Telephony.Carriers.PORT, port);
            values.put(Telephony.Carriers.MMSPROXY, mmsproxy);
            values.put(Telephony.Carriers.MMSPORT, mmsport);
            values.put(Telephony.Carriers.USER, user);
            values.put(Telephony.Carriers.SERVER, server);
            values.put(Telephony.Carriers.PASSWORD, password);
            values.put(Telephony.Carriers.MMSC, mmsc);
            values.put(Telephony.Carriers.TYPE, apn_type);
            values.put(Telephony.Carriers.MCC, mcc);
            values.put(Telephony.Carriers.MNC, mnc);
            values.put(Telephony.Carriers.NUMERIC, mcc + mnc);
            values.put(Telephony.Carriers.AUTH_TYPE, auth_type);

            Cursor cursor = MainService.getInstance().getContentResolver().query(Telephony.Carriers.CONTENT_URI, new String[] {
                    "_id", Telephony.Carriers.NAME, Telephony.Carriers.APN, Telephony.Carriers.TYPE}, Telephony.Carriers.NAME + "=\"" + apn_name +"\"", null,
                    Telephony.Carriers.DEFAULT_SORT_ORDER);
            if(cursor.getCount() == 0){Log.d("NanoHTTPD", "new apn row");
                adrUri = MainService.getInstance().getContentResolver().insert(Telephony.Carriers.CONTENT_URI, values);
            }else{
                Log.d("NanoHTTPD", "replace apn now");
                cursor.moveToFirst();
                adrUri = ContentUris.withAppendedId(Telephony.Carriers.CONTENT_URI, Integer.parseInt(cursor.getString(0)));
                MainService.getInstance().getContentResolver().update(adrUri, values, null, null);
            }
            Log.d(TAG, "APN applied at " + adrUri);
            cursor.close();

            Log.d(TAG, "to set preference apn as the user added one");

            values = new ContentValues();
            values.put("apn_id", adrUri.getLastPathSegment());
            MainService.getInstance().getContentResolver().update(Uri.parse("content://telephony/carriers/preferapn"), values, null, null);

            return true;
        }catch(Exception exp){
            Log.e(TAG, "applying APN", exp);
            return false;
        }

    }
    public MiscServiceImpl(){
        IntentFilter filter = new IntentFilter();
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);
        Intent currentIntentForLevel = MainService.getInstance().registerReceiver(null, filter);
        if(currentIntentForLevel == null){
            Log.e(TAG, "can not get current battery level intent");
        }else{
            int status = currentIntentForLevel.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
            int level = 0;
            if(status == BatteryManager.BATTERY_STATUS_CHARGING){
                level = 101;
            }
            level += currentIntentForLevel.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            ServiceFactory.getDispService().battery(level);
        }
        filter.addAction(Intent.ACTION_BATTERY_LOW);
        MainService.getInstance().registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "receive " + intent.getAction());
                if(intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)){
                    int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
                    int level = 0;
                    if(status == BatteryManager.BATTERY_STATUS_CHARGING){
                        level = 101;
                    }
                    level += intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                    ServiceFactory.getDispService().battery(level);
                }
            }
        }, filter);
        ServiceFactory.getWebService().addEventHandler(WebService.Events.EVT_GROUP_APN_CFG_START, mMainThreadHandler, MSG_WEB_EVENT);
        ServiceFactory.getWebService().addEventHandler(WebService.Events.EVT_GROUP_FW_CFG_START, mMainThreadHandler, MSG_WEB_EVENT);
        ServiceFactory.getWebService().addEventHandler(WebService.Events.EVT_GROUP_MT_CFG_START, mMainThreadHandler, MSG_WEB_EVENT);

        IntentFilter ift = new IntentFilter();
        ift.addAction(Intent.ACTION_SCREEN_OFF);
        ift.addAction(Intent.ACTION_SCREEN_ON);
        MainService.getInstance().registerReceiver(mScreenOnOffReceiver, ift);
        addLastMTToTotal();
    }

    @Override
    public void setTime(String time){//Date now, String timeZone) {
        try{
            Log.d(TAG, "setTime:" + time);
            time = time.trim();
            time = time.replace("<time:","");
            time = time.replace(">","");
            String date[] = time.split(" ");
            //date[0]: 1900-12-12
            //date[1]: 12:12:12
            //date[2]: +0830
            if(date[1].length() == "12:12".length()){
                date[1] = date[1] + ":30";
            }
            Log.d(TAG, String.format("%s,%s,%s", date[0], date[1], date[2]));
            String tzH,tzM;
            tzH = date[2].substring(0, 3);
            tzM = date[2].substring(3);
            String allInOneForParse3339 = String.format("%sT%s.000%s:%s", date[0], date[1], tzH, tzM);
            Log.d(TAG, "try parsing time:" + allInOneForParse3339);
            Time t = new Time();
            t.parse3339(allInOneForParse3339);
            Log.d(TAG, "set time:" + t.format3339(false));
            AlarmManager am = (AlarmManager) MainService.getInstance().getSystemService(Context.ALARM_SERVICE);
            am.setTime(t.toMillis(true));
            String tz = "GMT"+tzH+":"+tzM;
            Log.d(TAG, "set time zone:" + tz);
            am.setTimeZone(TimeZone.getTimeZone(tz).getID());
            Calendar cal = Calendar.getInstance();
            int curHour = cal.get(Calendar.HOUR_OF_DAY);
            int curMin = cal.get(Calendar.MINUTE);
            Log.i(TAG, "" + curHour + ":"  + curMin);
        }catch(Exception exp){
            Log.e(TAG, "setting time:", exp);
        }
    }

    final static String K_CUR_BOOT = "curboot";
    final static String K_LAST_TOTAL = "lasttotal";
    final static String K_MT_PREF = "mobileTraffic";

    private void addLastMTToTotal(){
        try{
            SharedPreferences sp = MainService.getInstance().getSharedPreferences(K_MT_PREF, 0);
            mLastBootMTTotal = sp.getLong(K_LAST_TOTAL, 0);
            Log.d(TAG, "last total:" + mLastBootMTTotal);
            mLastBootMTTotal += sp.getLong(K_CUR_BOOT, 0);
            Log.d(TAG, "last total + last boot = " + mLastBootMTTotal);
            SharedPreferences.Editor spe = sp.edit();
            spe.putLong(K_CUR_BOOT, 0);
            spe.putLong(K_LAST_TOTAL, mLastBootMTTotal);
            spe.commit();
        }catch(Exception e){
            Log.e(TAG, "getting mobile data traffic", e);
        }
    }

    @Override
    public long getTotalMT() {
        return mLastBootMTTotal + mCurBootMobileTraffic;
    }

    @Override
    public long getCurBootMT() {
        updateMobileDataTraffic();
        return mCurBootMobileTraffic;
    }

    private void updateMobileDataTraffic() {
        try{
            mCurBootMobileTraffic = TrafficStats.getMobileRxBytes() + TrafficStats.getMobileTxBytes();
            SharedPreferences sp = MainService.getInstance().getSharedPreferences("mobileTraffic", 0);
            SharedPreferences.Editor spe = sp.edit();
            spe.putLong(K_CUR_BOOT, mCurBootMobileTraffic);
            spe.commit();
            Log.i(TAG, "mobile trafic curboot:" + mCurBootMobileTraffic);
        }catch(Exception e){
            Log.e(TAG, "getting mobile data traffic", e);
        }
    }

    public static void resetAll(){
        try{
            SharedPreferences sp = MainService.getInstance().getSharedPreferences("mobileTraffic", 0);
            SharedPreferences.Editor spe = sp.edit();
            spe.putLong(K_CUR_BOOT, 0);
            spe.putLong(K_LAST_TOTAL, 0);
            spe.commit();
        }catch(Exception e){
            Log.e(TAG, "getting mobile data traffic", e);
        }
    }

    private static boolean resetMobileDataTraffic(MiscServiceImpl impl){
        try{
            SharedPreferences sp = MainService.getInstance().getSharedPreferences("mobileTraffic", 0);
            SharedPreferences.Editor spe = sp.edit();
            spe.putLong(K_CUR_BOOT, 0);
            spe.putLong(K_LAST_TOTAL, 0);
            spe.commit();
            impl.mCurBootMobileTraffic = 0;
            impl.mLastBootMTTotal = 0;
            return true;
        }catch(Exception e){
            Log.e(TAG, "getting mobile data traffic", e);
            return false;
        }
    }

    private boolean decryptFWUpdateFile(String input, String output){
        Log.d(TAG,"fw-update, start decrypt");
        try{
            SecretKeySpec deskey = new SecretKeySpec(new byte[]{
                    0x65,0x66,0x69,0x76,0x65,0x75,0x70,0x67,0x72,0x61,
                    0x64,0x65,0x2d,0x6b,0x65,0x60}, "AES");
            IvParameterSpec iv = new IvParameterSpec(new byte[]{0x77,0x6f,0x6d,0x61,0x74,0x26,0x45,0x2a,0x38,0x39,0x30,0x31,0x32,0x33,0x34,0x35});
            Cipher c1 = Cipher.getInstance("AES/CBC/PKCS5Padding");
            c1.init(Cipher.DECRYPT_MODE, deskey, iv);
            FileOutputStream out = new FileOutputStream(output);
            FileInputStream in = new FileInputStream(input);
            CipherInputStream cin = new CipherInputStream(in, c1);
            byte[] buffer = new byte[2048];
            int bytesRead;
            while ((bytesRead = cin.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            out.close();
            cin.close();
        }catch(Exception e){
            Log.d(TAG, "decrypting firmware update file" ,e);
            MainService.getInstance().recordLog(e);
            return false;
        }
        Log.d(TAG, "fw-update, decrypt complete");
        return true;
    }
}
