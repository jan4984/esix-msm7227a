package com.womate.esix.service.impl;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.IAudioService;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import com.android.internal.telephony.IESixTelephony;
import com.android.internal.telephony.IESixTelephonyListener;
import com.womate.esix.MainService;
import com.womate.esix.service.CallService;
import com.womate.esix.service.SIPUAService;
import com.womate.esix.service.ServiceFactory;

import java.util.ArrayList;
import java.util.List;

public class CallServiceImpl implements CallService, SIPUAService.CallListenerBase {
    static final int SEC_DIAL_DTMF_END = 11;//event '#'
    private final MyContentObserver mObserver;
    private final ContentResolver mProvider;
    volatile boolean READY_TO_DIAL_FROM_DTMF = false;
    static String SEC_DIAL_NUM = "";
    static final int MSG_DIAL = 1;
    static final int MSG_INIT_DISPLAY = 2;
    static final int MSG_CHECK_DTMF = 3;

    static final String TAG = "ESix-CallServiceImpl";
    String mDailingNum;
    Calls mCalls;
    SIPUAService mSIPService;
    ESixTelephonyListener mESixLsn;
    IESixTelephony mESixTelService;
    Handler mMainThreadHandler;
    List<Character> mDTMFs = new ArrayList<Character>(20);

    public CallServiceImpl(){
        mSIPService = MainService.getSIPUAService();
        mCalls = new Calls();
        mESixLsn = new ESixTelephonyListener();
        try{
            mESixTelService = IESixTelephony.Stub.asInterface(ServiceManager.getService("esixtelephony"));
            mESixTelService.setListener(mESixLsn);
        }catch(Exception exp){
            Log.e(TAG, "getting service and setting listener error:", exp);
        }
        mMainThreadHandler = new Handler(){
            public void handleMessage(Message msg){
                Log.d(TAG, "handling message:" + msg.what);
                switch(msg.what){
                    case MSG_CHECK_DTMF:{
                        try {
                            synchronized (mDTMFs){
                                int size = mDTMFs.size();
                                if(size > 0){
                                    char c = mDTMFs.remove(0);
                                    Log.d(TAG, "to send dtmf " + c);
                                    mESixTelService.dtmf(c);
                                    if(size > 1){
                                        this.sendEmptyMessageDelayed(MSG_CHECK_DTMF, 400);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "sending dtmf", e);
                        }
                    }
                        break;
                    case MSG_INIT_DISPLAY:{
                        ServiceFactory.getDispService().notify(-1, mObserver.getMissedCount());
                    }
                        break;
                    case MSG_DIAL:
                        dial((String)msg.obj);
                        break;
                    default:
                        Log.d(TAG, "unknown message:" + msg.what);
                        break;
                }
            }
        };
        mObserver = new MyContentObserver(mMainThreadHandler);
        mProvider = MainService.getInstance().getContentResolver();
    }
    private CallInfo getCallInfo(int which){
        CallInfo ci = mCalls.Act;
        switch(which){
            case CallService.CALL_HLD:
                ci = mCalls.Hld;
                break;
            case CallService.CALL_WNT:
                ci = mCalls.Wnt;
                break;
        }
        return ci;
    }

    public void startRecordDtmfForDailingNumber(){
        READY_TO_DIAL_FROM_DTMF = true;
    }

    public boolean isRecordingDtmfForDailingNumber(){
        return READY_TO_DIAL_FROM_DTMF;
    }

    //used by jni AudioGroup to forward DTMF or dial in second phase
    //dtmf received from SIP
    @SuppressWarnings("unused")
    public void onDtmfReceived(int c){
        Log.d(TAG, "Dtmf received " + c);
        if(READY_TO_DIAL_FROM_DTMF){
            if(c == SEC_DIAL_DTMF_END){
                Log.d(TAG, "dtmf for secondly dial end with " + SEC_DIAL_DTMF_END);
                //ServiceFactory.getCallService().dial(SEC_DIAL_NUM);
                Message.obtain(mMainThreadHandler, MSG_DIAL, SEC_DIAL_NUM).sendToTarget();
                READY_TO_DIAL_FROM_DTMF = false;
                SEC_DIAL_NUM = "";
                mSIPService.stopSendFile2Rtp();
                return;
            }
            SEC_DIAL_NUM += DTMF2Num(c);
            return;
        }

        ServiceFactory.getCallService().sendDtmf(DTMF2Num(c));
    }

    public void sendDtmf(char c){
        Log.d(TAG, "received dtmf from sip: " + c);
        try{
            synchronized (mDTMFs){
                mDTMFs.add(c);
                if(!mMainThreadHandler.hasMessages(MSG_CHECK_DTMF)){
                    Log.i(TAG, "start dtmf checking");
                    mMainThreadHandler.sendEmptyMessage(MSG_CHECK_DTMF);
                }
            }
        }catch(Exception e){
            Log.e(TAG, "sending dtmf", e);
        }
    }

    public static char DTMF2Num(int n){
        if(0 <= n && n <= 9){
            return (char)(n + '0');
        }else if(n == 10){
            return '*';
        }else if(n == 11){
            return '#';
        }
        throw new RuntimeException("unknown number for dtmf:" + n);
    }
    static IAudioService getAudioService() {
        IAudioService audioService = IAudioService.Stub.asInterface(
                ServiceManager.checkService(Context.AUDIO_SERVICE));
        if (audioService == null) {
            Log.w(TAG, "Unable to find IAudioService interface.");
        }
        return audioService;
    }
    void handleVolumeKeyUp() {
        IAudioService audioService = getAudioService();
        if (audioService == null) {
            return;
        }
        try {
            for(int i = 0; i < 10; i++){
                audioService.adjustStreamVolume(AudioManager.STREAM_VOICE_CALL,
                        AudioManager.ADJUST_RAISE, 0);
            }
        } catch (RemoteException e) {
            Log.w(TAG, "IAudioService.adjustStreamVolume() threw RemoteException " + e);
        }
    }
    public void dial(String num){
        Log.d(TAG, "try dial " + num);
        mDailingNum = num;
        try{
            mESixTelService.dial(num);
        }catch(Exception exp){
            Log.e(TAG, "dailing error:", exp);
        }
    }
    public CallInfo queryAct() {
        return mCalls.Act;//return FromESixCallInfo(mESixTelService.queryForgroundCall());
    }
    public CallInfo queryHld() {
        return mCalls.Hld;//return FromESixCallInfo(mESixTelService.queryBackgroundCall());
    }
    public CallInfo queryWnt() {
        return mCalls.Wnt;//return FromESixCallInfo(mESixTelService.queryWaitingCall());
    }

    public SIPUAService.CallListenerBase getSIPUAListener() {
        return this;
    }

    //static CallInfo FromESixCallInfo(Bundle b){
    //    return null;
    //}

    //remote sip call comming
    public void newCallComing(String num) {
        dial(num);
    }

    //remote sip call disconnected
    private void disconnect(boolean answered, int whichCall) throws RemoteException{
        mESixTelService.client(answered);
        mESixTelService.hangup(whichCall);
    }

    public void disconnected(boolean answered) {
        READY_TO_DIAL_FROM_DTMF = false;
        Log.d(TAG, "SIP disconnected");
        if(!answered){
            Log.e(TAG, "leaving ring if no answer");
            try {
                mESixTelService.client(answered);
            } catch (RemoteException e) {
                Log.e(TAG, "change client status", e);
            }
        }else{
            if(mCalls.Act.state != CallService.CALL_S_IDLE){
                Log.d(TAG, "try hangup active call");
                try{
                    disconnect(answered, CallService.CALL_ACT);
                    //mESixTelService.hangup(CallService.CALL_ACT);
                }catch(Exception exp){
                    Log.e(TAG, "hanguping error:", exp);
                }
            }else if(mCalls.Wnt.state != CallService.CALL_S_IDLE){
                Log.d(TAG, "try hangup active call");
                try{
                    disconnect(answered, CallService.CALL_WNT);
                    //mESixTelService.hangup(CallService.CALL_WNT);
                }catch(Exception exp){
                    Log.e(TAG, "hanguping error:", exp);
                }
            }
        }
    }

    //remote sip call accepted
    public void accepted() {
        try{
            mESixTelService.accept();
        }catch(Exception exp){
            Log.e(TAG, "accepting error:", exp);
        }
    }

    //!!!invoke in binder
    private class ESixTelephonyListener extends IESixTelephonyListener.Stub{
        public void ConnectionStateChanged(int whichCall, int whichConn, int newState, String num){
            Log.d(TAG, String.format("connection state changed:%d %d %d %s", whichCall, whichConn, newState, num));
            CallInfo ci = getCallInfo(whichCall);
            if(newState != CALL_S_IDLE){
                if(whichConn > ci.cons.size()){
                    Log.e(TAG, "connection index should less or equal than conntion count " + whichConn + " " + ci.cons.size());
                }else if(ci.cons.get(whichConn) == null){
                    Connection con = new Connection();
                    con.Num = num;
                    con.orgDialNum = num;
                    ci.cons.add(whichConn, con);
                }
            }else{
                try{
                    ci.cons.remove(whichConn);
                }catch(IndexOutOfBoundsException e){}
            }
        }
        private void handleActStateChange(int newState, String num){
            int oldState = mCalls.Act.state;
            mCalls.Act.state = newState;
            Log.d(TAG, "handle act state change:" + oldState + " > " + newState);
            switch(oldState){
                case CALL_S_ACTIVE:
                    switch(newState){
                        case CALL_S_IDLE://remote hungup
                            mSIPService.disconnect();
                            mCalls.Act.cons.clear();
                            break;
                        default:
                            Log.e(TAG, "should not get state in active:" + newState);
                            break;
                    }
                    break;
                case CALL_S_IDLE:
                    switch(newState){
                        case CALL_S_ACTIVE://MT CALL. incoming handled in Wnt call. Act call got active from idle
                            mCalls.Act.cons.clear();
                            mCalls.Act.cons.add(new Connection());
                            mCalls.Act.cons.get(0).orgDialNum = num;
                            mCalls.Act.cons.get(0).Num = num;
                            handleVolumeKeyUp();
                            break;
                        case CALL_S_DAILING://MO CALL
                            mCalls.Act.cons.clear();
                            mCalls.Act.cons.add(new Connection());
                            mCalls.Act.cons.get(0).orgDialNum = mDailingNum;
                            mCalls.Act.cons.get(0).Num = num;
                            break;
                        default:
                            Log.e(TAG, "should not get state in idle:" + newState);
                            break;
                    }
                    break;
                case CALL_S_DAILING:
                    switch(newState){
                        case CALL_S_ALERTING://MO CALL
                            mSIPService.ring();
                            handleVolumeKeyUp();
                            break;
                        case CALL_S_IDLE://Remote hungup
                            mSIPService.disconnect();
                            break;
                        case CALL_S_ACTIVE://MO CALL success
                            mSIPService.accept();
                            break;
                        default:
                            Log.e(TAG, "should not get state in dailing:" + newState);
                            break;
                    }
                    break;

                case CALL_S_ALERTING:
                    switch (newState){
                        case CALL_S_ACTIVE://remote accept
                            //TODO: start two way voice channel
                            mSIPService.accept();
                            break;
                        case CALL_S_IDLE://remote hungup
                            mSIPService.disconnect();
                            break;
                        default:
                            Log.e(TAG, "should not get state in alerting" + newState);
                            break;
                    }
                    break;
            }
        }
        private void handleWntStateChange(int newState, String num){
            int oldState = mCalls.Wnt.state;
            mCalls.Wnt.state = newState;
            Log.d(TAG, "handle wnt state change:" + oldState + " > " + newState);

            if(newState == CALL_S_INCOMING){
                if(mCalls.Act.state != CALL_S_IDLE){
                    try{
                        disconnect(false, CALL_WNT);
                    }catch (RemoteException re){
                        Log.e(TAG, "disconnect CALL_WNT", re);
                    }
                }
                mCalls.Wnt.cons.clear();
                mCalls.Wnt.cons.add(new Connection());
                mCalls.Wnt.cons.get(0).orgDialNum = num;
                mCalls.Wnt.cons.get(0).Num = num;
                mSIPService.invite(num);
            }else if(newState == CALL_S_IDLE){
                try{
                    mESixTelService.client(MainService.getSIPUAService() != null && MainService.getSIPUAService().isSipClientConnected());
                }catch (RemoteException e){
                    Log.e(TAG, "setting esix client status", e);
                }
                mCalls.Wnt.cons.clear();
                //if we are not in active call, disconnect sip
                if(mCalls.Act.state == CALL_S_IDLE){
                    mSIPService.disconnect();
                }
            }

        }
        private void handleHldStateChange(int newState, String num){

        }
        public void CallStateChanged(int whichCall, int newState, String num){
            Log.d(TAG, "call state changed " + whichCall + " "  + newState);
            if(num == null || num.equals("")){
                num = "Private_Number";
            }
            switch(whichCall){
                case CALL_ACT:
                    handleActStateChange(newState, num);
                    break;
                case CALL_WNT:
                    handleWntStateChange(newState, num);
                    break;
                case CALL_HLD:
                    handleHldStateChange(newState, num);
                    break;
                default:
                    Log.e(TAG, "which call unkown");
                    break;
            }
        }
        public void SwitchHld(){
            Log.d(TAG, "active/background call switch");
            CallInfo ci = mCalls.Act;
            mCalls.Act = mCalls.Hld;
            mCalls.Hld = ci;
        }
    }

    private class MyContentObserver extends ContentObserver {
        public MyContentObserver(Handler mMainThreadHandler) {
            super(mMainThreadHandler);
        }
        @Override
        public boolean deliverSelfNotifications() {
            return false;
        }

        public void onChange(boolean self){
            if(self){
                Log.d(TAG, "onChange self, ignore");
                return;
            }
            onChange(self, null);
        }

        public int getMissedCount(){
            Uri uri = Uri.parse("content://call_log/calls");

            Cursor c = mProvider.query(uri
                    , new String[]{"_id", "type","date", "number"}
                    , "type=?", new String[]{"3"}, null);//get missed calls
            int count = c.getCount();
            c.close();
            return count;
        }

        public void onChange(boolean self, Uri u){
            Log.d(TAG, "onChange content://call_log/calls ");
            Uri uri = Uri.parse("content://call_log/calls");

            Cursor c = mProvider.query(uri
                    , new String[]{"_id", "type","date", "number"}
                    , "type=?", new String[]{"3"}, null);//get missed calls
            Log.d(TAG, "query out missed call: " + c.getCount());
            ServiceFactory.getDispService().notify(-1, c.getCount());
            /*
            if(c.getCount() > 0){
                c.moveToFirst();
                Message msg = Message.obtain();
                int id = c.getInt(c.getColumnIndex("_id"));
                boolean sending = false;
                for(Bundle b : mTelephonyRecievedSipNotConfirmedMessages.values()){
                    if(b.getInt("id") == id){
                        sending = true;
                        break;
                    }
                }
                if(!sending){
                    msg.what = MSG_NEW_UNREAD_SMS;
                    msg.setData(new Bundle());
                    msg.getData().putInt("id", id);
                    msg.getData().putString("address", c.getString(c.getColumnIndex("address")));
                    msg.getData().putString("body", c.getString(c.getColumnIndex("body")));
                    msg.getData().putString("date", c.getString(c.getColumnIndex("date")));
                    mMainThreadHandler.sendMessage(msg);
                }
            }
            */
            c.close();
        }
    }
}
