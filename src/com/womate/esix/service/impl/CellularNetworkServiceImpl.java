package com.womate.esix.service.impl;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.ServiceManager;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.womate.esix.MainService;
import com.womate.esix.service.CellularNetworkService;
import com.womate.esix.service.DisplayService;
import com.womate.esix.service.ServiceFactory;

/**
 * Created with IntelliJ IDEA.
 * User: jan4984@gmail.com
 * Date: 8/5/13
 * Time: 2:20 PM
 * copyright belongs to jan4984@gmail.com
 */
public class CellularNetworkServiceImpl implements CellularNetworkService{
    final static String TAG = "ESix-CellularNetwork";
    Handler mMainThreadHandler;

    public CellularNetworkServiceImpl(){
        Log.d(TAG, "new CellularNetworkServiceImpl. wait 2s to register phone state listener");
        mMainThreadHandler = new Handler(){
            public void handleMessage(Message msg){
                Log.d(TAG, "handling " + msg.what);
                switch(msg.what){
                    case 1:{
                        if(ServiceManager.getService(Context.TELEPHONY_SERVICE) == null){
                            Log.d(TAG, Context.TELEPHONY_SERVICE + " service not ready, wait another 1s");
                            this.sendEmptyMessageDelayed(1, 1000);
                            return;
                        }
                        TelephonyManager tm = (TelephonyManager) MainService.getInstance().getSystemService(Context.TELEPHONY_SERVICE);
                        tm.listen(new _PhoneListener()
                                , PhoneStateListener.LISTEN_DATA_ACTIVITY |
                                PhoneStateListener.LISTEN_DATA_CONNECTION_STATE |
                                PhoneStateListener.LISTEN_SERVICE_STATE |
                                PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
                    }
                    break;
                    default:
                        Log.e(TAG, "unknown message:" + msg.what);
                        break;
                }
            }
        };
        ServiceFactory.getDispService().cellular(getLastStatus());
        mMainThreadHandler.sendEmptyMessageDelayed(1, 2000);
    }

    private DisplayService.Ind.CellularStatus getLastStatus(){
        DisplayService.Ind.CellularStatus status = new DisplayService.Ind.CellularStatus();
        if(!mInService){
            status.signalLevel = 0;
            status.networkStatus  = DisplayService.Ind.CelluarNetworkStatus.SEARCHING;
            status.dataType = DisplayService.Ind.CellularDataSignalType.NONE;
        }else{
            status.signalLevel = mSignalLevel;
            status.networkStatus = DisplayService.Ind.CelluarNetworkStatus.INSERVICE;
            status.dataType = mMobileDataActive ? DisplayService.Ind.CellularDataSignalType.WCDMA : DisplayService.Ind.CellularDataSignalType.NONE;
        }

        return status;
    }

    private volatile boolean mInService = false;
    private int mSignalLevel = 1;
    private boolean mMobileDataActive = false;

    private class _PhoneListener extends PhoneStateListener{
        @Override
        public void onServiceStateChanged(ServiceState serviceState){
            Log.d(TAG, "service state changed:" + serviceState.getState());
            boolean isIn = serviceState.getState() == ServiceState.STATE_IN_SERVICE;
            if(mInService != isIn){
                mInService = isIn;
                ServiceFactory.getDispService().cellular(getLastStatus());
            }
        }

        @Override
        public void onDataConnectionStateChanged(int state, int networkType){
            Log.d(TAG, "data connection state changed:" + state);
            boolean conn = (state == TelephonyManager.DATA_CONNECTED);
            if(mMobileDataActive != conn){
                mMobileDataActive = conn;
                ServiceFactory.getDispService().cellular(getLastStatus());
            }
        }

        @Override
        public void onDataActivity(int direction){
        }

        @Override
        public void onSignalStrengthsChanged(SignalStrength signalStrength){
            int asu = signalStrength.getGsmSignalStrength();
            Log.d(TAG, "signal: " + asu);
            if(!mInService){
                Log.d(TAG, "ignore signal strength change when out of service");
                return;
            }
            int iconLevel;

            if(asu == 99) iconLevel = 1;
            else if (asu < 1)  iconLevel = 0;
            else if (asu >= 7)  iconLevel = 4;
            else if (asu >= 5)  iconLevel = 3;
            else if (asu >= 3)  iconLevel = 2;
            else iconLevel = 1;

            if(mSignalLevel != iconLevel){
                mSignalLevel = iconLevel;
                ServiceFactory.getDispService().cellular(getLastStatus());
            }
        }
    }
}
