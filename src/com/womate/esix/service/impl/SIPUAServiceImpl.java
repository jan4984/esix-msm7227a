package com.womate.esix.service.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.AndroidHttpClient;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.womate.esix.MainService;
import com.womate.esix.Utils;
import com.womate.esix.service.CfgService;
import com.womate.esix.service.SIPUAService;
import com.womate.esix.service.ServiceFactory;
import local.media.MediaDesc;
import local.media.MediaSpec;
import local.server.ESixLocationServiceImpl;
import local.server.Proxy;
import local.ua.*;
import org.zoolu.sip.address.NameAddress;
import org.zoolu.sip.call.Call;
import org.zoolu.sip.provider.SipProvider;

import java.net.URI;
import java.util.Date;
import java.util.Vector;

public class SIPUAServiceImpl implements SIPUAService, UserAgentListener {
    static final String TAG = "ESIX-SIPUAServiceImpl";
    static final int MSG_ACCEPT = 1;
    static final int MSG_DIS = 2;
    static final int MSG_INVIT = 3;
    static final int MSG_RING = 4;
    public static final int MSG_UNKNOWN_PAYLOAD = 5;
    static final int MSG_MSG_ACK = 6;
    static final int MSG_NOTIFY_CLIENT_STATUS = 7;
    static final int MSG_WIFI_NETOWRK_STATUS_CHANGED = 8;
    static final int MSG_SEND_PING = 9;
    static final int MSG_PING_TIMEOUT = 10;

    String mPingMsgID = "";
    ESixUserAgent mAgent;
    CallListenerBase mCallLsn;
    SMSListenerBase mSmsLsn;
    boolean mExternalProxy = false;
    CfgService.CFG mCfg;
    BroadcastReceiver mWifiNetworkLostReceiver;
    volatile boolean mWifiConnected = true;
    volatile boolean mSIPClientConnected = false;
    volatile String mSipClientName = "";

    Handler mMainThreadHandler = new Handler(){
        public void handleMessage(Message msg){
            Log.d(TAG, "handling message:" + msg.what);
            switch(msg.what){
            case MSG_ACCEPT:
                mAgent.accept();
                break;
            case MSG_DIS:
                Log.d(TAG, "hangup()");
                mAgent.hangup();
                break;
                case MSG_INVIT:
                    if(mExternalProxy){
                        mAgent.callExternalUser(mCfg.externalSIPUsernameRemote);
                    }else{
                        mAgent.callInternalUser(msg.getData().getString("user"));
                    }
                break;
            case MSG_RING:
                mAgent.ring();
                break;
            case MSG_MSG_ACK:
                mAgent.reportMessageReceived(msg.getData().getString("id"), msg.getData().getBoolean("success"));
                break;
            case MSG_UNKNOWN_PAYLOAD:{
                Log.d(TAG, "unknown payload:" + Utils.dumpHex((byte[])msg.obj));
            }
                break;
            case MSG_NOTIFY_CLIENT_STATUS:{
                if(mSmsLsn != null){
                    mSmsLsn.clientStatusChanged(msg.arg1 == 1);
                }
            }
                break;
            case MSG_WIFI_NETOWRK_STATUS_CHANGED:{
                if(msg.arg1 == 1 && !mWifiConnected){
                    mAgent.loopRegister();
                }
                mWifiConnected = (msg.arg1 == 1);
            }
                break;
            case MSG_SEND_PING:{
                    mPingMsgID = mAgent.sendMessageWithSenderNumber("00000000000", "", "PING");
                    sendEmptyMessageDelayed(MSG_PING_TIMEOUT, 2000);
                    sendEmptyMessageDelayed(MSG_SEND_PING, 30 * 10000);
                }
                break;
            case MSG_PING_TIMEOUT:{
                removeMessages(MSG_SEND_PING);
                notifyClientStatusChanged(false, "");
            }
                break;
            default:
                Log.e(TAG, "unknow message:" + msg.what);
                break;
            }
        }
    };

    public Handler getUnknownPayloadHandler(){
        return mMainThreadHandler;
    }

    @Override
    public void notifyClientStatusChanged(boolean isConnected, String name) {
        Log.d(TAG, "sip client connected:" + isConnected);
        if(isConnected && (mSIPClientConnected != isConnected)){
            mMainThreadHandler.sendEmptyMessage(MSG_SEND_PING);
        }else if(!isConnected){
            mMainThreadHandler.removeMessages(MSG_SEND_PING);
            mMainThreadHandler.removeMessages(MSG_PING_TIMEOUT);
        }
        mSIPClientConnected = isConnected;
        mSipClientName = name;
        Message msg = Message.obtain(mMainThreadHandler, MSG_NOTIFY_CLIENT_STATUS, isConnected ? 1 : 0, 0);
        msg.sendToTarget();
        ServiceFactory.getDispService().sipClientConnect(isConnected, name);
    }

    @Override
    public void stopSendFile2Rtp() {
        mAgent.getMediaAgent().sendFile2Rtp(null);
    }

    @Override
    public boolean isSipClientConnected() {
        return mSIPClientConnected;
    }

    @Override
    public String getSipClientName() {
        return mSipClientName;
    }

    public SIPUAServiceImpl(Proxy mProxy, SipProvider sip_provider, UserAgentProfile ua_profile) throws IllegalStateException{
        mCfg = ServiceFactory.getCfgService().readCfg();
        if(mCfg.isInternalSIPProxyOn){
            ua_profile.proxy = ESixLocationServiceImpl.ESIX_STATIC_IP;
            ua_profile.registrar = ESixLocationServiceImpl.ESIX_STATIC_IP;
            ua_profile.auth_realm = ESixLocationServiceImpl.ESIX_STATIC_IP;
            ua_profile.setUserURI(new NameAddress("<sip:esix@" + ESixLocationServiceImpl.ESIX_STATIC_IP + ">"));
        }else{
            if(mCfg.externalSIPProxyAddr.equals("")){
                Log.e(TAG, "internal sip proxy off, and not external sip proxy configured. quit sip UA");
                throw new IllegalStateException("internal sip proxy off, and not external sip proxy configured. quit sip UA");
            }
            ua_profile.user = mCfg.externalSIPUsername;
            ua_profile.display_name = mCfg.externalSIPUsername;
            ua_profile.proxy = mCfg.externalSIPProxyAddr + ":" + mCfg.externalSIPProxyPort;
            ua_profile.registrar = ua_profile.proxy;
            ua_profile.auth_realm = mCfg.externalSIPProxyAddr;
            ua_profile.auth_user = mCfg.externalSIPUsername;
            ua_profile.auth_passwd = mCfg.externalSIPPassword;
            mExternalProxy = true;
        }
        mAgent = new ESixUserAgent(mProxy, sip_provider, ua_profile, this);
        if(ua_profile.do_register){
            mAgent.loopRegister(ua_profile.expires,ua_profile.expires/2,ua_profile.keepalive_time);
        }

        mWifiNetworkLostReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ConnectivityManager conn =  (ConnectivityManager)
                        context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = conn.getActiveNetworkInfo();

                if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                    if(networkInfo.isConnected()){
                        Log.d(TAG, "connected, re-register");
                        Message msg = Message.obtain(mMainThreadHandler, MSG_WIFI_NETOWRK_STATUS_CHANGED, 1, 0);
                        msg.sendToTarget();
                        return;
                    }
                }

                Message msg = Message.obtain(mMainThreadHandler, MSG_WIFI_NETOWRK_STATUS_CHANGED, 0, 0);
                msg.sendToTarget();
            }
        };
        IntentFilter iflt = new IntentFilter();
        iflt.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        MainService.getInstance().registerReceiver(mWifiNetworkLostReceiver, iflt);
    }

    public static String nameAddr2Num(NameAddress addr){
        return addr.getAddress().getUserName();
    }

    @Override
    public void sendMessageACK(String id, boolean isSuccess) {
        Log.d(TAG, "sip message forward result:" + isSuccess);
        Message msg = Message.obtain();
        Bundle data = new Bundle();
        data.putString("id", id);
        data.putBoolean("success", isSuccess);
        msg.setData(data);
        msg.what = MSG_MSG_ACK;
        mMainThreadHandler.sendMessage(msg);
    }

    @Override
    public String sendMessage(String sender, String time, String content) {
        Log.d(TAG, "send SIP message:" + sender + " " + content);
        return mAgent.sendMessageWithSenderNumber(sender, time, content);
    }

    @Override
    public void accept() {
        mMainThreadHandler.sendEmptyMessage(MSG_ACCEPT);
    }

    @Override
    public void ring() {
        mMainThreadHandler.sendEmptyMessage(MSG_RING);
    }

    private boolean mClientAnswered = false;
    @Override
    public void invite(String num) {
        if(true){
            mClientAnswered = false;
            Log.d(TAG, "invite " + num);
            Message msg = Message.obtain(mMainThreadHandler, MSG_INVIT);
            Bundle b = new Bundle();
            b.putString("user", num);
            msg.setData(b);
            msg.sendToTarget();
        }else{
            //auto answer for radio test
            mCallLsn.accepted();
        }
    }

    @Override
    public void disconnect() {
        Log.d(TAG, "disconnect()");
       mMainThreadHandler.sendEmptyMessage(MSG_DIS);
    }

    public void setCallListener(CallListenerBase l){
        mCallLsn = l;
    }

    public void setSmsListener(SMSListenerBase l){
        mSmsLsn = l;
    }

    @Override
    public void onUaRegistrationSucceeded(UserAgent ua, String result) {
        Log.d(TAG,"registration success");
    }

    @Override
    public void onUaRegistrationFailed(UserAgent ua, String result) {
        Log.d(TAG, "registration failed");
    }

    @Override
    public void onUaIncomingCall(UserAgent ua, NameAddress callee, NameAddress caller, Vector media_descs) {
        mClientAnswered = true;
        Log.d(TAG, "call " + callee);
        for(Object mdesc: media_descs){
            MediaDesc md = (MediaDesc)mdesc;
            for(Object mspec : md.getMediaSpecs()){
                MediaSpec ms = (MediaSpec)mspec;
                if(ms.getCodec().equals("telephone-event")){
                    int avp = ms.getAVP();
                    Log.d(TAG, "set dtmf recv payload type:" + avp);
                    mAgent.getMediaAgent().setDtmfRecvPayloadType(ms.getAVP());
                    break;
                }
            }
        }
        if(mCfg.isInternalSIPProxyOn){
            if(mCallLsn != null){
                mCallLsn.newCallComing(nameAddr2Num(callee));
            }
        }else{
            Log.d(TAG, "internal SIP proxy off, accept and wait DTMF for dailing number");
            if(mCallLsn != null){
                mCallLsn.startRecordDtmfForDailingNumber();
            }
            this.accept();
        }
    }

    @Override
    public void onUaCallCancelled(UserAgent ua) {
        Log.d(TAG, "call cancelled");
        if(mCallLsn != null){
            mCallLsn.disconnected(mClientAnswered);
        }
    }

    @Override
    public void onUaCallProgress(UserAgent ua) {
        mClientAnswered = true;
        Log.d(TAG, "call progress");
    }

    @Override
    public void onUaCallTring(UserAgent ua) {
        mClientAnswered = true;
    }

    @Override
    public void onUaCallRinging(UserAgent ua) {
        mClientAnswered = true;
        Log.d(TAG, "call ring");
    }

    @Override
    public void onUaCallAccepted(UserAgent ua) {
        mClientAnswered = true;
        Log.d(TAG, "call accepted");
        if(mCallLsn != null){
            mCallLsn.accepted();
        }
    }

    @Override
    public void onUaCallTransferred(UserAgent ua) {
        Log.d(TAG, "call transferred");
    }

    @Override
    public void onUaCallFailed(UserAgent ua, String reason) {
        Log.d(TAG, "call failed");
        if(mCallLsn != null){
            mCallLsn.disconnected(mClientAnswered);
        }
    }

    @Override
    public void onUaCallClosed(UserAgent ua) {
        Log.d(TAG, "call close");
        if(mCallLsn != null){
            mCallLsn.disconnected(mClientAnswered);
        }
    }

    @Override
    public void onUaMediaSessionStarted(UserAgent ua, String type, String codec) {
        Log.d(TAG, "media session started");
        if(mCallLsn != null){
            if(mCallLsn.isRecordingDtmfForDailingNumber()){
                mMainThreadHandler.postDelayed(new Runnable(){
                    @Override
                    public void run() {
                        mAgent.getMediaAgent().sendFile2Rtp(MainService.PCM_FILE_DIAL_NUM_EN);
                    }
                }, 1000);
            }
        }
    }

    @Override
    public void onUaMediaSessionStopped(UserAgent ua, String type) {
        Log.d(TAG, "media session stopped");
    }

    @Override
    public void onMessageReceived(UserAgent ua, NameAddress callee, String body, String tcId) {
        Log.d(TAG, "message received: " + callee + " " + body);
        if(mSmsLsn != null){
            mSmsLsn.newMessageReceived(tcId, nameAddr2Num(callee), body);
        }
    }

    @Override
    public void onMessageDeliverySuccess(UserAgent ua, NameAddress remote, String tcId) {
        Log.d(TAG, "message received by remote SIP user:" + tcId);
        if(mPingMsgID.equals(tcId)){
            Log.e(TAG, "ping success");
            mMainThreadHandler.removeMessages(MSG_PING_TIMEOUT);
        }
        if(mSmsLsn != null){
            mSmsLsn.messageSendOK(tcId);
        }
    }

    @Override
    public void onMessageDeliveryFailure(UserAgent ua, NameAddress remote, String tcId) {
        Log.d(TAG, "message failed to send to remote SIP user:" + tcId);
        if(mSmsLsn != null){
            mSmsLsn.messageSendFail(tcId);
        }
    }

}
