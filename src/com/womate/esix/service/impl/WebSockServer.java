package com.womate.esix.service.impl;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.womate.esix.service.WebService;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONException;
import org.json.JSONObject;


class WebSockServer extends WebSocketServer {
    final static String TAG = WebServiceImpl.TAG;
    Handler mLsn;

    public WebSockServer( int port ) throws UnknownHostException {
		super( new InetSocketAddress( port ) );
	}

	public WebSockServer(Handler listener, InetSocketAddress address ) {
		super( address );
        mLsn = listener;
	}
	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
        Log.d(TAG, "onOpen:" + conn);
        Message msg = new Message();
        msg.what = WebServiceImpl.MSG_NEW_WEB_SOCK_CONN;
        msg.obj = conn;
        mLsn.sendMessage(msg);
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        Log.d(TAG, "onClose:" + conn);
        Message msg = new Message();
        msg.what = WebServiceImpl.MSG_CLOSE_WEB_SOCK_CONN;
        msg.obj = conn;
        mLsn.sendMessage(msg);
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		Log.d(TAG, "onMessage:" + message + "from " + conn);
        JSONObject msg;
        Message ardMsg = Message.obtain();
        try {
            msg = new JSONObject(message);
            ardMsg.what = WebServiceImpl.MSG_NEW_WEBSOCK_MSG;
            Object[] params = new Object[]{conn, null};
            ardMsg.obj = params;
            WebService.Events evt;
            String cmd = msg.getString("cmd");
            try{
                evt = WebService.Events.fromString(cmd);
            }catch (Exception e){
                Log.e(TAG, "unknow command:" + cmd);
                return;
            }
            ardMsg.arg1 = evt.getValue();
            Object paras = msg.get("paras");
            if(paras instanceof JSONObject){
                params[1] = msg.getJSONObject("paras");
            }
            mLsn.sendMessage(ardMsg);
        } catch (JSONException e) {
            Log.e(TAG, "processing JSON message", e);
        }

    }

	@Override
	public void onError(WebSocket conn, Exception ex) {
        Log.e(TAG, "onError", ex);
	}

}
