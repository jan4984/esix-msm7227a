package com.womate.esix.service.impl;

import android.app.AlarmManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.*;
import com.womate.esix.MainService;
import com.womate.esix.service.DisplayService;

import android.util.Log;
import com.womate.esix.service.ServiceFactory;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class DisplayServiceImpl implements DisplayService{
    static final int MSG_TIME_TICK = 1;
    static final int MSG_SCREEN_ON = 2;
    static final int MSG_BATTERY_LEVEL_CHANGED = 3;
    static final int MSG_SIP_CLIENT_CONNECTED = 4;
    static final int MSG_NOTIF = 5;
    static final int MSG_WIFI_STATUS_CHANGED = 6;
    static final int MSG_UPDATE_MOBILE_TRAFFIC = 7;

    _Thread mThread;
    PaintHandler mPaintHandler;
    volatile boolean mRefreshOn = false;
    Handler mMainThreadHandler = null;
    int mLastHour, mLastMin;
    volatile int mLastBatteryLevel = 5;
    volatile boolean mSipClientOnline = false;
    BroadcastReceiver mScreenOnOffReceiver;
    volatile boolean mIsInService = false;
    volatile int mMissedCall = 0;
    volatile int mUnreadSms = 0;
    Ind.WifiStatus mWifiStatus;
    String mMobileTraffic = "/";

    @Override
    public void cellular(Ind.CellularStatus status) {
        mIsInService = status.networkStatus.equals(Ind.CelluarNetworkStatus.INSERVICE);
        Log.d(TAG, "in service?" + mIsInService + " data active?" + (status.dataType != Ind.CellularDataSignalType.NONE));
        if(mIsInService){
            int icon;
            switch(status.signalLevel){
                case 0:
                    icon = ICON_MOBILE_SIGNAL_1;
                    break;
                case 1:
                    icon = ICON_MOBILE_SIGNAL_2;
                    break;
                case 2:
                    icon = ICON_MOBILE_SIGNAL_3;
                    break;
                case 3:
                    icon = ICON_MOBILE_SIGNAL_4;
                    break;
                case 4:
                    icon = ICON_MOBILE_SIGNAL_5;
                    break;
                default:
                    icon = ICON_MOBILE_SIGNAL_1;
            }
            mPaintHandler.drawIcon(icon);
            mPaintHandler.drawIcon(status.dataType == Ind.CellularDataSignalType.NONE ? ICON_MOBILE_DATA_OFF : ICON_MOBILE_DATA_ON);
        }else{
            mPaintHandler.drawIcon(ICON_MOBILE_SIGNAL_NO);
            mPaintHandler.drawIcon(ICON_MOBILE_DATA_OFF);
        }
    }

    boolean mDisplayInitialized = false;
    @Override
    public void battery(int level) {
        Log.d(TAG, "battery:" + level + ", last is:" + mLastBatteryLevel);
        if(level != mLastBatteryLevel){
            mLastBatteryLevel = level;
            mMainThreadHandler.sendEmptyMessage(MSG_BATTERY_LEVEL_CHANGED);
        }
        if(!mDisplayInitialized){
            mDisplayInitialized = true;
            Intent i = new Intent("com.womate.esix.trunautoshutdown");
            MainService.getInstance().sendBroadcast(i);
        }
    }

    @Override
    public void wifi(Ind.WifiStatus status) {
        if(!status.equals(mWifiStatus)){
            mWifiStatus = status.clone();
            Log.d(TAG, "wifi status changed:" + status);
            Message msg = Message.obtain(mMainThreadHandler, MSG_WIFI_STATUS_CHANGED, status.clone());
            msg.sendToTarget();
        }else{
            Log.d(TAG, "wifi status not changed:" + mWifiStatus);
        }
    }

    @Override
    public void sipClientConnect(boolean isConnect, String name) {
        Log.d(TAG, "sip client:" + isConnect + ", " + name);
        if(isConnect != mSipClientOnline){
            mSipClientOnline = isConnect;
        }
        Message msg = Message.obtain(mMainThreadHandler, MSG_SIP_CLIENT_CONNECTED, isConnect ? 1: 0, 0, name);
        msg.sendToTarget();
    }

    @Override
    public void refresh(boolean onOff) {
        mMainThreadHandler.obtainMessage(MSG_SCREEN_ON, onOff ? 1: 0, 0).sendToTarget();
    }

    @Override
    public void notify(int sms, int call){
        Log.d(TAG, "missed SMS/CALL:" + sms + ", " + call);
        if(sms != -1){
            mUnreadSms = sms;
            mMainThreadHandler.sendEmptyMessage(MSG_NOTIF);
        }
        if(call != -1){
            mMissedCall = call;
        }
        if(sms != -1 || call != -1){
            mMainThreadHandler.sendEmptyMessage(MSG_NOTIF);
        }
    }

    @Override
    public void mobileDataTraffic(String traffic) {
        if(!mMobileTraffic.equals(traffic)){
            mMobileTraffic = traffic;
            mMainThreadHandler.sendEmptyMessage(MSG_UPDATE_MOBILE_TRAFFIC);
        }
    }

    private boolean updateTime(){
        boolean ret = false;
        Calendar cal = Calendar.getInstance();
        //Log.i(TAG, "time zone:" + TimeZone.getDefault());
        int curHour = cal.get(Calendar.HOUR_OF_DAY);
        int curMin = cal.get(Calendar.MINUTE);
        if(curHour != mLastHour || curMin != mLastMin){
            ret = true;
        }
        mLastHour = curHour;
        mLastMin = curMin;
        Log.d(TAG, "current time:" + mLastHour + ":" + curMin);
        return ret;
    }


    static class _Thread extends HandlerThread{
        //private DisplayServiceImpl mOwner = null;
        public _Thread(DisplayServiceImpl owner){
        	super("painter", android.os.Process.THREAD_PRIORITY_LOWEST);
            //mOwner = owner;
        }
    }

    static final String TAG = "ESix-DisplayServiceImpl";
    static final DecimalFormat MTFormat = new DecimalFormat("0.00");

    private static String getCenterString(String s, int charCount){
        int pad = charCount - s.length();
        if(pad <= 0) return s;
        StringBuilder sb = new StringBuilder();
        int padLeft = pad / 2;
        int padRight = pad - padLeft;
        for(int i = 0; i < padLeft; i++) sb.append(' ');
        sb.append(s);
        for(int i = 0; i < padRight; i++) sb.append(' ');
        return sb.toString();
    }

    private void drawMobileTraffic(){
        if(!ServiceFactory.getWifiService().isWIFISTAConnected()){
            mPaintHandler.drawIcon(ICON_3G_LOAD);
            long cur = ServiceFactory.getMiscService().getCurBootMT();
            long total = ServiceFactory.getMiscService().getTotalMT();
            String str = MTFormat.format(cur >> 20) + "/" + MTFormat.format(total >> 20) + "M";
            mPaintHandler.drawString(getCenterString(str, 13), STRING_TYPE_3G_LOAD);
            //mPaintHandler.drawString(str, STRING_TYPE_3G_LOAD);
        }
    }
    public DisplayServiceImpl(){
        mThread = new _Thread(this);
        mThread.start();
        mPaintHandler = new PaintHandler(mThread.getLooper());//getLooper() will block and wait looper be OK
        mMainThreadHandler = new Handler(){
            public void handleMessage(Message msg){
                Log.d(TAG, "handle msg:" + msg.what);

                switch(msg.what){
                    case MSG_UPDATE_MOBILE_TRAFFIC:
                        if(mWifiStatus != null && (mWifiStatus.type.equals(Ind.WifiType.AP_STA)
                                || mWifiStatus.type.equals(Ind.WifiType.STA))){
                            Log.d(TAG, "ignore mobile traffic when wifi station on");
                        }else{
                            if(mRefreshOn){
                                drawMobileTraffic();
                                this.sendEmptyMessageDelayed(MSG_UPDATE_MOBILE_TRAFFIC, 10000);
                            }
                        }
                        break;
                    case MSG_WIFI_STATUS_CHANGED:{
                        if(mRefreshOn){
                            if(mWifiStatus != null && (mWifiStatus.type.equals(Ind.WifiType.AP_STA) || mWifiStatus.type.equals(Ind.WifiType.AP))){
                                //mPaintHandler.drawIcon(ICON_WIFI_AP);
                                //mPaintHandler.drawString("1", STRING_TYPE_STA_COUNT_IN_AP);
                            }else{
                                //mPaintHandler.clearString(STRING_TYPE_STA_COUNT_IN_AP);
                            }
                            if(mWifiStatus != null && (mWifiStatus.type.equals(Ind.WifiType.AP_STA) || mWifiStatus.type.equals(Ind.WifiType.STA))){
                                mPaintHandler.drawIcon(ICON_WIFI_STA);
                                mPaintHandler.drawString(mWifiStatus.staStatus, STRING_TYPE_SSID_IN_STA);
                            }else{
                                drawMobileTraffic();
                            }
                        }
                    }
                        break;
                    case MSG_NOTIF:{
                        Log.d(TAG, "notify when client connected:" + mSipClientOnline);
                        if(!mSipClientOnline){
                            mPaintHandler.drawIcon(ICON_MISS_CALL);
                            mPaintHandler.drawIcon(ICON_UNREAD_SMS);
                            mPaintHandler.drawString(String.valueOf(mUnreadSms), STRING_TYPE_UNREAD_SMS_COUNT);
                            mPaintHandler.drawString(String.valueOf(mMissedCall), STRING_TYPE_MISS_CALL_COUNT);
                        }
                    }
                        break;
                    case MSG_SIP_CLIENT_CONNECTED:{
                        boolean isConnect = msg.arg1 == 1;
                        if(isConnect){
                            mPaintHandler.drawIcon(ICON_CLIENT_ONLINE);
                            String name = (String)msg.obj;
                            if(name == null) name = "";
                            mPaintHandler.drawString(name, STRING_TYPE_SIP_ACCOUNT_NAME);
                        }else{
                            this.sendEmptyMessage(MSG_NOTIF);
                        }
                    }
                        break;
                    case MSG_BATTERY_LEVEL_CHANGED:{
                        int icon;
                        if(195 < mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C10;
                        }else if(190 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C9;
                        }else if(180 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C8;
                        }else if(170 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C7;
                        }else if(160 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C6;
                        }else if(150 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C5;
                        }else if(140 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C4;
                        }else if(130 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C3;
                        }else if(120 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C2;
                        }else if(110 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C1;
                        }else if(100 < mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_C0;
                        }else if(95 < mLastBatteryLevel ){
                            icon = ICON_BATTERY_LEVLE_10;
                        }else if(90 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_9;
                        }else if(80 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_8;
                        }else if(70 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_7;
                        }else if(60 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_6;
                        }else if(50 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_5;
                        }else if(40 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_4;
                        }else if(30 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_3;
                        }else if(20 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_2;
                        }else if(10 <= mLastBatteryLevel){
                            icon = ICON_BATTERY_LEVLE_1;
                        }else{
                            icon = ICON_BATTERY_LEVLE_0;
                        }

                        mPaintHandler.drawIcon(icon);
                    }
                        break;
                    case MSG_SCREEN_ON:{
                        boolean onOff = msg.arg1 == 1;
                        mMainThreadHandler.removeMessages(MSG_TIME_TICK);
                        mMainThreadHandler.removeMessages(MSG_UPDATE_MOBILE_TRAFFIC);
                        mMainThreadHandler.removeMessages(MSG_WIFI_STATUS_CHANGED);
                        mMainThreadHandler.removeMessages(MSG_NOTIF);


                        if(onOff){
                            if(updateTime()){
                                mPaintHandler.drawString(String.format("%02d:%02d",mLastHour, mLastMin), STRING_TYPE_TIME);
                            }

                            mMainThreadHandler.sendEmptyMessage(MSG_TIME_TICK);
                            mMainThreadHandler.sendEmptyMessage(MSG_UPDATE_MOBILE_TRAFFIC);
                            mMainThreadHandler.sendEmptyMessage(MSG_WIFI_STATUS_CHANGED);
                            mMainThreadHandler.sendEmptyMessage(MSG_NOTIF);
                        }
                        mRefreshOn = onOff;
                    }
                        break;
                    case MSG_TIME_TICK:{
                        if(updateTime()){
                            mPaintHandler.drawString(String.format("%02d:%02d",mLastHour, mLastMin), STRING_TYPE_TIME);
                        }
                        if(mRefreshOn){
                            this.sendEmptyMessageDelayed(MSG_TIME_TICK, 1000);
                        }
                    }
                        break;
                    default:
                        Log.e(TAG, "unknown message:" + msg.what);
                        break;
                }
            }
        };
        mScreenOnOffReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(Intent.ACTION_SCREEN_OFF)){
                    Log.d(TAG, "screen off");
                    refresh(false);
                }else if(intent.getAction().equals(Intent.ACTION_SCREEN_ON)){
                    Log.d(TAG, "screen on");
                    refresh(true);
                }else if(intent.getAction().equals("com.womate.esix.SHUTTINGDOWN")){
                    Log.d(TAG, "shutting down");
                    mPaintHandler.markShutdown();
                    mPaintHandler.drawIcon(ICON_SHUTTING_DOWN);
                }else{
                    Log.e(TAG, "unknown action for receiver:" + intent.getAction());
                }
            }
        };
        IntentFilter ift = new IntentFilter();
        ift.addAction(Intent.ACTION_SCREEN_OFF);
        ift.addAction(Intent.ACTION_SCREEN_ON);
        ift.addAction("com.womate.esix.SHUTTINGDOWN");
        MainService.getInstance().registerReceiver(mScreenOnOffReceiver, ift);

        PowerManager pm = (PowerManager)MainService.getInstance().getSystemService(Context.POWER_SERVICE);
        refresh(pm.isScreenOn());
    }

    //final static int ICON_WIFI_AP = 1;
    final static int ICON_CLIENT_ONLINE = 2;
    final static int ICON_MOBILE_DATA_OFF = 3;
    final static int ICON_MOBILE_SIGNAL_NO = 4;
    final static int ICON_MOBILE_SIGNAL_1 = 5;
    final static int ICON_MOBILE_SIGNAL_2 = 6;
    final static int ICON_MOBILE_SIGNAL_3 = 7;
    final static int ICON_MOBILE_SIGNAL_4 = 8;
    final static int ICON_MOBILE_SIGNAL_5 = 9;
    //final static int ICON_BATTERY = 10;
    final static int ICON_MISS_CALL = 11;
    final static int ICON_UNREAD_SMS = 12;
    final static int ICON_WIFI_STA_SIGNAL_NO = 13;
    final static int ICON_WIFI_STA_SIGNAL_1 = 14;
    final static int ICON_WIFI_STA_SIGNAL_2 = 15;
    final static int ICON_WIFI_STA_SIGNAL_3 = 16;
    final static int ICON_WIFI_STA_SIGNAL_4 = 17;
    final static int ICON_MOBILE_DATA_ON = 18;
    final static int ICON_BATTERY_LEVLE_1 = 19;
    final static int ICON_BATTERY_LEVLE_2 = 20;
    final static int ICON_BATTERY_LEVLE_3 = 21;
    final static int ICON_BATTERY_LEVLE_4 = 22;
    final static int ICON_BATTERY_LEVLE_5 = 23;
    final static int ICON_BATTERY_LEVLE_6 = 24;
    final static int ICON_BATTERY_LEVLE_7 = 25;
    final static int ICON_BATTERY_LEVLE_8 = 26;
    final static int ICON_BATTERY_LEVLE_9 = 27;
    final static int ICON_BATTERY_LEVLE_10 = 28;
    final static int ICON_3G_ONGOING = 29;
    final static int ICON_3G_IDLE = 30;
    final static int ICON_CLIENT_OFFLINE = 32;
    final static int ICON_3G_DISABLED = 33;
    final static int ICON_BATTERY_LEVLE_0 = 34;
    final static int ICON_BATTERY_CHARGING = 35;
    final static int ICON_3G_LOAD = 36;
    final static int ICON_WIFI_STA = 37;
    final static int ICON_BATTERY_LEVLE_C1 = 38;
    final static int ICON_BATTERY_LEVLE_C2 = 39;
    final static int ICON_BATTERY_LEVLE_C3 = 40;
    final static int ICON_BATTERY_LEVLE_C4 = 41;
    final static int ICON_BATTERY_LEVLE_C5 = 42;
    final static int ICON_BATTERY_LEVLE_C6 = 43;
    final static int ICON_BATTERY_LEVLE_C7 = 44;
    final static int ICON_BATTERY_LEVLE_C8 = 45;
    final static int ICON_BATTERY_LEVLE_C9 = 46;
    final static int ICON_BATTERY_LEVLE_C10 = 47;
    final static int ICON_BATTERY_LEVLE_C0 = 48;
    final static int ICON_SHUTTING_DOWN = 49;


    final static int STRING_TYPE_TIME = 1;
    //final static int STRING_TYPE_STA_COUNT_IN_AP = 2;
    final static int STRING_TYPE_SSID_IN_STA = 3;
    final static int STRING_TYPE_MISS_CALL_COUNT = 4;
    final static int STRING_TYPE_UNREAD_SMS_COUNT = 5;
    final static int STRING_TYPE_SIP_ACCOUNT_NAME = 6;
    final static int STRING_TYPE_3G_LOAD = 7;

    private static native void drawString(String str, int type);
    private static native void drawIcon(int icon);
    private static native void clearString(int type);
    private static native void clearIcon(int type);
    private static native void flushScreen();

    class PaintHandler extends Handler{
        static final int MSG_REFRESH = 1;
        static final int MSG_DRAW_STRING = 3;
        static final int MSG_DRAW_ICON = 4;
        static final int MSG_CLEAR_STRING = 5;
        static final int MSG_CLEAR_ICON = 6;

        volatile boolean bShuttingdown = false;

        public PaintHandler(Looper looper) {
            super(looper);
        }

        public void drawString(String str, int type){
            Log.d(TAG, "draw String " + str + " of type " + type);
            Message msg = Message.obtain(this);
            msg.arg1 = type;
            msg.what = MSG_DRAW_STRING;
            msg.obj = str;
            msg.sendToTarget();
        }
        public void markShutdown(){
            bShuttingdown = true;
        }
        public void drawIcon(int icon){
            Log.d(TAG, "draw Icon:" + icon);
            Message msg = Message.obtain(this);
            msg.arg1 = icon;
            msg.what = MSG_DRAW_ICON;
            msg.sendToTarget();
        }
        public void clearString(int type){
            Log.d(TAG, "clear string " + type);
            Message msg = Message.obtain(this);
            msg.arg1 = type;
            msg.what = MSG_CLEAR_STRING;
            msg.sendToTarget();
        }
        public void clearIcon(int type){
            Log.d(TAG, "clear icon " + type);
            Message msg = Message.obtain(this);
            msg.arg1 = type;
            msg.what = MSG_CLEAR_ICON;
            msg.sendToTarget();
        }
        public void flushScreen(){
            this.removeMessages(MSG_REFRESH);
            this.sendEmptyMessageDelayed(MSG_REFRESH, 50);
        }

        public void handleMessage(Message msg){
            if(PaintHandler.this.bShuttingdown){
                if(msg.what != MSG_REFRESH){
                    if(!(msg.what == MSG_DRAW_ICON && msg.arg1 == ICON_SHUTTING_DOWN)){
                        Log.i(TAG, "ignore all paint request while shutting down, expect draw shutting down icon");
                        return;
                    }
                }
            }
            switch(msg.what){
                case MSG_DRAW_ICON:{
                    DisplayServiceImpl.drawIcon(msg.arg1);
                    this.flushScreen();
                }
                    break;
                case MSG_DRAW_STRING:{
                    DisplayServiceImpl.drawString((String)msg.obj, msg.arg1);
                    this.flushScreen();
                }
                break;
                case MSG_CLEAR_STRING:{
                    DisplayServiceImpl.clearString(msg.arg1);
                    this.flushScreen();
                }
                break;
                case MSG_CLEAR_ICON:{
                    DisplayServiceImpl.clearIcon(msg.arg1);
                    this.flushScreen();
                }
                break;
                case MSG_REFRESH:{
                    Log.d(TAG, "flush");
                    DisplayServiceImpl.flushScreen();
                }
                    break;

                default:
                    Log.e(DisplayServiceImpl.TAG, "unknown message:" + msg.what);
            }
        }
    }
}