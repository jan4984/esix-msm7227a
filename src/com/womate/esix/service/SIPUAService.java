package com.womate.esix.service;

import android.os.Handler;

public interface SIPUAService {
    public interface CallListenerBase{
        public void newCallComing(String num);
        public void disconnected(boolean answered);
        public void accepted();
        public void startRecordDtmfForDailingNumber();
        public boolean isRecordingDtmfForDailingNumber();
    }

    public interface SMSListenerBase{
        //message received from UA
        public void newMessageReceived(String id, String dest, String content);
        //message send to UA successed
        public void messageSendOK(String id);
        //message send to UA failed
        public void messageSendFail(String id);
        public void clientStatusChanged(boolean isConnnected);
    }

    public void sendMessageACK(String id, boolean isSuccess);
    public String sendMessage(String sender, String time, String content);

    public void accept();
    public void ring();
    public void invite(String num);
    public void disconnect();
    public void setCallListener(CallListenerBase l);
    public void setSmsListener(SMSListenerBase l);
    public Handler getUnknownPayloadHandler();
    public void notifyClientStatusChanged(boolean isConnected, String name);
    public void stopSendFile2Rtp();
    public boolean isSipClientConnected();
    public String getSipClientName();
}
