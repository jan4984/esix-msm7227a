package com.womate.esix.service;

import com.womate.esix.service.impl.*;

public class ServiceFactory {
    private static CallService CALL_SERVICE = null;
    private static WIFIService WIFI_SERVICE = null;
    private static WebService WEB_SERVICE = null;
    private static CfgService CFG_SERVICE = null;
    private static SMSService SMS_SERVICE = null;
    private static DisplayService DISP_SERVICE = null;
    private static CellularNetworkService CELL_NET_SERVICE = null;
    private static MiscService MISC_SERVICE = null;

    public static synchronized WIFIService getWifiService(){
        if(WIFI_SERVICE == null){
            WIFI_SERVICE = new WIFIServiceImpl();
        }
        return WIFI_SERVICE;
    }
    public static synchronized MiscService getMiscService(){
        if(MISC_SERVICE == null){
            MISC_SERVICE = new MiscServiceImpl();
        }
        return MISC_SERVICE;
    }
    public static synchronized CallService getCallService(){
        if(CALL_SERVICE == null){
            CALL_SERVICE = new CallServiceImpl();
        }
        return CALL_SERVICE;
    }

    public static synchronized SMSService getSmsService(){
        if(SMS_SERVICE == null){
            SMS_SERVICE = new SMSServiceImpl();
        }
        return SMS_SERVICE;
    }

    public static synchronized  WebService getWebService(){
        if(WEB_SERVICE == null){
            WEB_SERVICE = new WebServiceImpl();
        }
        return WEB_SERVICE;
    }

    public static synchronized  CfgService getCfgService(){
        if(CFG_SERVICE == null){
            CFG_SERVICE = new CfgServiceImpl();
        }
        return CFG_SERVICE;
    }

    public static synchronized  DisplayService getDispService(){
        if(DISP_SERVICE == null){
            DISP_SERVICE = new DisplayServiceImpl();
        }
        return DISP_SERVICE;
    }

    public static synchronized  CellularNetworkService getCellularNetworkService(){
        if(CELL_NET_SERVICE == null){
            CELL_NET_SERVICE = new CellularNetworkServiceImpl();
        }
        return CELL_NET_SERVICE;
    }
}
