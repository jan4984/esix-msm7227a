package com.womate.esix.service;

import android.database.Cursor;

public interface SMSService {
    SIPUAService.SMSListenerBase getSIPUAListener();
    void deleteAllReadAndSend();
    Cursor queryUnreadforRead();
    void markCurrentRead(Cursor current);
}
