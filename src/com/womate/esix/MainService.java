package com.womate.esix;

import android.app.KeyguardManager;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import com.womate.esix.service.*;
import com.womate.esix.service.impl.SIPUAServiceImpl;
import local.server.ESixLocationServiceImpl;
import local.server.ServerProfile;
import local.ua.UA;
import local.server.Proxy;

import org.zoolu.sip.provider.SipProvider;
import org.zoolu.sip.provider.SipStack;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import java.io.*;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class MainService extends Service {
    public static final String FIRMWARE_VERSIN = "0.92";
    static final String TAG = "ESix-MainService";
    public static final String WEB_SITE_DIR = "/data/esix/webservice/";
    public static final String WEB_SITE_ZIP_FILE = "/data/esix/webservice/website.zip";
    public static final String SIP_WORKING_DIR = "/data/esix/voip/";
    public static final String SIP_WORKING_RES_FILE = "/data/esix/voip/sip-res.zip";
    public static final String WORK_DIR = "/data/esix";
    public static final String CFG_FILE = WORK_DIR + "/voip/mjsip.cfg.txt";
    public static final String PCM_FILE_DIAL_NUM_EN = WORK_DIR + "/voip/dial_en.pcm";
    public static final int MSG_CHECK_WIFI = 0;
    public static final int MSG_REBOOT = 1;
    public static final int MSG_SHUTDOWN = 2;
    public static final int MSG_WAITING_ONSTARTCOMMAND_FAILED = 3;

    private static MainService THIS;

    //private volatile boolean mWaitingOnStartCommand = false;

    Proxy mProxy;
    SIPUAService mSIPUA;
    CallService mCallService;
    SMSService mSmsService;
    WIFIService mWIFIService;
    Handler mMainThreadHandler = new Handler(){
        public void handleMessage(Message msg){
            Log.d(TAG, "handling " + msg.what);
            switch(msg.what){
                case MSG_WAITING_ONSTARTCOMMAND_FAILED:
                    MainService.this.onStartCommand(null, 0, 0);
                    break;
                case MSG_SHUTDOWN:
                    ((PowerManager)MainService.this.getSystemService(Context.POWER_SERVICE)).reboot("shutdown");
                case MSG_REBOOT:
                    if(msg.obj == null || !(msg.obj instanceof String)){
                        ((PowerManager)MainService.this.getSystemService(Context.POWER_SERVICE)).reboot("");
                    }else{
                        ((PowerManager)MainService.this.getSystemService(Context.POWER_SERVICE)).reboot((String)msg.obj);
                    }
                    //ShutdownThread.reboot(MainService.this, (String)msg.obj, false);
                    break;
                case MSG_CHECK_WIFI:{
                    if(mWIFIService.isWIFIReady()){
                        ESixLocationServiceImpl.initConsts(mWIFIService.getWIFIAddress(), mCfgService.readCfg().isInternalSIPProxyOn);
                        Log.d(TAG, "wifi ready at " + ESixLocationServiceImpl.ESIX_STATIC_IP);
                        try{
                            Thread.sleep(1000);//wait 1 s to bind
                        }catch (InterruptedException e) {}
                        mWebService.start(ESixLocationServiceImpl.ESIX_STATIC_IP);

                        //parse config
                        SipStack.init(CFG_FILE);// just cfg, could be used in UA and Proxy

                        //new proxy instance
                        if(ServiceFactory.getCfgService().readCfg().isInternalSIPProxyOn){
                            SipProvider prov_sv = new SipProvider(CFG_FILE, "ESix-sipServer");
                            ServerProfile prof_sv = new ServerProfile(CFG_FILE);
                            mProxy = new Proxy(prov_sv, prof_sv);// start proxy server
                        }

                        //new UA for gateway
                        // sip port has been used by server, using 6543 in user agent
                        if (!UA.init("local.ua.UA",
                                new String[] { "-f", CFG_FILE, "-p", String.valueOf(ESixLocationServiceImpl.ESIX_STATIC_PORT) })){
                            Log.e(TAG, "config parse failed", new RuntimeException());
                        }
                        try{
                            mSIPUA =  new SIPUAServiceImpl(mProxy, UA.sip_provider, UA.ua_profile);
                        }catch(IllegalStateException exp){
                            Log.d(TAG, "voip funciton closed");
                            break;
                        }
                        //new call service
                        mCallService = ServiceFactory.getCallService();
                        mSmsService = ServiceFactory.getSmsService();
                        //bind to CallService to SIPUAService
                        mSIPUA.setCallListener(mCallService.getSIPUAListener());
                        mSIPUA.setSmsListener(mSmsService.getSIPUAListener());
                    }else{
                        Log.d(TAG, "wifi not ready, wait another 1s");
                        this.sendEmptyMessageDelayed(MSG_CHECK_WIFI, 1000);
                    }
                }
                break;
                default:
                    Log.e(TAG, "unknown message:" + msg.what);
                    break;
            }
        }
    };
    WebService mWebService;
    CfgService mCfgService;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    public void recordLog(Throwable ex){
        PrintStream ps = null;
        try{
            ps = new PrintStream(new FileOutputStream("/data/esix/exception", true));
            ps.println(new Date().toString());
            ps.println(Log.getStackTraceString(ex));
            ps.println(Log.getStackTraceString(ex.getCause()));
            ps.close();
        }catch(Exception e){
            if(ps != null) ps.close();
        }
    }
    public void onCreate(){
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                recordLog(ex);
                Log.e(TAG, "uncaughtException:", ex);
                Log.e(TAG, "cause:", ex.getCause());
                ((PowerManager)THIS.getSystemService(Context.POWER_SERVICE)).reboot("");
            }
        });

        Log.d(TAG, "onCreate");
        try {
            System.setErr(new LogcatPrintStream());
        } catch (FileNotFoundException e) {
            Log.e(TAG, "std err not directed because:", e);
        }
        super.onCreate();
        THIS = this;
        extarctWebSite(WEB_SITE_DIR, WEB_SITE_ZIP_FILE);
        extarctWebSite(SIP_WORKING_DIR, SIP_WORKING_RES_FILE);
        //WHEN process is killed, it not invoke onstartcommand(), so call it manully in 1000ms
        //mWaitingOnStartCommand = true;
        mMainThreadHandler.sendEmptyMessageDelayed(MSG_WAITING_ONSTARTCOMMAND_FAILED, 1000);
    }

    private void extarctWebSite(String path, String file){
        Log.d(TAG, "extracting " + file + " to " + path);
        ZipInputStream zis = null;
        try{
            String filename;
            InputStream is = new FileInputStream(file);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;
            byte[] buffer = new byte[1024];
            int count;

            while ((ze = zis.getNextEntry()) != null)
            {
                // zapis do souboru
                filename = ze.getName();

                // Need to create directories if not exists, or
                // it will generate an Exception...
                if (ze.isDirectory()) {
                    Log.d(TAG, "creating folder:" + path + filename);
                    File fmd = new File(path + filename);
                    fmd.mkdirs();
                    continue;
                }

                File asp = new File(new File(path + filename).getParent());
                if(!asp.exists()){
                    Log.d(TAG, "creating folder:" + path + filename);
                    asp.mkdirs();
                }
                FileOutputStream fout = new FileOutputStream(path + filename);

                // cteni zipu a zapis
                while ((count = zis.read(buffer)) != -1)
                {
                    fout.write(buffer, 0, count);
                }

                fout.close();
                zis.closeEntry();
            }

            zis.close();
        }catch(Exception e){
            try{
                zis.close();
            }catch(Exception ee){}
            Log.e(TAG, "extracting web site files.", e);
        }
    }

    static class LogcatPrintStream extends PrintStream{
        public LogcatPrintStream() throws FileNotFoundException {
            super("/data/esix/stderr");
        }
        public void println(String str){
            Log.e("ESix-StdErr", str);
            Log.e("ESix-StdErr", "calling stack:", new RuntimeException("for stack info only"));
        }
    }

    public static MainService getInstance(){
        return THIS;
    }
    public Handler getUnknownPayloadHandler(){
        return mSIPUA.getUnknownPayloadHandler();
    }

    final static byte[] ON = {'1'};
    final static byte[] OFF = {'0'};
    public static void turnLED(String p, boolean onoff){
        Log.d(TAG, "write file " + p + " " + onoff);

        try{
            FileOutputStream fp = new FileOutputStream(p);
            fp.write(onoff ? ON : OFF);
            fp.close();
        }catch(Exception e)
        {
            Log.e(TAG, "write led", e);
        }
    }
    public int onStartCommand(Intent intent, int flags, int startId) {
        KeyguardManager km = (KeyguardManager)getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock kl = km.newKeyguardLock("esix");
        kl.disableKeyguard();
        mMainThreadHandler.removeMessages(MSG_WAITING_ONSTARTCOMMAND_FAILED);
        Log.d(TAG, "onStartCommand");
        ServiceFactory.getDispService();//start display
        ServiceFactory.getCellularNetworkService();//start cellular network status publisher
        ServiceFactory.getMiscService();//start misc service

        mWebService = ServiceFactory.getWebService();//currently not start in real.
        mCfgService = ServiceFactory.getCfgService();
        mWIFIService = ServiceFactory.getWifiService();//start wifi to get a static IP
        mWIFIService.turnOn();
        mMainThreadHandler.sendEmptyMessageDelayed(MSG_CHECK_WIFI, 1000);

        return START_STICKY;
        //return START_REDELIVER_INTENT;
    }

    public static SIPUAService getSIPUAService(){
        return THIS.mSIPUA;
    }

    public void shutdown(){
        Log.e(TAG, "shutdown at ", new RuntimeException("stack only"));
        Message msg = new Message();
        msg.what = MSG_SHUTDOWN;
        mMainThreadHandler.sendMessage(msg);
    }

    public void reboot(String reason, long delay){
        Log.e(TAG, reason + " reboot at ", new RuntimeException("stack only"));
        Message msg = new Message();
        msg.what = MSG_REBOOT;
        msg.obj = reason;
        mMainThreadHandler.sendMessageDelayed(msg, delay);
    }
    public void reboot(String reason){
        reboot(reason, 0);
    }

    static {
        System.loadLibrary("phone_ext_jni");
    }
}