package com.womate.esix;

/**
 * Created with IntelliJ IDEA.
 * User: jan4984@gmail.com
 * Date: 6/5/13
 * Time: 4:11 PM
 * copyright belongs to jan4984@gmail.com
 */
public class Utils {
    public static String dumpHex(byte[] data){
        StringBuilder sb = new StringBuilder();
        for (byte b : data) {
            sb.append(String.format("%02X ", b));
        }
        return sb.toString();
    }
}
