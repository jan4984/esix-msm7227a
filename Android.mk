LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := eng user
LOCAL_SRC_FILES := $(call all-java-files-under, src) #$(call all-subdir-java-files)

LOCAL_JAVA_LIBRARIES := services

LOCAL_PACKAGE_NAME := PhoneExt
LOCAL_CERTIFICATE := platform

include $(BUILD_PACKAGE)


include $(CLEAR_VARS)
LOCAL_MODULE := esix-website
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := eng
LOCAL_SRC_FILES := working-dir/webservice/website.zip
PRODUCT_COPY_FILES += $(LOCAL_PATH)/working-dir/webservice/website.zip:data/esix/webservice/website.zip
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := esix-sip
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_TAGS := eng
LOCAL_SRC_FILES := working-dir/voip/sip-res.zip
PRODUCT_COPY_FILES += $(LOCAL_PATH)/working-dir/voip/sip-res.zip:data/esix/voip/sip-res.zip
include $(BUILD_PREBUILT)

include $(call all-makefiles-under,$(LOCAL_PATH))