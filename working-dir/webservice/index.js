document.observe("dom:loaded", function () {        
    if (!window.WebSocket) {
        alert("FATAL: WebSocket not natively supported. Change a browser");
    }
    log("opening websocket:" + "ws://" + location.hostname + ":2211");
    __ws = new WebSocket("ws://" + location.hostname + ":2211");
    __ws.sendmessage = function (c, p) {
            var e = { cmd: c, paras: p };
            var str = Object.toJSON(e);
            __ws.send(str);
            log("sent: " + str +"\n");
        };
    __ws.onopen = function () {
        __ws.sendmessage("EVT_QUERY_FW", "");
    };
    __ws.onmessage = function (e) {
        log("receive:" + e.data + "\n");
        var msg = e.data.toString().evalJSON();
        if (msg.cmd == "EVT_QUERY_FW") {
            if (msg.val != "FAIL") {
                ko.cleanNode($('info'));
                ko.applyBindings(msg.paras, $('info'));
            }
        }else if(msg.cmd == "EVT_PING"){
            __ws.sendmessage("EVT_PONG", "");
        }
    };
});
$('resetDevice').observe('click', function(e){
    __ws.sendmessage("EVT_RESET_DEV", "");
});
