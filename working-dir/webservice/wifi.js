var wifiAPModel = {ssid:"hello", password:"init-password"};
ko.applyBindings(wifiAPModel, $('wifiap'));
var wifiStaOpModel = {
    onoff:ko.observable(false),
    items:ko.observableArray([])//{idx:-1,ssid:"",statusTxt:"",status:"0"}])
};
var wifiOnRefresher = undefined;
document.observe("dom:loaded", function () {
    var statusTxts = $F('txtStatus').toString().evalJSON();
    if (!window.WebSocket) {
        alert("FATAL: WebSocket not natively supported. Change a browser");
    }
    log("opening websocket:" + "ws://" + location.hostname + ":2211");
    __ws = new WebSocket("ws://" + location.hostname + ":2211");
    __ws.sendmessage = function (c, p) {
            var e = { cmd: c, paras: p };
            var str = Object.toJSON(e);
            __ws.send(str);
            log("sent: " + str +"\n");
        };
    __ws.onopen = function () {
         __ws.sendmessage("EVT_QUERY_WIFI_AP","");
         __ws.sendmessage("EVT_QUERY_WIFI_STA_ONOFF", "");
    };
    __ws.onmessage = function (e) {
        log("receive:" + e.data + "\n");
        var msg = e.data.toString().evalJSON();
        if (msg.cmd == "EVT_QUERY_WIFI_AP"){
                if (msg.paras != "FAIL") {
                    wifiAPModel = Object.clone(msg.paras);
                    ko.cleanNode($('wifiap'));
                    ko.applyBindings(wifiAPModel, $('wifiap'));
                }
        }else if(msg.cmd == "EVT_QUERY_WIFI_STA_ONOFF"){
            if(msg.paras != 'FAIL'){
                var onoff = msg.paras.onoff == "true";
                ko.cleanNode($('wifiStaOnOff'));
                ko.applyBindings(msg.paras, $('wifiStaOnOff'));
                wifiStaOpModel.onoff(onoff);
            }
        }else if(msg.cmd == "EVT_WIFI_STA_SCAN"){
            if(msg.paras != 'FAIL'){
                var exists = false;
                var statusTxt = statusTxts[msg.paras.status];
                //log("current items length:" + wifiStaOpModel.items.length);
                for(var j = 0; j < wifiStaOpModel.items().length; j++){
                    //log("comparing idx:" + wifiStaOpModel.items()[j].idx);
                    if(wifiStaOpModel.items()[j].idx == msg.paras.idx){
                        wifiStaOpModel.items()[j].statusTxt(statusTxt);
                        exists = true;
                        break;
                    }
                }
                if(!exists){
                    newItem = {idx:msg.paras.idx, ssid:msg.paras.ssid, statusTxt:ko.observable(statusTxt), status:msg.paras.status};
                    wifiStaOpModel.items.push(newItem);
                }
            }
        }else if(msg.cmd == "EVT_PING"){
            __ws.sendmessage("EVT_PONG", "");
        }
    };
});
ko.applyBindings(wifiStaOpModel, $('wifiSta'));
$('applyWifiStaOnOff').observe('click', function(e){
    var onoff = Form.getInputs('wifiStaOnOff', 'radio', 'WIFIStaOnOff').find(function(r){
                return r.checked;
            }).value;
    if(onoff=="true"){
        if(!wifiOnRefresher)
            wifiOnRefresher = setInterval(function(){location.reload();},3000);
    }
    __ws.sendmessage("EVT_SET_WIFI_STA_ONOFF", {onoff: onoff});
});
$('applyWifiAP').observe('click', function(e){
    var pass = wifiAPModel.password;
    if(pass.length < 8){
        return;
    }
    __ws.sendmessage("EVT_SET_WIFI_AP", wifiAPModel);
});
$('startScan').observe('click', function(e){
    wifiStaOpModel.items.removeAll();
    __ws.sendmessage("EVT_WIFI_STA_SCAN", "");
    if(wifiOnRefresher){
        clearInterval(wifiOnRefresher);
        wifiOnRefresher=undefined;
    }
});
$('staConnect').observe('click', function(e){
    var pass = prompt($F('txtInputPass'));
    if(pass == null) pass = '';
    var idx = Form.getInputs('wifiSta', 'radio', 'staNetIdx').find(function(r){
                      return r.checked;
                  }).value;
    if(idx == -1){
        idx = $F("hiddenssid");
    }
    __ws.sendmessage("EVT_WIFI_STA_CONNECT", {"idx":idx, password:pass});
});
//$('staForget').observe('click', function(e){
//    __ws.sendmessage("EVT_WIFI_STA_FORGET", {"idx":Form.getInputs('wifiSta', 'radio', 'staNetIdx').find(function(r){
//        return r.checked;
//    }).value});
//});
