var apnModel = {};  
document.observe("dom:loaded", function () {      
    if (!window.WebSocket) {
        alert("FATAL: WebSocket not natively supported. Change a browser");
    }
    log("opening websocket:" + "ws://" + location.hostname + ":2211");
    __ws = new WebSocket("ws://" + location.hostname + ":2211");
    __ws.sendmessage = function (c, p) {
            var e = { cmd: c, paras: p };
            var str = Object.toJSON(e);
            __ws.send(str);
            log("sent: " + str +"\n");
        };
    __ws.onopen = function () {
        __ws.sendmessage("EVT_QUERY_MT", "");
        __ws.sendmessage("EVT_QUERY_MD_ONOFF", "");
        __ws.sendmessage("EVT_QUERY_APN","");
    };
    __ws.onmessage = function (e) {
        log("receive:" + e.data + "\n");
        var msg = e.data.toString().evalJSON();
        if (msg.cmd == "EVT_QUERY_MT"){
            if (msg.paras != "FAIL") {
                msg.curTxt = msg.paras.cur + "KB";
                msg.totalTxt = msg.paras.total + "KB";
                ko.cleanNode($('mt'));
                ko.applyBindings(msg, $('mt'));
            }
        }else if(msg.cmd == "EVT_QUERY_MD_ONOFF") {
            if(msg.paras != 'FAIL'){
                ko.cleanNode($('mobiledata'));
                ko.applyBindings(msg.paras, $('mobiledata'));
            }
        }else if(msg.cmd == "EVT_QUERY_APN"){
            if(msg.paras != 'FAIL'){
                //Note that Object.clone is a shadow clone, not add deep object in paras
                apnModel = Object.clone(msg.paras);
                ko.cleanNode($('apn'));
                ko.applyBindings(apnModel, $('apn'));
            }
        }else if(msg.cmd == "EVT_PING"){
            __ws.sendmessage("EVT_PONG", "");
        }
    };
});
$('resetMT').observe('click', function(e){
    __ws.sendmessage("EVT_RESET_MT","");
});
$('applyMTOnOff').observe('click', function(e){
    __ws.sendmessage("EVT_SET_MD_ONOFF", {onoff:
        Form.getInputs('mobiledata', 'radio', 'mobileDataOnOff').find(function(r){
            return r.checked;
        }).value});
});
$('applyAPN').observe('click', function(e){
    __ws.sendmessage("EVT_APPLY_APN", apnModel);
});
