#include <stdio.h>

int main(int args, char** argv){
  int c,r;
  for(r = 0; r < 64; r+=8){
    for(c = 0; c < 128; c++){
      printf("*(d++) = SP[%d][%d] | SP[%d][%d] << 1 | SP[%d][%d] << 2 | SP[%d][%d] << 3 | SP[%d][%d] << 4 | SP[%d][%d] << 5 | SP[%d][%d] << 6 | SP[%d][%d] << 7;\n",r,c,r+1,c,r+2,c,r+3,c,r+4,c,r+5,c,r+6,c,r+7,c);
    }
  }
  return 0;
}
