#ifndef DISPLAY_H_
#define DISPLAY_H_

#define STRING_TYPE_TIME 1
#define STRING_TYPE_STA_COUNT_IN_AP 2
#define STRING_TYPE_SSID_IN_STA  3
#define STRING_TYPE_MISS_CALL_COUNT 4
#define STRING_TYPE_UNREAD_SMS_COUNT 5
#define STRING_TYPE_SIP_ACCOUNT_NAME 6
#define STRING_TYPE_3G_LOAD 7

typedef struct _StringInfo{
  int type;
  int x;
  int y;
  int w;
  int h;
}StringInfo;

typedef struct _CharInfo{
  int w;
  int h;
  int y;
  const char* bmp;
}CharInfo;

typedef struct _IconInfo{
  int type;
  int x;
  int y;
  int w;
  int h;
  const char* bmp;
}IconInfo;

#define ICON_WIFI_AP 1
#define ICON_CLIENT_ONLINE 2
#define ICON_CLIENT_OFFLINE 32
#define ICON_MOBILE_DATA_OFF 3
#define ICON_MOBILE_SIGNAL_NO 4
#define ICON_MOBILE_SIGNAL_1 5
#define ICON_MOBILE_SIGNAL_2 6
#define ICON_MOBILE_SIGNAL_3 7
#define ICON_MOBILE_SIGNAL_4 8
#define ICON_MOBILE_SIGNAL_5 9
//#define ICON_BATTERY 10
#define ICON_MISS_CALL 11
#define ICON_UNREAD_SMS 12
#define ICON_WIFI_STA_SIGNAL_NO 13
#define ICON_WIFI_STA_SIGNAL_1 14
#define ICON_WIFI_STA_SIGNAL_2 15
#define ICON_WIFI_STA_SIGNAL_3 16
#define ICON_WIFI_STA_SIGNAL_4 17
#define ICON_MOBILE_DATA_ON 18
#define ICON_BATTERY_LEVLE_1 19
#define ICON_BATTERY_LEVLE_2 20
#define ICON_BATTERY_LEVLE_3 21
#define ICON_BATTERY_LEVLE_4 22
#define ICON_BATTERY_LEVLE_5 23
#define ICON_BATTERY_LEVLE_6 24
#define ICON_BATTERY_LEVLE_7 25
#define ICON_BATTERY_LEVLE_8 26
#define ICON_BATTERY_LEVLE_9 27
#define ICON_BATTERY_LEVLE_10  28
#define ICON_3G_ONGOING 29
#define ICON_3G_IDLE 30
#define ICON_3G_DISABLED 33
#define ICON_BATTERY_LEVLE_0  34
#define ICON_BATTERY_CHARGING  35
#define ICON_3G_LOAD 36
#define ICON_WIFI_STA 37
#define ICON_BATTERY_LEVLE_C1  38
#define ICON_BATTERY_LEVLE_C2  39
#define ICON_BATTERY_LEVLE_C3  40
#define ICON_BATTERY_LEVLE_C4  41
#define ICON_BATTERY_LEVLE_C5  42
#define ICON_BATTERY_LEVLE_C6  43
#define ICON_BATTERY_LEVLE_C7  44
#define ICON_BATTERY_LEVLE_C8  45
#define ICON_BATTERY_LEVLE_C9  46
#define ICON_BATTERY_LEVLE_C10  47
#define ICON_BATTERY_LEVLE_C0  48
#define ICON_SHUTTINGDOWN 49

extern StringInfo STR_INFOS[];
extern IconInfo ICN_INFS[];
extern CharInfo CHARS[];

#endif
