#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <time.h>
#include <utils/Log.h>
#include <linux/cdev.h>
#include <linux/ioctl.h>
#include <sys/mman.h>  
#include "jni.h"
#include "JNIHelp.h"
#include "Display.h"

#define WOMATE_IOCTL_MAGIC      'w'
#define WOMATE_FBIOUPDATE          _IO(WOMATE_IOCTL_MAGIC, 2)

//namespace esix_display{

  //using namespace android;


  static CharInfo CI_UNKNOWN = {8,10,1,"\xFF\x81\x81\x81\x81\x81\x81\x81\x81\xFF"};
  static char SP[64][128];//screen paint

  static inline int getCharBitmapPixel(CharInfo* inf, int x, int y){
    int bytesPerRow = inf->w > 8 ? 2 : 1;
    int b;
    if(x > 7){
      b = inf->bmp[bytesPerRow * y + 1];
      x = x - 8;
    }else{
      b = inf->bmp[bytesPerRow * y];
    }
    return b & (0x80 >> x);
  }

  static void getRectOfStringByType(int type, int* x, int* y, int* w, int* h){
    for(int i = 0; STR_INFOS[i].type != -1; i++){
      if(type == STR_INFOS[i].type){
	*x = STR_INFOS[i].x;
	*y = STR_INFOS[i].y;
	*w = STR_INFOS[i].w;
	*h = STR_INFOS[i].h;
	return;
      }
    }
    *x = *y = *w = *h = -1;
  }

  static void clearRect(JNIEnv *env, jobject thiz, jint x, jint y, jint w, jint h){
    for(int c = x; c - x < w && x < 128; c++){
      for(int r = y; r - y< h && r < 64; r++){
	SP[r][c] = 0;
      }
    }
  }

  static void clearString(JNIEnv *env, jobject thiz, jint type){
    int x,y,w,h;
    getRectOfStringByType(type, &x, &y, &w, &h);
    if(x == -1){
      LOGE("unknown position for type %d", type);
      return;
    }
    LOGD("clear string at (%d,%d)", x, y);
    clearRect(env, thiz, x, y, w, h);
  }

  static void drawString(JNIEnv *env, jobject thiz, jstring jstr, jint type){
    int x,y,w,h;    
    const char* pStr = env->GetStringUTFChars(jstr, NULL);
    const char* pStrDup = pStr;
    if(pStr == NULL){
      LOGE("draw string NULL, ignore");
      return;
    }

    getRectOfStringByType(type, &x, &y, &w, &h);
    if(x == -1){
      LOGE("unknown position for type %d", type);
      return;
    }
    LOGD("draw string %s at (%d,%d)", pStr, x, y);
    CharInfo* pCi = &CI_UNKNOWN;
    int rowBytesCount;
    char byte1;
    if(x < 0 || x >= 128 || y < 0 || y >= 64){
      LOGE("(%d,%d) out of range", x, y);
      return;
    }
    
    clearRect(env, thiz, x, y, w, h);
    do{
      if(' ' <= *pStr && *pStr <= '~'){
	pCi = &CHARS[(*pStr) - ' '];
      }else{
	LOGE("unknown char bitmap:%c", *pStr);
	pCi = &CI_UNKNOWN;
      }

      for(int r = y + pCi->y; r - y - pCi->y < pCi->h && y < 64; r++){
	for(int c = x; c - x < pCi->w && c < 128; c++){
	  SP[r][c] = getCharBitmapPixel(pCi, c - x, r - y - pCi->y) ? 1 : 0;
	  //LOGD("pixel at (%d,%d) of %c is %c", c - x, r - y, *pStr, SP[r][c] ? '*' : '.');
	}
      }
      pStr++;
      x += pCi->w + 1;//one pix for padding at right of char
    }while(*pStr);

    env->ReleaseStringUTFChars(jstr, pStrDup);
  }



  static inline int getIconBitmapPixel(IconInfo* inf, int x, int y){
    int bytesPerRow = (inf->w + 7) / 8;
    int b = inf->bmp[(bytesPerRow * y) + (x / 8)];
    x = x % 8;
    return b & (0x80 >> x);
  }

  static void clearIcon(JNIEnv *env, jobject thiz, jint iconId){
    IconInfo* pIcn = NULL;
    for(int i = 0; ICN_INFS[i].type != -1; i++){
      if(ICN_INFS[i].type == iconId){
	pIcn = &ICN_INFS[i];
	break;
      }
    }

    if(pIcn == NULL){
      LOGE("can not find icon for type %d to clear", iconId);
      return;
    }
    clearRect(env, thiz, pIcn->x, pIcn->y, pIcn->w, pIcn->h);
  }

  static void drawIcon(JNIEnv *env, jobject thiz, jint iconId){    
    IconInfo* pIcn = NULL;

    LOGD("draw icon %d", iconId);
    for(int i = 0; ICN_INFS[i].type != -1; i++){
      if(ICN_INFS[i].type == iconId){
	pIcn = &ICN_INFS[i];
	break;
      }
    }

    if(pIcn == NULL){
      LOGE("can not find icon for type:%d", iconId);
      return;
    }

    for(int y = 0; y < pIcn->h && y + pIcn->y < 64; y++){
      for(int x = 0; x < pIcn->w && x + pIcn->x < 128; x++){
	SP[y + pIcn->y][x + pIcn->x] = getIconBitmapPixel(pIcn, x, y) ? 1 : 0;
      }
    }    
  }
  
  static char ROW[128 + 2];
  static int FD = -1;
  static void flushScreen(JNIEnv *env, jobject thiz){
    int fd,ret,i;
    uint8_t* map;

    uint8_t* d;

    fd=open("/dev/womate_fb",O_RDWR);
    if(fd<0){
      LOGE("open /dev/womate_fb failed");
      return;
    }

    if((map=(uint8_t *)mmap(NULL, 64 * 128 / 8, PROT_READ|PROT_WRITE,MAP_SHARED, fd, 0))==MAP_FAILED){
      LOGE("map failed");
      close(fd);
      return;
    }
    d = map;

    #include "FLUSHSCREEN"
    //memset(map, 0xab, 1024);
    ioctl(fd,WOMATE_FBIOUPDATE);
    munmap(map, 64 * 128 / 8);
    close(fd);

#if 1
    LOGD("flushScreen");
    if(FD < 0){
        FD = open("/data/fakeScreen", O_WRONLY|O_CREAT);
    }
    if(FD < 0){      
      LOGE("open fake screen fail:%s", strerror(errno));
    }else{
      lseek(FD, 0, SEEK_SET);
      for(int row = 0; row < 64; row++){
	for(int col = 0; col < 128; col++){
	  ROW[col] = (SP[row][col] == 1) ? '*' : '.';
	}
	ROW[128] = '\n';
	write(FD, (const void*)ROW, 129);
      }
      fsync(FD);
    }
#endif//#if 0
  }

  JNINativeMethod gMethods[] = {
    {"drawString", "(Ljava/lang/String;I)V", (void *)drawString},
    {"drawIcon", "(I)V", (void *)drawIcon},
    {"clearIcon", "(I)V", (void *)clearIcon},
    {"clearString", "(I)V", (void *)clearString},
    {"flushScreen", "()V", (void *)flushScreen}
  };

//}//namespace

//using namespace esix_display;

static int registerMethods(JNIEnv *env)
{
    jclass clazz;
    if ((clazz = env->FindClass("com.womate.esix.service.impl.DisplayServiceImpl")) == NULL ||
        env->RegisterNatives(clazz, gMethods, NELEM(gMethods)) < 0) {
        LOGE("JNI registration failed");
        return -1;
    }
    return 0;
}

__attribute__((visibility("default"))) jint JNI_OnLoad(JavaVM *vm, void *unused)
{
    JNIEnv *env = NULL;
    if (vm->GetEnv((void **)&env, JNI_VERSION_1_4) != JNI_OK ||
        registerMethods(env)){
        return -1;
    }
    return JNI_VERSION_1_4;
}
