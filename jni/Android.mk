LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := eng

LOCAL_SRC_FILES := \
	DisplayServiceImpl.cpp \
	DisplayData.cpp	

LOCAL_SHARED_LIBRARIES := \
	libnativehelper \
	libcutils \
	libutils \

LOCAL_C_INCLUDES += \
	$(JNI_H_INCLUDE) \
	system/core/include/cutils \
	bionic/libc/include \

LOCAL_CFLAGS += -DLOG_TAG=\"esix-disp-jni\" -DLOG_NDEBUG=0 -DLOG_NDDEBUG=0 -DLOG_NIDEBUG=0

LOCAL_MODULE := libphone_ext_jni

LOCAL_PRELINK_MODULE := false

include $(BUILD_SHARED_LIBRARY)
