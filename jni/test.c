#include "Display.h"

static inline int getIconBitmapPixel(IconInfo* inf, int x, int y){
  int bytesPerRow = (inf->w + 7) / 8;
  int b = inf->bmp[(bytesPerRow * y) + (x / 8)];
  x = x % 8;
  return b & (0x80 >> x);
}

int main(){
  IconInfo* pInfo = &ICN_INFS[4];
  int x,y;
  for(y=0;y<pInfo->h;y++){
    for(x=0;x<pInfo->w;x++){
      if(getIconBitmapPixel(pInfo, x, y)){
	printf("*");
      }else{
	printf(".");
      }
    }
    printf("\n");
  }
}
